# codegeneration

#### 介绍
   一款基于数据库表格，根据自定义的velocity模板生成代码的Eclipse插件，支持用户自己新增更多的模板，把重复的工作制作成模版，
提高开发效率。本插件是基于Tao-AutoDAO（https://www.oschina.net/p/tao-autodao）项目的源码进行改造，变得更灵活通用。
总体思想就是：
1. 读取表格信息
2. 对表格信息进行简单转换
3. 将信息作为变量放置到VelocityContext中
4. 结合模板生成代码

#### 效果图
1. 配置模板路径， 这个是**必须的** ，可以从仓库下载简单的模板进行修改，然后放置到系统目录就可以了

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/145818_ddf88485_1018017.png "26.png")

2. 双击表名称，直接在Eclipse中查看表结构信息，避免切换的麻烦

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150013_7c9a259b_1018017.png "21.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150027_11713cfc_1018017.png "22.png")

3. 选中表格（可多选），右键，选择代码生成

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/145911_eca95123_1018017.png "23.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/145929_a79830af_1018017.png "24.png")

4. 展示最近生成的文件列表，双击即可打开

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/145950_9b08ae45_1018017.png "25.png")

#### 软件架构

  插件由3个模块组成，分别是：
- codegeneration： 用于生成插件安装包
- com.dengjk.eclipse.plugin.codegeneration：插件的核心处理代码，包括UI、逻辑处理等
- com.dengjk.eclipse.plugin.codegeneration.feature：用于生成插件的feature

#### 安装教程

1. 下载仓库中的安装包及模板zip（含模板和安装的zip），解压

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150139_19dca748_1018017.png "19.png")

2. Eclipse-->Help-->Install New Software...-->添加site(有点慢)-->Next

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150218_b5e8e250_1018017.png "04.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150233_8b0c2c6a_1018017.png "05.png")

3. 因为是个人的插件，会提示，点击Ok即可，等待安装完成， 重启即可。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150309_01a3d519_1018017.png "07.png")

#### 使用说明

1. 打开Eclipse的window->Preference，切换到Code Generation面板，配置模板路径、Velocity的配置文件（可不配置）、其他信息，保存

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150336_4a11dd5c_1018017.png "09.png")

2. 打开Eclipse的showview->Code Generation，展示DB Browse面板

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150402_e4ee1db1_1018017.png "10.png")

3. 点击DB Browse右上角的新增数据库配置按钮，填写完成数据库配置，点击Finish

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150445_bbe019bd_1018017.png "12.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150503_6b366cff_1018017.png "13.png")

4. 双击配置好的数据库，即展开树形结构

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150529_28b803f9_1018017.png "14.png")

5. 点击表名即在Table Info展示表结构信息

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150604_54c02849_1018017.png "22.png")

6. 选中需要生成代码的表格，右键，Generate Code（表结构有变动，建议refresh）

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150647_456730a5_1018017.png "23.png")

7. 勾选需要生成的代码模板，选择保存位置（可以直接填写，项目不存在的包或路径会自动创建），点击Finish，哈哈....so easy！！！

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/152124_8be17c18_1018017.png "00.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/150755_f95c845a_1018017.png "25.png")

8 **.注意：** 当初始化VolecityContext,vm模板的“-”的第一部分会作为一个key，然后将模板的保存路径放至Context中，以便在vm模板中获取，并且vm文件名会截取掉key部分及.vm，然后以剩余的文件的类型作为生成的文件类型，再通过Context中的变量替换文件名的velocity表达式。如模板： modelPackage-${className}.java.vm，Context会存在：

modelPackage: com.domain.model

className: AdminRole

等信息，则生成文件AdminRole.java,类的包名为com.domain.model，存放包位置com.domain.model


9.当表名称转换类名称和列名称转换属性名称的逻辑与默认配置的存在差异，可以修改模板路径下的NameHandler.java的两个方法即可， **不能修改文件名称、类名和两个方法的名称、参数、返回值类型哦！！** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/153849_a0b76263_1018017.png "30.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1018/153900_a4aeaa38_1018017.png "31.png")


#### 常见问题

1.showview后DB Browse没看到添加配置数据库按钮或配置过的不见了，可以尝试切换至其他面板再切换回来，如console<-->DB Browse

2.数据库新建表后，点击refresh仍然看不到新建的表，可以尝试对配置信息进行编辑，然后测试连接或者关闭后重新连接。

3.代码中文乱码，可以尝试修改Preference中的文件编码或配置Velocity（一个properties文件），可以从velocity-1.7.jar解压后获取到velocity.properties然后修改velocity的相关配置。

4.插件日志查看，在当前Eclipse的工作空间中查看codegeneration-velocity.log和codegeneration.log

#### 最后
 个人对Swing和Eclipse Plugin了解得也不多，整个项目都是基于Tao-AutoDAO项目进行改造的，过程中也遇到各种各样的问题，也收获颇多吧！！！
