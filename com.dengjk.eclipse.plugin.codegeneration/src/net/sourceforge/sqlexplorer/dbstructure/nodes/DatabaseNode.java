/*
 * Copyright (C) 2006 Davy Vanherbergen
 * dvanherbergen@users.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sourceforge.sqlexplorer.dbstructure.nodes;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;

import net.sourceforge.sqlexplorer.dbproduct.MetaDataSession;
import net.sourceforge.squirrel_sql.fw.sql.SQLDatabaseMetaData;

/**
 * Root node for a database. ChildNodes can be filtered based on expressions in
 * the alias.
 * 
 * @author Davy Vanherbergen
 */
public class DatabaseNode extends AbstractNode {

    private List<String> _childNames = new ArrayList<String>();

    private String _databaseProductName = "";

    private boolean _supportsCatalogs = false;

    private boolean _supportsSchemas = false;

    private String _databaseVersion = "";
    

    /**
     * Create a new database node with the given name
     * 
     * @param name
     * @param alias
     */
    public DatabaseNode(String name, MetaDataSession session) throws SQLException {
    	super(name, session);
    	setImageKey("Images.OpenDBIcon");
        
        try {
            SQLDatabaseMetaData metadata = _session.getMetaData();

            if (metadata.supportsCatalogs()) {
                _supportsCatalogs = true;
            }
            if (metadata.supportsSchemas()) {
                _supportsSchemas = true;
            }
            _databaseProductName = metadata.getDatabaseProductName();
            _databaseVersion = "[unknown]";
            try
            {
            	_databaseVersion = "[v"
            		+ metadata.getJDBCMetaData().getDatabaseMajorVersion() + "." 
            		+ metadata.getJDBCMetaData().getDatabaseMinorVersion() 
            		+ "]";
            }
            catch(Throwable ignored)
            {
            	// not all drivers support this (JDBC/ODBC Bridge, DB2)
            }
            
        } catch (AbstractMethodError e) {
            CodeGenerationUIPlugin.error("Error loading database product name.", e);
        }
    }


    /**
     * @return List of catalog nodes
     */
    public List<CatalogNode> getCatalogs() {

        List<CatalogNode> catalogs = new ArrayList<CatalogNode>();

        Iterator<INode> it = getChildIterator();
        while (it.hasNext()) {
            Object o = it.next();
            if (o instanceof CatalogNode) {
                catalogs.add((CatalogNode)o);
            }
        }

        return catalogs;
    }


    public String[] getChildNames() {

        if (_childNames.size() == 0) {
            getChildNodes();
        }
        return (String[]) _childNames.toArray(new String[] {});
    }


    public String getDatabaseProductName() {

        return _databaseProductName;
    }


    /*
     * (non-Javadoc)
     * 
     * @see net.sourceforge.sqlexplorer.dbstructure.nodes.INode#getLabelText()
     */
    public String getLabelText() {

    	String labelTextPrefix = "Connection of " + this._name;
    	String labelText = labelTextPrefix + "(" + _databaseProductName + " " + _databaseVersion + ")";
        return labelText;
    }


    /**
     * @return List of all database schemas
     */
    public List<SchemaNode> getSchemas() {

        ArrayList<SchemaNode> schemas = new ArrayList<SchemaNode>();

        Iterator<INode> it = getChildIterator();
        while (it.hasNext()) {
            Object o = it.next();
            if (o instanceof SchemaNode) {
                schemas.add((SchemaNode)o);
            }
        }

        return schemas;
    }


    /**
     * Returns "database" as the type for this node.
     * 
     * @see net.sourceforge.sqlexplorer.dbstructure.nodes.INode#getType()
     */
    public String getType() {

        return "database";
    }


    /*
     * (non-Javadoc)
     * 
     * @see net.sourceforge.sqlexplorer.dbstructure.nodes.INode#getUniqueIdentifier()
     */
    public String getUniqueIdentifier() {

        return getQualifiedName();
    }

    /**
     * Loads childnodes, filtered to a subset of schemas/databases depending on
     * whether a comma separated list of regular expression filters has been
     * set.
     */
    public void loadChildren() {
    	synchronized(this)
    	{
    		syncLoadChildren();
    	}
    }
    
    private void syncLoadChildren() {

        _childNames = new ArrayList<String>();

        try {
            SQLDatabaseMetaData metadata = _session.getMetaData();

            if (_supportsCatalogs) {

                String[] catalogs = _session.getCatalogs();
                if (catalogs == null || catalogs.length == 0) {
                	if (_supportsSchemas)
                		_supportsCatalogs = false;
                } else {
                	_supportsSchemas = false;
	                for (int i = 0; i < catalogs.length; ++i) {
	                    _childNames.add(catalogs[i]);
	                    addChildNode(new CatalogNode(this, catalogs[i], _session));
	                }
                }

            }
            if (!_supportsCatalogs && _supportsSchemas) {

                final String[] schemas = metadata.getSchemas();
                for (int i = 0; i < schemas.length; ++i) {
                    _childNames.add(schemas[i]);
                    addChildNode(new SchemaNode(this, schemas[i], _session));
                }

            } 
            if (!_supportsCatalogs && !_supportsSchemas) {

                addChildNode(new CatalogNode(this, CodeGenerationSubclipseMessages.getString("NoCatalog_2"), _session));
            }

        } catch (Exception e) {
            CodeGenerationUIPlugin.error("Error loading children", e);
        }

    }


    /**
     * @return true if this database supports catalogs
     */
    public boolean supportsCatalogs() {

        return _supportsCatalogs;
    }


    /**
     * @return true if this database supports schemas
     */
    public boolean supportsSchemas() {

        return _supportsSchemas;
    }

}
