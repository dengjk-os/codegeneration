/*
 * Copyright (C) 2006 Davy Vanherbergen
 * dvanherbergen@users.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package net.sourceforge.sqlexplorer.dbstructure.nodes;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.sqlexplorer.dbproduct.MetaDataSession;
import net.sourceforge.squirrel_sql.fw.sql.ITableInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;

public class SchemaNode extends AbstractNode {

    private List<String> _childNames = new ArrayList<String>();
    
    private static final Log _logger = LogFactory.getLog(SchemaNode.class);


    /**
     * Create new database Schema node.
     * 
     * @param parent node
     * @param name of this node
     * @param sessionNode session for this node
     */
    public SchemaNode(INode parent, String name, MetaDataSession sessionNode) {
    	super(parent, name, sessionNode, "schema");
        setImageKey("Images.SchemaNodeIcon");
    }

    public String[] getChildNames() {

        if (_childNames.size() == 0) {
            getChildNodes();
        }
        return (String[]) _childNames.toArray(new String[] {});
    }

    /*
     * (non-Javadoc)
     * 
     * @see net.sourceforge.sqlexplorer.dbstructure.nodes.INode#getUniqueIdentifier()
     */
    public String getUniqueIdentifier() {

        return getQualifiedName();
    }

    /*
     * (non-Javadoc)
     * 
     * @see net.sourceforge.sqlexplorer.dbstructure.nodes.AbstractNode#loadChildren()
     */
    public void loadChildren() {

        _childNames = new ArrayList<String>();

        try {

            ITableInfo[] tables = null;
            String[] tableTypes = _session.getMetaData().getTableTypes();

            try {
                tables = _session.getMetaData().getTables(_name, _name, "%", tableTypes, null);
            } catch (Throwable e) {
                _logger.debug("Loading all tables at once is not supported");
            }

            for (int i = 0; i < tableTypes.length; ++i) {
                TableFolderNode node = new TableFolderNode(this, tableTypes[i], _session, tables);
                _childNames.add(node.getLabelText());
                addChildNode(node);
            }
        } catch (Throwable e) {

            CodeGenerationUIPlugin.error("Could not load child nodes for " + _name, e);
        }

    }
}
