/*
* (C) 2007-2012 Alibaba Group Holding Limited
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
*
* If you have any question, please contact: 智清、剑笑
* Authors:智清 <zhiqing.ht@taobao.com>；剑笑<jianxiao@taobao.com>
*
*/
package net.sourceforge.sqlexplorer.dbstructure.nodes;

import org.eclipse.swt.graphics.Image;


public class ObjectNode extends AbstractNode {

	private char quoteChar = '"';
	
    public ObjectNode(String name, String type, INode parent, Image image) {
    	super(parent, name, parent.getSession(), type);
        _image = image;
    }
    

    /**
     * This node cannot have childnodes.
     */
    public boolean isEndNode() {
        return true;
    }

    /**
     * This node cannot have childnodes.
     */
    public void loadChildren() {
        return;
    }


    public String getQualifiedName() {
        return quoteChar + getSchemaOrCatalogName() + quoteChar + "." +quoteChar + getName() + quoteChar;
    }


	public char getQuoteChar() {
		return quoteChar;
	}


	public void setQuoteChar(char quoteChar) {
		this.quoteChar = quoteChar;
	}

}
