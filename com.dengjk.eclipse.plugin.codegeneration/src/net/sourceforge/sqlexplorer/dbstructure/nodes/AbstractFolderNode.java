/*
 * Copyright (C) 2007 SQL Explorer Development Team
 * http://sourceforge.net/projects/eclipsesql
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sourceforge.sqlexplorer.dbstructure.nodes;

import net.sourceforge.sqlexplorer.dbproduct.MetaDataSession;

import org.eclipse.swt.graphics.Image;

import com.dengjk.eclipse.plugin.codegeneration.ui.util.ImageUtil;

public abstract class AbstractFolderNode extends AbstractNode {

    public AbstractFolderNode(String name) {
		super(name);
	}

	public AbstractFolderNode(String name, MetaDataSession session) {
		super(name, session);
	}

	public AbstractFolderNode(INode parent, String name, MetaDataSession session, String type) {
		super(parent, name, session, type);
        setImageKey("Images.closedFolder");
        setExpandedImageKey("Images.OpenFolder");
    }

    /**
     * Override this method to change the image that is displayed for this node
     * in the database structure outline.
     */
    public Image getImage() {
        if (get_imageKey() == null)
        	return super.getImage();
        return ImageUtil.getImage(get_imageKey());
    }

    public final String getUniqueIdentifier() {
        return getParent().getName() + '.' + getType();
    }

    public abstract void loadChildren();
}
