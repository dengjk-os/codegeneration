/*
* (C) 2007-2012 Alibaba Group Holding Limited
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
*
* If you have any question, please contact: 智清、剑笑
* Authors:智清 <zhiqing.ht@taobao.com>；剑笑<jianxiao@taobao.com>
*
*/
package net.sourceforge.sqlexplorer.dbproduct;

import java.sql.SQLException;

import net.sourceforge.sqlexplorer.dbstructure.nodes.DatabaseNode;
import net.sourceforge.squirrel_sql.fw.sql.SQLDatabaseMetaData;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.exception.CodeGenerationException;

/**
 * Specialisation of Session which adds meta data; every user has at most one
 * of these, loaded for the first time on demand (which is pretty much always
 * because it's used for detailing catalogs in the editor and for navigating
 * the database structure view)
 * 
 * @author John Spackman
 */
public class MetaDataSession extends Session {
	
	// Cached meta data for this connection
	private SQLDatabaseMetaData metaData;
	
	private String databaseProductName;

	// Cached set of Catalogs for this connection
	private String[] catalogs; 

    // Whether content assist is enabled
    boolean _assistanceEnabled;
    
	public MetaDataSession(User user) throws SQLException {
		super(user);
		setKeepConnection(true);
	}
	
	/**
	 * Initialises the metadata, but only if the meta data has not already been collected
	 */
	private synchronized void initialise() throws SQLException {
		if (metaData != null)
			return;
		
		
		SQLConnection connection = null;
		try {
			connection = grabConnection();
			metaData = connection.getSQLMetaData();
			if (metaData.supportsCatalogs())
			{
				try
				{
					catalogs = metaData.getCatalogs();
				}
				catch(Throwable ex)
				{
					CodeGenerationUIPlugin.error("Error reading catalogs", ex);
					String catalog = connection.getCatalog();
					if(catalog != null)
					{
						catalogs = new String[]{catalog};
					}
				}
			}
			databaseProductName = metaData.getDatabaseProductName();
		}finally {
			if (connection != null)
				releaseConnection(connection);
		}
        
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.sqlexplorer.dbproduct.Session#internalSetConnection(net.sourceforge.sqlexplorer.dbproduct.SQLConnection)
	 */
	@Override
	protected void internalSetConnection(SQLConnection newConnection) throws SQLException {
		super.internalSetConnection(newConnection);
		if (newConnection == null) {
			metaData = null;
		}
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.sqlexplorer.dbproduct.Session#close()
	 */
	@Override
	public synchronized void close() {
		super.close();
		
        // clear detail tab cache
        //DetailTabManager.clearCacheForSession(this);
	}

    /**
     * Gets (and caches) the meta data for this connection
     * @return
     * @throws CodeGenerationException
     */
    public synchronized SQLDatabaseMetaData getMetaData() throws SQLException {
    	initialise();
        return metaData;
    }
    
    /**
     * Returns the catalogs supported by the underlying database, or null
     * if catalogs are not supported
     * @return
     * @throws SQLException
     */
    public String[] getCatalogs() {
    	if (catalogs != null)
    		return catalogs;
    	try {
    		initialise();
    	}catch(SQLException e) {
    		CodeGenerationUIPlugin.error(e);
    		return null;
    	}
   		return catalogs;
    }

    /**
     * Returns the root DatabaseNode for the DatabaseStructureView
     * @return
     */
    public DatabaseNode getRoot() {
    	try {
    		initialise();
    	}catch(SQLException e) {
    		CodeGenerationUIPlugin.error(e);
    		return null;
    	}
    	try {
			DatabaseNode databaseNode = new DatabaseNode(CodeGenerationSubclipseMessages.getString("Database_1"), this);
			return databaseNode;
		} catch (SQLException e) {
    		CodeGenerationUIPlugin.error(e);
			return null;
		}
    }

	/**
	 * @return the databaseProductName
	 */
	public String getDatabaseProductName() throws SQLException {
		if (databaseProductName != null)
			return databaseProductName;
       	initialise();
		return databaseProductName;
	}
}
