/*
 * Copyright (C) 2007 SQL Explorer Development Team
 * http://sourceforge.net/projects/eclipsesql
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sourceforge.sqlexplorer.dbproduct;

import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.service.datalocation.Location;

/**
 * Utility class to provide replacement functions for some locations
 * 
 * @author hhilbert
 *
 */
public class Locations {
	private static final String ECLIPSE_HOME = "${eclipse_home}";
	private static final String WORKSPACE_LOC = "${workspace_loc}";

	static private String getPath(Location pLocation)
	{
		String path = pLocation.getURL().getPath();
		// on win platforms we get /c:/xx and we need c:/xx on linux the path is ok
		int pos = path.indexOf(':');
		if( pos > 0 && pos < 5)
		{
			path = path.substring(1);
		}
		return path;
	}

	/**
	 * expand place holder for workspace location with real path
	 * 
	 * @param pName string to replace place holder with path of known location
	 * @return modified string
	 */
	static public String expandWorkspace(String pName)
	{
		return pName
			.replace(WORKSPACE_LOC, getPath(Platform.getInstanceLocation()))
			;
	}

	/**
	 * expand place holder for known locations with real path
	 * 
	 * @param pName string to replace place holder with path of known location
	 * @return modified string
	 */
	static public String expand(String pName)
	{
		return expandWorkspace(pName)
			.replace(ECLIPSE_HOME, getPath(Platform.getInstallLocation()))
			;
	}

	/**
	 * insert place holder for known locations
	 * 
	 * @param pName string to replace paths of known locations with place holder
	 * @return modified string
	 */
	static public String insert(String pName)
	{
		return pName
			.replace('\\', '/')
			.replace(getPath(Platform.getInstanceLocation()),WORKSPACE_LOC)
			.replace(getPath(Platform.getInstallLocation()),ECLIPSE_HOME)
			;
	}

}
