/*
 * Copyright (C) 2007 SQL Explorer Development Team
 * http://sourceforge.net/projects/eclipsesql
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sourceforge.sqlexplorer.dbproduct;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;

import org.dom4j.Element;
import org.dom4j.tree.DefaultElement;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants;
import com.dengjk.eclipse.plugin.codegeneration.ui.exception.CodeGenerationException;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.XMLUtils;

public class BasicConfigurationManager {
	
	public static final String CONFIGURATION = "configuration";
	public static final String PROJECT = "project";
	public static final String RESOURCE_FOLDER = "resourceFolder";
	public static final String ID = "id";
	public static final String SQLMAP_CONFIG_FILE = "sqlmapConfigFile";
	public static final String DAO_BEAN_CONFIG_FILE = "daoBeanConfigFile";
	public static final String SQLMAP_FILE_FOLDER = "sqlmapFileFolder";
	public static final String DATASOURCE_BEAN_ID = "datasourceBeanID";
	public static final String DATASOURCE_BEAN_DEFINATION_FILE = "datasourceBeanDefinitionFile";
	// List of drivers, indexed by ID
	private HashMap<String, ManagedBasicConfiguration> configurations = new HashMap<String, ManagedBasicConfiguration>();
	
	// Highest ID, used when creating a new unique ID
	private int highestId;
	
	/**
	 * 装载Configurations
	 * @throws CodeGenerationException
	 */
	public void loadConfigurations() throws CodeGenerationException {
		try {
			File file = new File(CodeGenerationUIConstants.USER_CONFIGURATION_FILE_NAME);
			if (!file.exists()) {
				return;
			}
			loadConfigurations(new FileInputStream(file));
		}catch(IOException e) {
			throw new CodeGenerationException("Cannot load user drivers: " + e.getMessage(), e);
		}
	}
	
	/**
	 * Loads configuration definition from a given location
	 * @param input
	 * @throws CodeGenerationException
	 */
	protected void loadConfigurations(InputStream input) throws CodeGenerationException {
		try {
			Element root = XMLUtils.readRoot(input);
			if(root == null)
			{
				throw new CodeGenerationException("Unable to read driver definitions");
			}
			
			for (Element configuration : root.elements(CONFIGURATION)) {
				ManagedBasicConfiguration newConfiguration = new ManagedBasicConfiguration(configuration);
				addConfiguration(newConfiguration);
			}
		}catch(Exception e) {
			throw new CodeGenerationException(e);
		}
	}
	
	/**
	 * Saves the configuration back to disk
	 * @throws CodeGenerationException
	 */
	public void saveConfigurations() throws CodeGenerationException {
		Element root = new DefaultElement(CONFIGURATION);
		for (ManagedBasicConfiguration configuration : configurations.values()){
			Element  element = configuration.describeAsXml();
			if(element!=null){
				root.add(element);
			}
		}
			
		XMLUtils.save(root, new File(CodeGenerationUIConstants.USER_CONFIGURATION_FILE_NAME));
	}
	
	/**
	 * Adds a new configuration
	 * @param driver
	 */
	public void addConfiguration(ManagedBasicConfiguration configuration) {
		if (configuration.getId() == null || configuration.getId().trim().length() == 0)
			throw new IllegalArgumentException("configuration has an invalid ID");
		configurations.remove(configuration.getId());
		configurations.put(configuration.getId(), configuration);
		
		// Try and update our highest ID; if it's not a valid number then we
		//	just ignore it
		try {
			int id = Integer.parseInt(configuration.getId());
			if (id > 0 && id > highestId)
				highestId = id;
		} catch(NumberFormatException e) {
			// Nothing
		}
	}
	
	/**
	 * Removes a driver
	 * @param driver
	 */
	public void removeConfiguration(ManagedBasicConfiguration configuration) {
		configurations.remove(configuration.getId());
	}
	
	public void removeConfiguration(String id) {
		configurations.remove(id);
	}
	
	/**
	 * Returns a driver with a given ID
	 * @param id
	 * @return
	 */
	public ManagedBasicConfiguration getConfiguration(String id) {
		return configurations.get(id);
	}
	
	/**
	 * Returns all the drivers 
	 * @return
	 */
	public Collection<ManagedBasicConfiguration> getConfigurations() {
		return configurations.values();
	}
	
	/**
	 * Allocates a new Unique ID for creating drivers with
	 * @return
	 */
	public String createUniqueId() {
		return Integer.toString(++highestId);
	}
	
}
