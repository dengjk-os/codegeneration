/*
* (C) 2007-2012 Alibaba Group Holding Limited
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
*
* If you have any question, please contact: 智清、剑笑
* Authors:智清 <zhiqing.ht@taobao.com>；剑笑<jianxiao@taobao.com>
*
*/
package net.sourceforge.sqlexplorer.dbproduct;

import org.dom4j.Element;
import org.dom4j.tree.DefaultElement;

public class ManagedBasicConfiguration{
		private String id;
		private String project;
		private String resourceFolder;
		private String sqlMapConfigFile;
		private String daoBeanConfigFile;
		private String sqlMapFileFolder;
		private String dataSourceBeanID;
		private String dataSorceBeanDefinationFile;

		
		public ManagedBasicConfiguration(String id){
			this.id = id;
		}
		
		public ManagedBasicConfiguration(Element root) {
			super();
			id = root.attributeValue(BasicConfigurationManager.ID);
			project = root.elementText(BasicConfigurationManager.PROJECT);
			resourceFolder = root.elementText(BasicConfigurationManager.RESOURCE_FOLDER);
			sqlMapConfigFile = root.elementText(BasicConfigurationManager.SQLMAP_CONFIG_FILE);
			daoBeanConfigFile = root.elementText(BasicConfigurationManager.DAO_BEAN_CONFIG_FILE);
			sqlMapFileFolder = root.elementText(BasicConfigurationManager.SQLMAP_FILE_FOLDER);
			dataSourceBeanID = root.elementText(BasicConfigurationManager.DATASOURCE_BEAN_ID);
			dataSorceBeanDefinationFile = root.elementText(BasicConfigurationManager.DATASOURCE_BEAN_DEFINATION_FILE);
		}
		
		/**
		 * Describes this Configuration in XML; the result can be passed to the constructor
		 * to refabricate it late
		 * @return
		 */
		public Element describeAsXml() {
			if(id==null || id.trim().isEmpty()) return null;
			
			Element root = new DefaultElement(BasicConfigurationManager.CONFIGURATION);
			root.addAttribute(BasicConfigurationManager.ID, id);
			root.addElement(BasicConfigurationManager.PROJECT).setText(project);
			root.addElement(BasicConfigurationManager.RESOURCE_FOLDER).setText(resourceFolder);
			root.addElement(BasicConfigurationManager.SQLMAP_CONFIG_FILE).setText(sqlMapConfigFile);
			root.addElement(BasicConfigurationManager.DAO_BEAN_CONFIG_FILE).setText(daoBeanConfigFile);
			root.addElement(BasicConfigurationManager.SQLMAP_FILE_FOLDER).setText(sqlMapFileFolder);
			root.addElement(BasicConfigurationManager.DATASOURCE_BEAN_ID).setText(dataSourceBeanID);
			root.addElement(BasicConfigurationManager.DATASOURCE_BEAN_DEFINATION_FILE).setText(dataSorceBeanDefinationFile);
			return root;
		}

		public String getProject() {
			return project;
		}
		public void setProject(String project) {
			this.project = project;
		}
		public String getResourceFolder() {
			return resourceFolder;
		}
		public void setResourceFolder(String resourceFolder) {
			this.resourceFolder = resourceFolder;
		}
		public String getSqlMapConfigFile() {
			return sqlMapConfigFile;
		}
		public void setSqlMapConfigFile(String sqlMapConfigFile) {
			this.sqlMapConfigFile = sqlMapConfigFile;
		}
		public String getDaoBeanConfigFile() {
			return daoBeanConfigFile;
		}
		public void setDaoBeanConfigFile(String daoBeanConfigFile) {
			this.daoBeanConfigFile = daoBeanConfigFile;
		}
		public String getSqlMapFileFolder() {
			return sqlMapFileFolder;
		}
		public void setSqlMapFileFolder(String sqlMapFileFolder) {
			this.sqlMapFileFolder = sqlMapFileFolder;
		}
		public String getDataSourceBeanID() {
			return dataSourceBeanID;
		}
		public void setDataSourceBeanID(String dataSourceBeanID) {
			this.dataSourceBeanID = dataSourceBeanID;
		}
		public String getDataSorceBeanDefinationFile() {
			return dataSorceBeanDefinationFile;
		}
		public void setDataSorceBeanDefinationFile(String dataSorceBeanDefinationFile) {
			this.dataSorceBeanDefinationFile = dataSorceBeanDefinationFile;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
	}