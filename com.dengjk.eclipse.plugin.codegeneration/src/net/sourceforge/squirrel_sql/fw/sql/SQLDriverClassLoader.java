/*
* (C) 2007-2012 Alibaba Group Holding Limited
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
*
* If you have any question, please contact: 智清、剑笑
* Authors:智清 <zhiqing.ht@taobao.com>；剑笑<jianxiao@taobao.com>
*
*/
package net.sourceforge.squirrel_sql.fw.sql;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.util.List;

import net.sourceforge.sqlexplorer.dbproduct.ManagedDriver;
import net.sourceforge.squirrel_sql.fw.util.MyURLClassLoader;
import net.sourceforge.squirrel_sql.fw.util.log.ILogger;

/**
 * 类说明:SQLDriverClassLoader
 * 
 * @author 剑笑<a href="mailto:jianxiao@taobao.com">&lt;jianxiao@taobao.com&gt;</a>.
 * 创建时间：2011-03-13
 */
public class SQLDriverClassLoader extends MyURLClassLoader
{
	public SQLDriverClassLoader(ClassLoader parent, ManagedDriver sqlDriver) throws MalformedURLException
	{
		super(parent, createURLs(sqlDriver.getJars()));
	}

	public SQLDriverClassLoader(ClassLoader parent, URL url)
	{
		super(parent, url);
	}
	
	public SQLDriverClassLoader(ClassLoader parent, URL[] urls)
	{
		super(parent, urls);
	}

	public Class<?>[] getDriverClasses(ILogger logger)
	{
		return getAssignableClasses(Driver.class, logger);
	}

	private static URL[] createURLs(List<String> fileNames) throws MalformedURLException {
		URL[] urls;
		if (fileNames == null)
			urls = new URL[0];
		else {
			urls = new URL[fileNames.size()];
			int i = 0;
			for (String fileName : fileNames)
				urls[i++] = new File(fileName).toURI().toURL();
		}
		return urls;
	}
}
