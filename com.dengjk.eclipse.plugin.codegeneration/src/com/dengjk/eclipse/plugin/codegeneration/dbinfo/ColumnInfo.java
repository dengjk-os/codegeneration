package com.dengjk.eclipse.plugin.codegeneration.dbinfo;

public class ColumnInfo {
	
	/**主键*/
	private boolean isPrimaryKey;
	
	/**索引*/
	private boolean isIndexKey;
	
	/** 字段名称 */
	private String columnName;
	
	/** 字段类型 */
	private int dataType;
	
	private String typeName;
	
	private int columnSize;
	
	private int decimalDigits;
	
	private int radix;
	
	private int isNullAllowed;
	
	private String defaultValue;
	
	private int octetLength;
	
	private int ordinalPosition;
	
	private String isNullable;

	/** 列描述 */
	private String columnComment;

	/**属性类型 */
	private String attrType;
	
	/**处理后的属性名*/
    private String attrName;
	
	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}

	public void setPrimaryKey(boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	public boolean isIndexKey() {
		return isIndexKey;
	}

	public void setIndexKey(boolean isIndexKey) {
		this.isIndexKey = isIndexKey;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public int getColumnSize() {
		return columnSize;
	}

	public void setColumnSize(int columnSize) {
		this.columnSize = columnSize;
	}

	public int getDecimalDigits() {
		return decimalDigits;
	}

	public void setDecimalDigits(int decimalDigits) {
		this.decimalDigits = decimalDigits;
	}

	public int getRadix() {
		return radix;
	}

	public void setRadix(int radix) {
		this.radix = radix;
	}

	public int getIsNullAllowed() {
		return isNullAllowed;
	}

	public void setIsNullAllowed(int isNullAllowed) {
		this.isNullAllowed = isNullAllowed;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public int getOctetLength() {
		return octetLength;
	}

	public void setOctetLength(int octetLength) {
		this.octetLength = octetLength;
	}

	public int getOrdinalPosition() {
		return ordinalPosition;
	}

	public void setOrdinalPosition(int ordinalPosition) {
		this.ordinalPosition = ordinalPosition;
	}

	public String getIsNullable() {
		return isNullable;
	}

	public void setIsNullable(String isNullable) {
		this.isNullable = isNullable;
	}

	public String getColumnComment() {
		return columnComment;
	}

	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}

	public String getAttrType() {
		return attrType;
	}

	public void setAttrType(String attrType) {
		this.attrType = attrType;
	}

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	
}
