package com.dengjk.eclipse.plugin.codegeneration.dbinfo;

import java.util.List;

public class TableInfo {

	/** 表名称 */
    private String tableName;

    /** 表描述 */
    private String tableComment;

    /** 表的列名 */
    private List<ColumnInfo> columns;
    
    /**处理后的类名*/
    private String className;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableComment() {
		return tableComment;
	}

	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}
	
	public List<ColumnInfo> getColumns() {
		return columns;
	}

	public void setColumns(List<ColumnInfo> columns) {
		this.columns = columns;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

}