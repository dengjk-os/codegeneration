package com.dengjk.eclipse.plugin.codegeneration.dbinfo.handler;

public class IrcloudNameHandler implements NameHandler {

	private static final char UNDERLINE = '_';

	private static String getCamelCaseString(String inputString) {
		int len = inputString.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = inputString.charAt(i);
			if (c == UNDERLINE) {
				if (++i < len) {
					sb.append(Character.toUpperCase(inputString.charAt(i)));
				}
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	@Override
	public String handlerTableName(String tableName) {
		tableName = tableName.substring(2);
		tableName = Character.toUpperCase(tableName.charAt(0)) + tableName.substring(1);
		return getCamelCaseString(tableName);
	}

	@Override
	public String handlerColumnName(String columnName) {
		return columnName.substring(1);
	}

}
