package com.dengjk.eclipse.plugin.codegeneration.dbinfo.handler;

import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.dengjk.eclipse.plugin.codegeneration.dbinfo.ColumnInfo;
import com.dengjk.eclipse.plugin.codegeneration.dbinfo.TableInfo;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.exception.CodeGenerationException;
import com.dengjk.eclipse.plugin.codegeneration.utils.FilePathUtils;

public class NameHandlerHelper {
	
	private static volatile Object instance;

	private static void initHandlerInstance(String javaFilePath) throws CodeGenerationException{
		if(instance != null){
			return;
		}
		synchronized (NameHandlerHelper.class) {
			if(instance != null){
				return;
			}
			final ClassLoader oldContextClassLoader = Thread.currentThread().getContextClassLoader();
			Thread.currentThread().setContextClassLoader(NameHandlerHelper.class.getClassLoader());
			
			if (javaFilePath == null || javaFilePath.trim().length() == 0) {
				instance =  new IrcloudNameHandler();
				return;
			}
			try {
				// 动态编译
				com.sun.tools.javac.Main javac = new com.sun.tools.javac.Main();
				String folder = FilePathUtils.getFolderPath(javaFilePath);
				String fileName = FilePathUtils.getFileName(javaFilePath);
				fileName = fileName.replace(".java", "");
				String[] xargs = new String[] {"-sourcepath", folder,"-d", folder,javaFilePath};
				// 编译校验
				int status = javac.compile(xargs);
				if (status != 0) {
					throw new RuntimeException("compiler NameHandler.java failure !!!,source file:"+fileName);
				}
				
				// 自定义加载器，加载外部类
			    URL resource = new URL("file:"+folder+"/");
			    ClassLoader nameClassLoader = new URLClassLoader( new URL[] {resource},ClassLoader.getSystemClassLoader());
				Class<?> clz = nameClassLoader.loadClass(fileName);
				
				// 创建实例
				instance = clz.newInstance();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			// set back default class loader
		    Thread.currentThread().setContextClassLoader(oldContextClassLoader);
		}
	}
	
	public static void handlerName(String javaFilePath,TableInfo tableInfo)throws CodeGenerationException{
		initHandlerInstance(javaFilePath);
		Class<?> clazz = instance.getClass();
		try {
			Method handlerTableNameMethod = clazz.getDeclaredMethod("handlerTableName",String.class);
			String className = (String) handlerTableNameMethod.invoke(instance,new Object[]{tableInfo.getTableName()});
			tableInfo.setClassName(className);
			
			List<ColumnInfo> columns = tableInfo.getColumns();
			if(columns != null){
				Method handlerColumnNameMethod = clazz.getDeclaredMethod("handlerColumnName",String.class);
				for(ColumnInfo column : columns){
					String attrName = (String) handlerColumnNameMethod.invoke(instance,new Object[]{column.getColumnName()});
					column.setAttrName(attrName);
				}
			}
			
		} catch (Exception e) {
			throw new CodeGenerationException(CodeGenerationSubclipseMessages.getString("WARNING_HANDLER_METHOD_INFO"),e);
		}
	}
	
}
