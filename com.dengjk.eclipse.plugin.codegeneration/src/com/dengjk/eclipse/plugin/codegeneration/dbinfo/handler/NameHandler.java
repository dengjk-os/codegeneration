package com.dengjk.eclipse.plugin.codegeneration.dbinfo.handler;

/**
 * 
 * 表、列名称处理
 *
 */
public interface NameHandler {

	String handlerTableName(String tableName);
	
	String handlerColumnName(String columnName);
}
