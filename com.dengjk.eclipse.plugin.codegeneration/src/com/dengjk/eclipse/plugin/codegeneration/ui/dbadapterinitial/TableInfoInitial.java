package com.dengjk.eclipse.plugin.codegeneration.ui.dbadapterinitial;

import java.util.List;

import com.dengjk.eclipse.plugin.codegeneration.dbinfo.ColumnInfo;
import com.dengjk.eclipse.plugin.codegeneration.dbinfo.TableInfo;

import net.sourceforge.sqlexplorer.dbstructure.nodes.TableNode;
import net.sourceforge.squirrel_sql.fw.sql.ITableInfo;

public class TableInfoInitial {

	public static TableInfo buildTableInfo(TableNode tableNode){
		if(tableNode == null){
			return null;
		}
		TableInfo tableInfo = new TableInfo();
		
		ITableInfo info = tableNode.getTableInfo();
		
		tableInfo.setTableName(info.getSimpleName());
		tableInfo.setTableComment(info.getRemarks());
		
		
		List<ColumnInfo> columns = TableAndColumnInfoInitial.getColumnInfosFromTableNode(tableNode);
		tableInfo.setColumns(columns);
		
		return tableInfo;
	}
	
	
}
