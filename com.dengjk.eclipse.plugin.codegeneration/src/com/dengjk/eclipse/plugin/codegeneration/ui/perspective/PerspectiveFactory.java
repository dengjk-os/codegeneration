
package com.dengjk.eclipse.plugin.codegeneration.ui.perspective;

import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewDBBrowse;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewGennerateList;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewTableInfo;

/**
 * 类说明:codegeneration透视图
 * 
 */
public class PerspectiveFactory implements IPerspectiveFactory {
	
	public static String ID_AUTODAO_DB_BROWSE = ViewDBBrowse.class.getName();
	public static String ID_AUTODAO_TABLE_INFO = ViewTableInfo.class.getName();
	public static String ID_AUTODAO_GENNERATE_LIST = ViewGennerateList.class.getName();

	@Override
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		String leftLayOutName = "autodao.autodaoLeft";
		String bottomLayOutName = "autodao.autodaoBottom";
		
		//添加视图
		IFolderLayout autodaoLeft = layout.createFolder(leftLayOutName, IPageLayout.LEFT, 0.25f, editorArea);
		autodaoLeft.addView(ID_AUTODAO_DB_BROWSE);

		IFolderLayout autodaoProjectExplorer = layout.createFolder("autodaoProjectExplorer", IPageLayout.BOTTOM, 0.5f, leftLayOutName);
		//autodaoProjectExplorer.addView(IPageLayout.ID_PROJECT_EXPLORER);
		autodaoProjectExplorer.addView(JavaUI.ID_PACKAGES);
		autodaoProjectExplorer.addView(IPageLayout.ID_OUTLINE);
		
		IFolderLayout autoDAOBottom = layout.createFolder(bottomLayOutName, IPageLayout.BOTTOM, 0.6f, editorArea);
		autoDAOBottom.addView(ID_AUTODAO_TABLE_INFO);
		autoDAOBottom.addView(ID_AUTODAO_GENNERATE_LIST);
		autoDAOBottom.addView(IPageLayout.ID_PROBLEM_VIEW);
		
		
	}

}
