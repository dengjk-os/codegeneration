package com.dengjk.eclipse.plugin.codegeneration.ui.dbadapterinitial;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.dengjk.eclipse.plugin.codegeneration.dbinfo.ColumnInfo;

import net.sourceforge.squirrel_sql.fw.sql.TableColumnInfo;

public class TableColumnInfoAdapter {

	private static ColumnInfo adpateTableColumnInfo( TableColumnInfo tableColumnInfo ){
		if( null == tableColumnInfo ){
			return null;
		}
		ColumnInfo tbTableColumnInfo = new ColumnInfo();
		try {
			BeanUtils.copyProperties(tbTableColumnInfo, tableColumnInfo);
			tbTableColumnInfo.setColumnComment(tableColumnInfo.getRemarks());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return tbTableColumnInfo;
	}

	public static List<ColumnInfo> adpateTableColumnInfos(TableColumnInfo[] tableColumnInfos ){
		if( null == tableColumnInfos || tableColumnInfos.length == 0 ){
			return null;
		}
		
		List<ColumnInfo> tbTableColumnInfos = new ArrayList<ColumnInfo>();
		for( TableColumnInfo tableColumnInfo : tableColumnInfos ){
			ColumnInfo tbTableColumnInfo = adpateTableColumnInfo(tableColumnInfo);
			tbTableColumnInfos.add(tbTableColumnInfo);
		}
		
		return tbTableColumnInfos;
	}

	public static List<ColumnInfo> adpateTableColumnInfoList( List<TableColumnInfo> tableColumnInfoList ){
		if( null == tableColumnInfoList ){
			return null;
		}
		List<ColumnInfo> tbTableColumnInfoList = new ArrayList<ColumnInfo>();
		for( TableColumnInfo tableColumnInfo : tableColumnInfoList ){
			ColumnInfo tbTableColumnInfo = adpateTableColumnInfo(tableColumnInfo);
			if( null != tbTableColumnInfo ){
				tbTableColumnInfoList.add(tbTableColumnInfo);
			}
		}
		
		return tbTableColumnInfoList;
	}
	
	public static ColumnInfo[] adpateTableColumnInfoListToArray( List<TableColumnInfo> tableColumnInfoList ){
		List<ColumnInfo> tbTableColumnInfoList = adpateTableColumnInfoList(tableColumnInfoList);
		if( null == tbTableColumnInfoList ){
			return null;
		}
		return tbTableColumnInfoList.toArray(new ColumnInfo[tableColumnInfoList.size()] );
	}
	
}
