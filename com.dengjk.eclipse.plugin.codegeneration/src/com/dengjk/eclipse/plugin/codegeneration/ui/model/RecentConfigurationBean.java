package com.dengjk.eclipse.plugin.codegeneration.ui.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * 类说明:最近的设值
 * 
 */
public class RecentConfigurationBean implements Serializable{
    
    private static final long serialVersionUID = 89298457895993412L;

    private String javaProjectName;
    
    private String srcFolder;
    
    private String templateSavePaths;
    
    
    public RecentConfigurationBean(){
    }

	public RecentConfigurationBean(String javaProjectName, String srcFolder,String templateSavePaths) {
		super();
		this.javaProjectName = javaProjectName;
		this.srcFolder = srcFolder;
		this.templateSavePaths = templateSavePaths;
	}

	public String getJavaProjectName() {
		return javaProjectName;
	}

	public void setJavaProjectName(String javaProjectName) {
		this.javaProjectName = javaProjectName;
	}

	public String getSrcFolder() {
		return srcFolder;
	}

	public void setSrcFolder(String srcFolder) {
		this.srcFolder = srcFolder;
	}

	public String getTemplateSavePaths() {
		return templateSavePaths;
	}

	public void setTemplateSavePaths(String templateSavePaths) {
		this.templateSavePaths = templateSavePaths;
	}

	public HashMap<String,String> getSavePathMap(){
		if(templateSavePaths != null && templateSavePaths.trim().length() > 0){
			HashMap<String,String> pathMap = new HashMap<String,String>();
			String[] paths = templateSavePaths.split(";");
			if(paths == null){
				return pathMap;
			}
			for(String path : paths){
				String[] pathTemps = path.split("&");
				if(pathTemps != null && pathTemps.length >= 2){
					pathMap.put(pathTemps[0], pathTemps[1]);
				}
			}
			return pathMap;
		}
		return null;
	}
	
}
