package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import net.sourceforge.sqlexplorer.dbproduct.Alias;
import net.sourceforge.sqlexplorer.dbproduct.AliasManager;
import net.sourceforge.sqlexplorer.dbproduct.BasicConfigurationManager;
import net.sourceforge.sqlexplorer.dbproduct.DriverManager;
import net.sourceforge.sqlexplorer.dbproduct.ManagedBasicConfiguration;
import net.sourceforge.sqlexplorer.dbproduct.ManagedDriver;
import net.sourceforge.sqlexplorer.dbproduct.User;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Shell;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes.ManageDriverNode;
import com.dengjk.eclipse.plugin.codegeneration.ui.exception.SQLCannotConnectException;

/**
 * 类说明:DB Browse 视图 右键后拷贝driver菜单
 * 
 */
public class CopyDriverAction extends AbstractDBTreeContextAction {
	
    private static final ImageDescriptor _image = CodeGenerationUIPlugin.getImageDescriptor(CodeGenerationSubclipseMessages.getString("Images.CopyAlias"));
    
    private Shell shell;
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
        return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.CopyDriver");
    }

    public String getToolTipText() {
    	return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.CopyDriver");
    }
    
    public CopyDriverAction(Shell shell){
    	this.shell = shell;
    }
	
	public void run(){
		if(isAvailable()){
			String newName = null;
			String initValue = "CopyOf" + _selectedNodes[0].getName();
			InputDialog inputDialog = new InputDialog(null,
					CodeGenerationSubclipseMessages.getString("CONFIRM_COPY"),
					CodeGenerationSubclipseMessages.getString("CONFIRM_NEWLABEL"),
					initValue,null);
			if(inputDialog.open()==InputDialog.OK){
				newName = inputDialog.getValue();
				if(newName==null || newName.trim().isEmpty()){
					MessageDialog.openInformation(null, 
							CodeGenerationSubclipseMessages.getString("CONFIRM_TIP"), 
							CodeGenerationSubclipseMessages.getString("CONFIRM_NEWLABEL"));
				}else if(newName.trim().equals(_selectedNodes[0].getName())){
					MessageDialog.openInformation(null, 
							CodeGenerationSubclipseMessages.getString("CONFIRM_TIP"),
							CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_LABELSAME"));
				}else{
					//保存driver、Alias、configuration
					DriverManager driverManager = CodeGenerationUIPlugin.getDefault().getDriverModel();
			        AliasManager aliasManager = CodeGenerationUIPlugin.getDefault().getAliasManager();
			        BasicConfigurationManager  configurationManager = CodeGenerationUIPlugin.getDefault().getConfigurationManager();
			        
			        Alias oldAlias = aliasManager.getAlias(_selectedNodes[0].getName());
			        ManagedDriver oldManagedDriver = oldAlias.getDriver();
			        ManagedBasicConfiguration  oldConfiguration = configurationManager.getConfiguration(_selectedNodes[0].getName());
			        if(oldAlias!=null && oldManagedDriver!=null ){
			        	 	ManagedDriver managedDriver = new ManagedDriver(newName);
					        managedDriver.setDriverClassName(oldManagedDriver.getDriverClassName());
					        managedDriver.setName(newName);
					        managedDriver.setJars(oldManagedDriver.getJars());

							Alias alias = new Alias();
					        alias.setName(managedDriver.getId());
					        alias.setDriver(managedDriver);
					        alias.setUrl(oldAlias.getUrl());
					        User userAlias = new User( oldAlias.getDefaultUser().getUserName(), oldAlias.getDefaultUser().getPassword());
					        alias.setHasNoUserName(false);
					        alias.setDefaultUser(userAlias);
					        alias.setConnectAtStartup(false);
					        alias.setAutoLogon(true);
					        
							try {
					            driverManager.addDriver(managedDriver);
							} catch (Exception e) {
							}
							
							try {
								aliasManager.addAlias(alias);
							} catch (Exception e) {
							}
					        
					        try {
					        	CodeGenerationUIPlugin.getDefault().getDatabaseStructureView().addManagedDriver(managedDriver);
							} catch (SQLCannotConnectException e1) {
								e1.printStackTrace();
							}
					        
					        if( oldConfiguration!=null ){
						        ManagedBasicConfiguration configuration = new ManagedBasicConfiguration(newName);
						        configuration.setDaoBeanConfigFile(oldConfiguration.getDaoBeanConfigFile());
						        configuration.setDataSorceBeanDefinationFile(oldConfiguration.getDataSorceBeanDefinationFile());
						        configuration.setDataSourceBeanID(oldConfiguration.getDataSourceBeanID());
						        configuration.setProject(oldConfiguration.getProject());
						        configuration.setResourceFolder(oldConfiguration.getResourceFolder());
						        configuration.setSqlMapConfigFile(oldConfiguration.getSqlMapConfigFile());
						        configuration.setSqlMapFileFolder(oldConfiguration.getSqlMapFileFolder());
								
								try {
									configurationManager.addConfiguration(configuration);
								} catch (Exception e) {
								}
					        }
			        }
			       
				}
			}
		}

		return ;		
	}
	
    public boolean isAvailable() {

        if (null == _selectedNodes || _selectedNodes.length == 0) {
            return true;
        }
        if (_selectedNodes[0] instanceof ManageDriverNode) {
            return true;
        }
        return false;
    }

}
