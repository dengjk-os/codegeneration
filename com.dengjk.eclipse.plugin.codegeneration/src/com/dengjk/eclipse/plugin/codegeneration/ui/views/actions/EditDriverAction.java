package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes.ManageDriverNode;
import com.dengjk.eclipse.plugin.codegeneration.ui.wizards.WizardDialogCodeGenerationLocation;
import com.dengjk.eclipse.plugin.codegeneration.ui.wizards.driver.NewDriverWizard;

/**
 * 类说明:DB Browse 视图 右键后修改driver菜单
 * 
 */
public class EditDriverAction extends AbstractDBTreeContextAction {
	
    private static final ImageDescriptor _image = CodeGenerationUIPlugin.getImageDescriptor(CodeGenerationSubclipseMessages.getString("Images.EditDriver"));
    
    private Shell shell;
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
        return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.EditDriver");
    }

    public String getToolTipText() {
    	return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.EditDriver");
    }
    
    public EditDriverAction(Shell shell){
    	this.shell = shell;
    }
	
	public void run(){
		NewDriverWizard wizard = new NewDriverWizard();
		wizard.setEdit(true);
		wizard.setNeedsProgressMonitor(true);
		if(isAvailable()){
			wizard.setCurrentProjectID(_selectedNodes[0].getName());
		}
		WizardDialog wizardDialog = new WizardDialogCodeGenerationLocation(shell, wizard, 600, 650); //$NON-NLS-1$
		wizardDialog.setMinimumPageSize(350, 500);
		wizardDialog.open();
		return ;		
	}
	
    public boolean isAvailable() {

        if (null == _selectedNodes || _selectedNodes.length == 0) {
            return true;
        }
        if (_selectedNodes[0] instanceof ManageDriverNode) {
            return true;
        }
        return false;
    }

}
