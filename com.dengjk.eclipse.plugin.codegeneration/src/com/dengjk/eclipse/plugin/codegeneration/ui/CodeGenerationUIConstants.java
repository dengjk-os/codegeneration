package com.dengjk.eclipse.plugin.codegeneration.ui;

import java.io.File;

public class CodeGenerationUIConstants {
    
    
    /**
     * 公用变量
     */
    
    public static final String CHARACTER_ENCODING = "UTF-8";

    public static final int BUFFER_SIZE_DEFAULT = 30000;
    
    public static final String DATA_FORMATEER_1 = "yyyy-MM-dd HH:mm:ss";
    
    public static final String DATA_FORMATEER_2 = "yyyy-MM-dd";
    
    /**空字符串*/
    public static final String EMPTY_STRING = "";
    
	/** 文本中的回车符*/
	public static final String LINE_BR = "\r\n";
	
	/** 必填项的标识*/
	public static final String FIELD_REQUIRED = "*";
    
    ///////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * 数据库连接相关
     */
    
    /** 记录版本信息；为保证各版本兼容，发行的各个版本可能存储的文件名不一样. */
    public static final String VERSION_CURRENT = "1.0.0-1";

    /** User Setting的存储路径. */
    public static final String USER_SETTINGS_FOLDER = CodeGenerationUIPlugin.getDefault().getStateLocation().toFile().getAbsolutePath();

    /** aliases的存储路径. */
    public static final String USER_ALIAS_FILE_NAME = USER_SETTINGS_FOLDER + File.separator + "CodeGenerationAliases" + VERSION_CURRENT + ".xml";
    
    /** Drivers的存储路径. */
    public static final String USER_DRIVER_FILE_NAME = USER_SETTINGS_FOLDER + File.separator + "CodeGenerationDrivers" + VERSION_CURRENT + ".xml";
    
    /** basic configuration的存储路径. */
    public static final String USER_CONFIGURATION_FILE_NAME = USER_SETTINGS_FOLDER + File.separator + "CodeGenerationConfigurations" + VERSION_CURRENT + ".xml";
    
    public static final String AUTO_COMMIT = "CodeGeneration.AutoCommit";
    
    public static final String COMMIT_ON_CLOSE = "CodeGeneration.CommitOnClose";
    
    /** Show schema name with table name */
    public static final String SHOW_SCHEMA_ON_TABLES = "CodeGeneration.ShowSchemaOnTables";
    
}
