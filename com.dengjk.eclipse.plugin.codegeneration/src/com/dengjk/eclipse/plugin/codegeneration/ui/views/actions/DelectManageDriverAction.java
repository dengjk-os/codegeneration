package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import net.sourceforge.sqlexplorer.dbproduct.ManagedDriver;
import net.sourceforge.sqlexplorer.dbstructure.nodes.DatabaseNode;
import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes.ManageDriverNode;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.ImageUtil;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewDBBrowse;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewTableInfo;

/**
 * 类说明:DB Browse 视图 右键后Delete驱动连接的菜单
 * 
 */
public class DelectManageDriverAction extends AbstractDBTreeContextAction {

    private static final ImageDescriptor _image = ImageUtil.getDescriptor("Images.DeleteDriverNode");
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
        return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.Delete");
    }

    public void run() {
    	
    	if(!MessageDialog.openConfirm(null, "提示", "确认删除该Driver吗？该操作将永久删除该Driver!")){
    		return ;
    	}
        
        ViewDBBrowse structureView = CodeGenerationUIPlugin.getDefault().getDatabaseStructureView();
        
        for (int i = 0; i < _selectedNodes.length; i++) {
        	ManageDriverNode manageDriverNode = (ManageDriverNode)_selectedNodes[i];
        	structureView.getRootNodeViewDBBrowse().removeChildNode(_selectedNodes[i]);
        	ManagedDriver managedDriver = manageDriverNode.getManagerDriver();
        	if( null != managedDriver ){
            	CodeGenerationUIPlugin.getDefault().getDriverModel().removeDriver( managedDriver );
            	CodeGenerationUIPlugin.getDefault().getAliasManager().removeAlias( managedDriver.getId() );
            	CodeGenerationUIPlugin.getDefault().getConfigurationManager().removeConfiguration(managedDriver.getId());
            	structureView.getAllManagedDrivers().remove( managedDriver );
        	}
        	INode[] nodes = manageDriverNode.getChildNodes();
        	//关闭数据库连接，删除子节点
        	if( null != nodes && nodes.length > 0 ){
            	for( INode node : nodes ){
            		DatabaseNode databaseNode = (DatabaseNode)node;
            		try {
						databaseNode.getSession().close();
					} catch (Exception e) {
						e.printStackTrace();
					}
            		((ManageDriverNode)_selectedNodes[i]).removeChildNode(databaseNode);
            	}
        	}
        }

        _treeViewer.refresh();

        // refresh detail view
        ViewTableInfo detailView = (ViewTableInfo) CodeGenerationUIPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(ViewTableInfo.class.getName());
        structureView.getTreeViewer().synchronizeDetailView(detailView);
    }

    public boolean isAvailable() {

        if (null == _selectedNodes || _selectedNodes.length == 0) {
            return true;
        }
        if (_selectedNodes[0] instanceof ManageDriverNode) {
            return true;
        }
        return false;
    }
}
