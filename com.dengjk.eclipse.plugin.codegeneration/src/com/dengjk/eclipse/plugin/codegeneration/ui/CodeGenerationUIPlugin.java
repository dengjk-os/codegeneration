package com.dengjk.eclipse.plugin.codegeneration.ui;

import net.sourceforge.sqlexplorer.dbproduct.AliasManager;
import net.sourceforge.sqlexplorer.dbproduct.BasicConfigurationManager;
import net.sourceforge.sqlexplorer.dbproduct.DriverManager;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jdt.ui.IPackagesViewPart;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewDBBrowse;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewGennerateList;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewTableInfo;

public class CodeGenerationUIPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "com.dengjk.eclipse.plugin.codegeneration"; //$NON-NLS-1$
    
	// The shared instance
	private static CodeGenerationUIPlugin plugin;

    private DriverManager driverManager;

    private AliasManager aliasManager;
    
    private ViewDBBrowse databaseStructureView ;
    
    private ViewGennerateList viewGennerateList;
    
    private ViewTableInfo tableInfoView;
    
    private BasicConfigurationManager configurationManager;
    
	public CodeGenerationUIPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;

        driverManager = new DriverManager();
		try {
	        driverManager.loadDrivers();
		} catch (Exception e) {
		}
        aliasManager = new AliasManager();
        try {
            aliasManager.loadAliases();
		} catch (Exception e) {
		}
		//基本配置信息load
		configurationManager = new BasicConfigurationManager();
        try {
        	configurationManager.loadConfigurations();
		} catch (Exception e) {
		}
	}

    /**
     * Get the version number as specified in plugin.xml
     * 
     * @return version number of SQL Explorer plugin
     */
    public String getVersion() {
        String version = (String) plugin.getBundle().getHeaders().get(org.osgi.framework.Constants.BUNDLE_VERSION);
        return version;
    }

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		try {
			driverManager.saveDrivers();
		} catch (Exception e) {
		}
        try {
			aliasManager.saveAliases();
		} catch (Exception e) {
		}
        try {
			aliasManager.closeAllConnections();
		} catch (Exception e) {
		}
		//基本配置信息save
		 try {
			 configurationManager.saveConfigurations();
		} catch (Exception e) {
		}
		
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static CodeGenerationUIPlugin getDefault() {
		return plugin;
	}
    
    public static CodeGenerationUIPlugin getPlugin() {
        return plugin;
    }
	
	public ViewDBBrowse getDatabaseStructureView() {
		IWorkbenchPage page = getActivePage();
		if (page != null) {
			databaseStructureView = (ViewDBBrowse) page.findView(ViewDBBrowse.class.getName());
	        if (databaseStructureView == null)
	        	try {
	        		databaseStructureView = (ViewDBBrowse)page.showView(ViewDBBrowse.class.getName());
	        	} catch(PartInitException e) {
	        		error(e);
	        	}
		}
		return databaseStructureView;
	}

	public ViewGennerateList getViewGennerateList() {
		IWorkbenchPage page = getActivePage();
		if (page != null) {
			viewGennerateList = (ViewGennerateList) page.findView(ViewGennerateList.class.getName());
	        if (viewGennerateList == null)
	        	try {
	        		viewGennerateList = (ViewGennerateList)page.showView(ViewGennerateList.class.getName());
	        	} catch(PartInitException e) {
	        		error(e);
	        	}
		}
		return viewGennerateList;
	}
	
	public ViewTableInfo getTableInfoView() {
		IWorkbenchPage page = getActivePage();
		if (page != null) {
			tableInfoView = (ViewTableInfo) page.findView(ViewTableInfo.class.getName());
	        if (tableInfoView == null)
	        	try {
	        		tableInfoView = (ViewTableInfo)page.showView(ViewTableInfo.class.getName());
	        	} catch(PartInitException e) {
	        		error(e);
	        	}
		}
		return tableInfoView;
	}
	
	public IPackagesViewPart getPackagesView() {
		IPackagesViewPart fPackageExplorerPart = null;
		if (fPackageExplorerPart == null) {
			IWorkbenchPage page = getActivePage();
			if (page != null) {
				fPackageExplorerPart = (IPackagesViewPart) page.findView(JavaUI.ID_PACKAGES);
		        if (fPackageExplorerPart == null)
		        	try {
		        		fPackageExplorerPart = (IPackagesViewPart)page.showView(JavaUI.ID_PACKAGES);
		        	} catch(PartInitException e) {
		        	}
			}
		}
		return fPackageExplorerPart;
	}
	
	private IWorkbenchPage getActivePage() {
		if (getWorkbench() != null && getWorkbench().getActiveWorkbenchWindow() != null)
			return getWorkbench().getActiveWorkbenchWindow().getActivePage();
		return null;
    }
	
	public static void error(String message) {
        getDefault().getLog().log(new Status(IStatus.ERROR, PLUGIN_ID,message));
    }

	public static void error(String message, Throwable t) {
        getDefault().getLog().log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, String.valueOf(message), t));
    }
	
	public static void error(Throwable t) {
        error( t.getMessage(), t);
    }

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		/*URL imageURL = getBundle().getEntry(TOOLSTORE_ICON_PATH);
		if (imageURL != null) {
			ImageDescriptor descriptor = ImageDescriptor
					.createFromURL(imageURL);
			reg.put(TOOLSTORE_ICON_ID_IMAGE_REGISTRY, descriptor);
		} else {
			;
		}*/
	}
    
    /**
     * Returns the DriverModel
     * @return
     */
    public DriverManager getDriverModel() {
        return driverManager;
    }

    /**
     * @return The list of configured Aliases
     */
	public AliasManager getAliasManager() {
		return aliasManager;
	}

    public static String getStringPref(String pKey)
    {
    	return Platform.getPreferencesService().getString(PLUGIN_ID, pKey, null, null);
    }
    public static boolean getBooleanPref(String pKey)
    {
    	return Platform.getPreferencesService().getBoolean(PLUGIN_ID, pKey, false, null);
    }
    public static int getIntPref(String pKey)
    {
    	return Platform.getPreferencesService().getInt(PLUGIN_ID, pKey, 0, null);
    }
    public static void setPref(String pKey, boolean pValue)
    {
    	new InstanceScope().getNode(PLUGIN_ID).putBoolean(pKey, pValue);
    }
    
	public IWorkbenchSite getSite() {
		if (getDatabaseStructureView() == null)
			return null;
		return getDatabaseStructureView().getSite();
	}

	public BasicConfigurationManager getConfigurationManager() {
		return configurationManager;
	}
}
