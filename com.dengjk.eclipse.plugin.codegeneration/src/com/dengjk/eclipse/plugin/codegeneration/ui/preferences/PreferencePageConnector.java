package com.dengjk.eclipse.plugin.codegeneration.ui.preferences;

import static com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants.EMPTY_STRING;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.CodeGenerationUIUtils;

/**
 * 类说明: 数据库连接设置 
 * 
 */
public class PreferencePageConnector extends PreferencePage implements IWorkbenchPreferencePage {
    
    public static final String P_DRIVER_TEMPLATE = "P_DRIVER_TEMPLATE"; //$NON-NLS-1$

    public static final String P_DRIVER_NAME = "P_DRIVER_NAME"; //$NON-NLS-1$

    public static final String P_CONNECT_URL = "P_CONNECT_URL"; //$NON-NLS-1$

    public static final String P_USERNAME = "P_USERNAME"; //$NON-NLS-1$
    
    public static final String P_PASSWORD = "P_PASSWORD"; //$NON-NLS-1$
    
    public static final String P_DRIVER_JARS = "P_DRIVER_JARS"; //$NON-NLS-1$
    
    public static final String P_DRIVER_CLASSNAME = "P_DRIVER_CLASSNAME"; //$NON-NLS-1$
    
    private IPreferenceStore preferenceStore;
    
    private Composite parent;
    
    private Text txtDriverName;
    
    /**
     * Create the preference page.
     */
    public PreferencePageConnector() {
        setPreferenceStore(CodeGenerationUIPlugin.getDefault().getPreferenceStore());
    }

    /**
     * Create contents of the preference page.
     * @param parent
     */
    public Control createContents(Composite parent) {
        
        this.parent = parent;
        
        preferenceStore = CodeGenerationUIPlugin.getDefault().getPreferenceStore();
        
        Composite composite = new Composite(parent, SWT.NULL);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        composite.setLayout(layout);
        GridData gd = new GridData();
        gd.horizontalSpan = 2;
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        Group group = new Group(composite, SWT.NONE);
        layout = new GridLayout();
        layout.numColumns = 2;
        group.setLayout(layout);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        group.setLayoutData(gd);
        group.setText( CodeGenerationSubclipseMessages.getString("PreferenceConnector.Title")  );

        Label lblDriverName = new Label(group, SWT.NONE);
        lblDriverName.setText( CodeGenerationSubclipseMessages.getString("PreferenceConnector.label.DriverName") );
        
        txtDriverName = new Text(group, SWT.BORDER);
        txtDriverName.setLayoutData(CodeGenerationUIUtils.createHFillGridData());
        
        initialize();
        
        return composite;
    }
    
    
    /**
     * 参数初始化
     */
    private void initialize() {
    	txtDriverName.setText(preferenceStore.getString(P_DRIVER_NAME));
    }
    
    /**
     * Initialize the preference page.
     */
    public void init(IWorkbench workbench) {
        setPreferenceStore(CodeGenerationUIPlugin.getDefault().getPreferenceStore());
    }

    @Override
    protected void performDefaults(){
        preferenceStore = CodeGenerationUIPlugin.getDefault().getPreferenceStore();
        txtDriverName.setText(EMPTY_STRING);
    }
    
    @Override
    protected void performApply(){
        performOk();
    }
    

    @Override
    public boolean performOk(){
        
        preferenceStore = CodeGenerationUIPlugin.getDefault().getPreferenceStore();
        preferenceStore.setValue(P_DRIVER_NAME, txtDriverName.getText().trim());
        
        return true;
        
    }

}
