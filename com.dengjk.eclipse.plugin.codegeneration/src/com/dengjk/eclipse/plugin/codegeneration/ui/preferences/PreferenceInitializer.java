package com.dengjk.eclipse.plugin.codegeneration.ui.preferences;

import static com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants.EMPTY_STRING;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;

/**
 * 插件配置初始化
 * 
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

    @Override
    public void initializeDefaultPreferences() {
        
        IPreferenceStore pStore = CodeGenerationUIPlugin.getDefault().getPreferenceStore();
        pStore.setDefault(PreferencePageConnector.P_DRIVER_TEMPLATE, EMPTY_STRING ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageConnector.P_DRIVER_NAME, EMPTY_STRING ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageConnector.P_CONNECT_URL, EMPTY_STRING ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageConnector.P_USERNAME, EMPTY_STRING ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageConnector.P_PASSWORD, EMPTY_STRING ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageConnector.P_DRIVER_JARS, EMPTY_STRING ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageConnector.P_DRIVER_CLASSNAME, EMPTY_STRING ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageGeneral.P_NEED_OPEN_FILE, Boolean.toString(false) ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageGeneral.P_NEED_BACKUP_FILE, Boolean.toString(false) ); //$NON-NLS-1$
        pStore.setDefault(PreferencePageGeneral.P_TEMPLATE_PATH, EMPTY_STRING ); //$NON-NLS-1$
        
    }
    
}
