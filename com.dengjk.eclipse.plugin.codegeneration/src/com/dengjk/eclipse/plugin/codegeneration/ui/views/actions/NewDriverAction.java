package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes.ManageDriverNode;
import com.dengjk.eclipse.plugin.codegeneration.ui.wizards.WizardDialogCodeGenerationLocation;
import com.dengjk.eclipse.plugin.codegeneration.ui.wizards.driver.NewDriverWizard;

/**
 * 类说明:DB Browse 视图 右键后创建一个新的driver菜单
 * 
 */
public class NewDriverAction extends AbstractDBTreeContextAction {
	
    private static final ImageDescriptor _image = CodeGenerationUIPlugin.getImageDescriptor(CodeGenerationSubclipseMessages.getString("Images.NewAliasIcon"));
    
    private Shell shell;
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
        return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.NewDriver");
    }

    public String getToolTipText() {
        return CodeGenerationSubclipseMessages.getString("ACTION_NEWDRIVER_TOOLTIP");
    }
    
    public NewDriverAction(Shell shell){
    	this.shell = shell;
    }
	
	public void run(){
		NewDriverWizard wizard = new NewDriverWizard();
		wizard.setNeedsProgressMonitor(true);
		WizardDialog wizardDialog = new WizardDialogCodeGenerationLocation(shell, wizard, 600, 650); //$NON-NLS-1$
		wizardDialog.setMinimumPageSize(500, 550);
		wizardDialog.open();
		
	}
	
    public boolean isAvailable() {

        if ( null == _selectedNodes || _selectedNodes.length == 0) {
            return true;
        }
        if (_selectedNodes[0] instanceof ManageDriverNode) {
            return true;
        }
        return false;
    }

}
