package com.dengjk.eclipse.plugin.codegeneration.ui.util;

import static com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants.EMPTY_STRING;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaConventions;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.internal.ui.dialogs.FilteredTypesSelectionDialog;
import org.eclipse.jdt.internal.ui.wizards.TypedElementSelectionValidator;
import org.eclipse.jdt.internal.ui.wizards.TypedViewerFilter;
import org.eclipse.jdt.ui.JavaElementComparator;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jdt.ui.StandardJavaElementContentProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ResourceSelectionDialog;
import org.eclipse.ui.ide.IDE;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.dialog.PackageLabelProvider;
import com.dengjk.eclipse.plugin.codegeneration.ui.model.RecentConfigurationBean;
import com.dengjk.eclipse.plugin.codegeneration.ui.model.RecentConfigurationBeanManager;
import com.dengjk.eclipse.plugin.codegeneration.utils.IOUtils;

/**
 * 类说明:界面相关、公用计算之类的Util
 * 
 */
public final class CodeGenerationUIUtils {
	public static final int  dialogTypeSelectProject = 1;
	public static final int  dialogTypeSelectPackage = 2;
	public static final int  dialogTypeSelectFile = 3;
	public static final int  dialogTypeSelectResource = 4;
	public static final int  dialogTypeSelectPath = 5;
	
	private static final String ELEMENT_SEPARATION_TAG = ",";
	
    private CodeGenerationUIUtils() {
        super();
    }

    public static GridData createHFillGridData() {
        return createHFillGridData(1);
    }

    public static GridData createHFillGridData(int span ) {
        final GridData gd= createGridData(0, SWT.DEFAULT, SWT.FILL, SWT.CENTER, true, false);
        gd.horizontalSpan= span;
        return gd;
    }
    
    public static GridData createHFillGridData(int span, int height ) {
        final GridData gd= createGridData(0, height, SWT.FILL, SWT.CENTER, true, false);
        gd.horizontalSpan= span;
        return gd;
    }
    
    public static GridData createGridData(int width, int height, int hAlign, int vAlign, boolean hGrab, boolean vGrab) {
        final GridData gd= new GridData(hAlign, vAlign, hGrab, vGrab);
        gd.widthHint= width;
        gd.heightHint= height;
        return gd;
    }
    
	/**
	 * Returns the system {@link Color} matching the specific ID.
	 * 
	 * @param systemColorID
	 *            the ID value for the color
	 * @return the system {@link Color} matching the specific ID
	 */
	public static Color getColor(int systemColorID) {
		Display display = Display.getCurrent();
		return display.getSystemColor(systemColorID);
	}
	
    public static Button createRadioButton(Composite parent, String text) {
        Button radioButton = new Button(parent, SWT.RADIO);
        radioButton.setText(text);
        return radioButton;
    }
    
    /**
     * 用外部浏览器打开目录或网址
     * @param url
     * @throws PartInitException
     */
    public static void openExternalUrl(URL url) throws PartInitException {
        try {
            IWorkbenchBrowserSupport support = PlatformUI.getWorkbench().getBrowserSupport();
            support.getExternalBrowser().openURL(url);
        } catch (Exception e) {
        }
    }

    public static void selectDefaultComboItem(ComboViewer comboViewer) {
        if (comboViewer.getCombo().getItemCount() > 0 && comboViewer.getSelection().isEmpty()) {
            comboViewer.getCombo().select(0);
        }
    }
    
    /**
     * 截取一段文本的前maxSize个字符
     * @param value
     * @param maxSize
     * @return
     */
    public static String getShortString(String value, int maxSize) {    
        if( maxSize <= 0 || value.length() <= maxSize ){
            return value;
        }
        return value.substring(0, maxSize) +"...";
    }
    
	/**
	 * 弹出一个选择项目的"搜索选择"窗口,用户选择之后将返回一个IJavaProject对象. 
	 * @param shell
	 * @param projectNameInit 已选定的Project（默认打开弹窗时将选中该Project）
	 * @return
	 */
	public static IJavaProject chooseJavaProject( Shell shell, String projectNameInit ) {

		// chooseJavaProject()方法会返回用户选择的项目,如果是null,说明是选择了取消按钮,那么直接返回.
		
		ILabelProvider labelProvider = new JavaElementLabelProvider(JavaElementLabelProvider.SHOW_DEFAULT);
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(shell, labelProvider);

		dialog.setTitle(CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_PROJECT")); // 项目选择容器的Title

		dialog.setMessage(CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_PROJECT_2")); // 提示语

		try {
			// 将当前工作空间中所有项目加入到ElementList中去, 供选择.
			IJavaProject[] projects = JavaCore.create(ResourcesPlugin.getWorkspace().getRoot()).getJavaProjects();
			dialog.setElements(projects);
		} catch (JavaModelException jme) {
		}

		// 根据文本框中已经存在的项目名称来为用户确定一个初始的选择
		IJavaProject javaProject = null;
		if ( null != projectNameInit && projectNameInit.trim().length() > 0) {
			javaProject = JavaCore.create(ResourcesPlugin.getWorkspace().getRoot()).getJavaProject(projectNameInit.trim());
		}
		if (javaProject != null) {
			dialog.setInitialSelections(new Object[] { javaProject });
		}
		if (dialog.open() == Window.OK) {
			IJavaProject iJavaProject = (IJavaProject) dialog.getFirstResult();
			return iJavaProject;
		}
		return null;
	}
	
	/**
	 * 选取一个package
	 * 
	 * @param shell
	 * @param javaProject
	 * @return
	 */
	public static IJavaElement choosePackage( Shell shell, IJavaProject javaProject ) {
		IPackageFragmentRoot[] packageFragmentRoots = null;
		try {
			packageFragmentRoots = javaProject.getPackageFragmentRoots();
		} catch (JavaModelException e) {
		}
		List<IJavaElement> javaElements = new ArrayList<IJavaElement>();
		if( null != packageFragmentRoots && packageFragmentRoots.length > 0 ){
			for( IPackageFragmentRoot packageFragmentRoot : packageFragmentRoots ){
				IResource resource = packageFragmentRoot.getResource();
				if( null != resource && resource instanceof IFolder ){
					try {
						IJavaElement[] javaElementsTmp = packageFragmentRoot.getChildren();
						if( null != javaElementsTmp && javaElementsTmp.length >0 ){
							for( IJavaElement javaElementTmp : javaElementsTmp ){
								if( null != javaElementTmp && !javaElementTmp.getElementName().trim().equals( EMPTY_STRING )){
									javaElements.add(javaElementTmp);
								}
							}
						}
					} catch (JavaModelException e) {
						e.printStackTrace();
					}
				}
			}
		}
		ElementListSelectionDialog elementListSelectionDialog = new ElementListSelectionDialog(null, new PackageLabelProvider());
		elementListSelectionDialog.setTitle("choose package");
		elementListSelectionDialog.setMessage("&choose package");
		elementListSelectionDialog.setElements(javaElements.toArray( new IJavaElement[javaElements.size()]));
		if (elementListSelectionDialog.open() == Window.OK) {
        	Object[] results = elementListSelectionDialog.getResult();
        	if( null != results && results.length >0 ){
        		IJavaElement javaElement = (IJavaElement)results[0];
        		return javaElement;
        	}
        }
		return null;
	}
	
	/**
	 * @param shell
	 * @param project
	 * @param isSelectSingle true则仅返回单一文件；否则多个文件以  "File.pathSeparator" 隔开 返回
	 * @return
	 */
	public static String handlePropFileSelect( Shell shell, IProject project, boolean isSelectSingle ) {
	      // 指定当前的项目
	      // 弹出一个资源选择框,让用户选择他自己的配置文件.
		  if(project==null){
			  MessageDialog.openInformation(null, "notice", CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_PROJECT_2"));
			  return null;
		  }
	      ResourceSelectionDialog dialog = new ResourceSelectionDialog(shell, project, CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_FILE"));

	      dialog.setTitle(CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_FILE"));
	      dialog.open();

	      // 用户作出选择之后, 会将这个文件以","隔开写入文本框中去.
	      Object[] results = dialog.getResult();
	      if ((results != null) && (results.length > 0)) {
	         StringBuilder sb = new StringBuilder();
	         for (Object obj : results) {
	            if (obj instanceof IFile) {
	               IFile file = (IFile) obj;
	               sb.append(file.getProjectRelativePath().toString() + ELEMENT_SEPARATION_TAG);
	            }
	         }
	         if (sb.length() > 0) {
	        	 return sb.toString().substring(0, sb.length() - 1);
	            //fPropText.setText(sb.toString().substring(0, sb.length() - 1));
	         }
	      }
	      return null;

	   }

	/**
	 * 选取一个Source目录。Opens a selection dialog that allows to select a source container.
	 *
	 * @return returns the selected package fragment root  or <code>null</code> if the dialog has been canceled.
	 * The caller typically sets the result to the container input field.
	 * <p>
	 * Clients can override this method if they want to offer a different dialog.
	 * </p>
	 *
	 * @since 3.2
	 */
	public static IPackageFragmentRoot chooseContainer( Shell shell ) {
		//IJavaElement initElement= getPackageFragmentRoot();
		Class[] acceptedClasses= new Class[] { IPackageFragmentRoot.class, IJavaProject.class };
		TypedElementSelectionValidator validator= new TypedElementSelectionValidator(acceptedClasses, false) {
			public boolean isSelectedValid(Object element) {
				try {
					if (element instanceof IJavaProject) {
						IJavaProject jproject= (IJavaProject)element;
						IPath path= jproject.getProject().getFullPath();
						return (jproject.findPackageFragmentRoot(path) != null);
					} else if (element instanceof IPackageFragmentRoot) {
						return (((IPackageFragmentRoot)element).getKind() == IPackageFragmentRoot.K_SOURCE);
					}
					return true;
				} catch (JavaModelException e) {
					JavaPlugin.log(e.getStatus()); // just log, no UI in validation
				}
				return false;
			}
		};

		acceptedClasses= new Class[] { IJavaModel.class, IPackageFragmentRoot.class, IJavaProject.class };
		ViewerFilter filter= new TypedViewerFilter(acceptedClasses) {
			public boolean select(Viewer viewer, Object parent, Object element) {
				if (element instanceof IPackageFragmentRoot) {
					try {
						return (((IPackageFragmentRoot)element).getKind() == IPackageFragmentRoot.K_SOURCE);
					} catch (JavaModelException e) {
						JavaPlugin.log(e.getStatus()); // just log, no UI in validation
						return false;
					}
				}
				return super.select(viewer, parent, element);
			}
		};

		StandardJavaElementContentProvider provider= new StandardJavaElementContentProvider();
		ILabelProvider labelProvider= new JavaElementLabelProvider(JavaElementLabelProvider.SHOW_DEFAULT);
		ElementTreeSelectionDialog dialog= new ElementTreeSelectionDialog(shell, labelProvider, provider);
		dialog.setValidator(validator);
		dialog.setComparator(new JavaElementComparator());
		dialog.setTitle("Source Folder Selection");
		dialog.setMessage("&Choose a source folder:");
		dialog.addFilter(filter);
		dialog.setInput(JavaCore.create(ResourcesPlugin.getWorkspace().getRoot()));
		//dialog.setInitialSelection(initElement);
		dialog.setHelpAvailable(false);

		if (dialog.open() == Window.OK) {
			Object element= dialog.getFirstResult();
			if (element instanceof IJavaProject) {
				IJavaProject jproject= (IJavaProject)element;
				return jproject.getPackageFragmentRoot(jproject.getProject());
			} else if (element instanceof IPackageFragmentRoot) {
				return (IPackageFragmentRoot)element;
			}
			return null;
		}
		return null;
	}


	/**
	 * 选取工程中的一个目录
	 * @param shell
	 * @param project 选定的工程
	 * @return
	 */
	public static IPath chooseIPathInProject ( Shell shell, IProject project) {
		IPath path = null;
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(shell, project, false,
				CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_PATHSOURCE"));
		dialog.setTitle(CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_PATHSOURCE_TITLE"));
		if (project != null) {
			dialog.setInitialSelections(new Object[] { project.getFullPath() });
		}
		dialog.showClosedProjects(false);
		if (dialog.open() == Window.OK) {
			Object[] results = dialog.getResult();
			if ((results != null) && (results.length > 0) && (results[0] instanceof IPath)) {
				path = (IPath) results[0];
				path = path.removeFirstSegments(1);
				//String containerName = path.makeRelative().toString();
				//txtSrcFolder.setText(containerName);
			}
		}
		return path;
	}
	
	/**
	 * 指定Class
	 * @param shell
	 * @param context
	 * @param project
	 * @return
	 */
	public static IType chooseClass( Shell shell, IRunnableContext context, IProject project ) {
		IJavaElement[] elements= new IJavaElement[] { JavaCore.create(project)};
		IJavaSearchScope scope= SearchEngine.createJavaSearchScope(elements);
		FilteredTypesSelectionDialog dialog= new FilteredTypesSelectionDialog(shell, false,
				context, scope, IJavaSearchConstants.CLASS);
			dialog.setTitle(CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_CLASS")); 
			dialog.setMessage(CodeGenerationSubclipseMessages.getString("DIALOG_CHOOSE_CLASS_2"));
			if(Window.OK == dialog.open()){
				Object obj = dialog.getFirstResult();
				IType type = (IType)obj;
				return type;
		}
		return null;
	}
	
	/**
	 * Verifies the input for the package field.
	 * 
	 * @param packName
	 * @return 错误信息。若无错误信息，则返回null
	 */
	public static String packageChanged( String packName ) {
		if (packName.length() > 0) {
			IStatus val= null;
			String[] sourceComplianceLevels = new String[] {
					JavaCore.getOption(JavaCore.COMPILER_SOURCE),
					JavaCore.getOption(JavaCore.COMPILER_COMPLIANCE)
			};
			val = JavaConventions.validatePackageName(packName, sourceComplianceLevels[0], sourceComplianceLevels[1]);
			if( null == val ){
				return null;
			}
			if (val.getSeverity() == IStatus.ERROR) {
				return MessageFormat.format("Invalid package name. {0}", new Object[]{ val.getMessage() });
			} else if (val.getSeverity() == IStatus.WARNING) {
				return MessageFormat.format("Discouraged package name. {0}", new Object[]{ val.getMessage() });
			}
		} else {
			return "Enter a package name.";
		}
		return null;
	}
	
	
	/**
	 * @param foldPath
	 * @param project
	 * 
	 * 刷新文件夹
	 */
	public static void refreshFolder( String foldPath, IProject project){
		try{
			project.getFolder(foldPath).refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
		} catch (Exception e) {
		}
	}
	
	/**
	 * @param foldPath
	 * @param project
	 * 
	 * 刷新文件夹
	 */
	public static void refreshFolder( IResource resource, IProject project){
		try{
			resource.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
		} catch (Exception e) {
		}
	}
	
	/**
	 * 在Project中创建一个文件
	 * 
	 * @param runnableContext
	 * @param shell
	 * @param javaProject
	 * @param fileFolder 创建完毕或创建过程中可能需要刷新的文件夹（即：受影响的文件夹）
	 * @param fileOrJavaTypeName 如果出现错误信息，显示的文件夹
	 * @param ifile 待创建的文件
	 * @param compilationUnitContent 待创建的文件内容
	 * @return
	 * 
	 */
	public static boolean createFileInProject(IRunnableContext runnableContext, final Shell shell, 
			final IJavaProject javaProject, String fileFolder, String fileOrJavaTypeName, 
			final IFile ifile, final String compilationUnitContent ,final String charsetName){
		
		if( ifile.exists() ){
			IContainer parentFolder = ifile.getParent();
			if( !MessageDialog.openConfirm(shell, CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"), 
					fileOrJavaTypeName + CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_FILE_EXIST")) ){
				return false;
			}
			File file = ifile.getLocation().toFile();
			file.delete();
			refreshFolder(parentFolder, javaProject.getProject());
		}
		IRunnableWithProgress op = new IRunnableWithProgress() {
			@Override
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				IOUtils.saveFile(ifile.getLocation().toString(), compilationUnitContent, charsetName);
				refreshFolder(ifile.getParent(), javaProject.getProject());
				try {
					ifile.setCharset(charsetName, null);
				} catch (CoreException e) {
					e.printStackTrace();
				}
				
			}
			
		};
		try {
			runnableContext.run( true, true, op );
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		refreshFolder(ifile, javaProject.getProject());
		return true;
	}
	
	/**
	 * 在编辑器中打开文件
	 * @param file
	 * @param editorId 运行传入null
	 */
	public static void openFileWithEditor(final IFile file, final String editorId){
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {
				if(file.exists()){
					if( null != editorId && !editorId.isEmpty() ){
						try {
							IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), file, editorId);
							return ;
						} catch (PartInitException e) {
						}
					}
	                try {
						IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), file);
					} catch (PartInitException e) {
					}
	            }
			}
		});
	}
	
	/**
	 * 保存用户的上一次操作的值
	 */
	public static void intelligentSaveDefaultValueByMemory(String javaProjectName, String srcFolder,String templateSavePaths){
		RecentConfigurationBean recentConfigurationBean = new RecentConfigurationBean(javaProjectName, srcFolder,templateSavePaths);
		RecentConfigurationBeanManager.getSingletonInstance().setRecentConfigurationBean(recentConfigurationBean);
		RecentConfigurationBeanManager.getSingletonInstance().writeCache();
	}
	
}
