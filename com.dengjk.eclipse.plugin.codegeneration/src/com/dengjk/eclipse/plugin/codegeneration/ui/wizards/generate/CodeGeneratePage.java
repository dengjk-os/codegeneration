
package com.dengjk.eclipse.plugin.codegeneration.ui.wizards.generate;

import static com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants.EMPTY_STRING;
import static com.dengjk.eclipse.plugin.codegeneration.ui.util.IntelligentSetValueUtil.RECOMMENDVALUE_SRC_FOLDER1;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.model.RecentConfigurationBean;
import com.dengjk.eclipse.plugin.codegeneration.ui.model.RecentConfigurationBeanManager;
import com.dengjk.eclipse.plugin.codegeneration.ui.preferences.PreferenceInfoReader;
import com.dengjk.eclipse.plugin.codegeneration.ui.preferences.PropBean;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.CodeGenerationUIUtils;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.IntelligentSetValueUtil;
import com.dengjk.eclipse.plugin.codegeneration.utils.FilePathUtils;

/**
 * 类说明:Gennerate Code 页面
 * 
 */
public class CodeGeneratePage extends WizardPage {
	
	private Composite parent;
	
	private Color nominateColor = CodeGenerationUIUtils.getColor(SWT.COLOR_RED);

	private Text txtProject = null;
	
	private Text txtJavaSrcFolder = null;
	
	/*模板相关*/
	private ArrayList<TemplateItemWizard> items = new ArrayList<TemplateItemWizard>();
	
	/**
	 * 选定的src Folder
	 */
	private IPackageFragmentRoot packageFragmentRoot;
	
	private String nameHandlerJava;
	
	
	protected CodeGeneratePage(String pageName){
		super(pageName);
		setTitle(CodeGenerationSubclipseMessages.getString("PAGE_SIMPLE_GENNERATE_TITLE"));
		setMessage(CodeGenerationSubclipseMessages.getString("PAGE_SIMPLE_GENNERATE_MESSAGE"));
	}
	
	@Override
	public void createControl(Composite parent) {
		Composite compositeSetingInf = commLayout(parent);

		// 项目
		commonSettingInf(compositeSetingInf);
		showHorizontalLine(compositeSetingInf);
		
		// 插件总配置
		PropBean propBean = PreferenceInfoReader.getPropBeanFromPreference();
		String templatePath = propBean.getTemplatePath();
		List<File> templates = getTemplates(templatePath);
		if(templates == null || templates.size() == 0){
			doWarningInf(compositeSetingInf);
			return;
		}
		
		// 模板下拉
		itemSettingInf(compositeSetingInf,templates);
        
		// 初始化值
        initValue();
	}
	
	private List<File> getTemplates(String templatePath){
		if(templatePath == null || templatePath.trim() == ""){
			return null;
		}
		File[] files = new File(templatePath).listFiles();
		if(files == null || files.length == 0){
			return null;
		}
		List<File> templates = new ArrayList<File>();
		for(File file : files){
			String fileName = file.getName();
			if(fileName.endsWith(".vm")){
				templates.add(file);
			}else if(fileName.equalsIgnoreCase("NameHandler.java")){
				nameHandlerJava = file.getPath();
			}
		}
		
		return templates;
	}
	
	private void initValue(){
		RecentConfigurationBean recentConfigurationBean = RecentConfigurationBeanManager.getSingletonInstance().getRecentConfigurationBean();
		if( null != recentConfigurationBean ){
			this.setJavaProjectName(recentConfigurationBean.getJavaProjectName());
			this.setJavaSrcFolder(recentConfigurationBean.getSrcFolder());
			
			HashMap<String,String> templateSavePathMap = recentConfigurationBean.getSavePathMap();
			if(templateSavePathMap != null){
				for(TemplateItemWizard item : items){
					String templatePath = item.getTemplatePath();
					String savePath = templateSavePathMap.get(templatePath);
					if(savePath != null){
						item.getTxtPackage().setText(savePath);
					}
				}
			}
		}
	}

	private void intelligentSetValueByJavaProjectNameForWizardPage(){
		IJavaProject javaProject = getSelectedJavaProject();
		
		setCommpositeValue(javaProject);
		
	}
	
	/**
	 * 设置组件的值
	 * @param javaProject
	 */
	private void setCommpositeValue(IJavaProject javaProject) {
		if( null == javaProject || !javaProject.exists() ){
			return ;
		}

		String formerValueSrcFolder = getSelectedPackageFragmentRoot();
		
		String intelligentValueOfSrcFolder = IntelligentSetValueUtil.getSrcFolderValue(javaProject);
		
		boolean isformerValueSrcFolderExist = false;
		if( null != formerValueSrcFolder && !formerValueSrcFolder.isEmpty() ){
			isformerValueSrcFolderExist = IntelligentSetValueUtil.isValidPackageRoot(javaProject, formerValueSrcFolder);
		}
		if(!isformerValueSrcFolderExist ){
			if( null != intelligentValueOfSrcFolder ){
				txtJavaSrcFolder.setText(intelligentValueOfSrcFolder);
			}else{
				if( null != formerValueSrcFolder && !formerValueSrcFolder.isEmpty() ){
					txtJavaSrcFolder.setText(RECOMMENDVALUE_SRC_FOLDER1);
				}
			}
		}
	}
	
	/**
	 * 设置告警信息
	 * @param compositeSetingInf
	 */
	private void doWarningInf(Composite compositeSetingInf) {
		Label label = new Label(compositeSetingInf, SWT.NONE);
        label.setText(CodeGenerationSubclipseMessages.getString("WARNING_INFO"));
        label.setForeground(nominateColor);
	}


	/**
	 * 布局
	 * @param parent
	 * @return
	 */
	private Composite commLayout(Composite parent) {
		this.parent = parent;
		
		GridData gd = null;
        GridLayout layout = null;
        
		Composite composite = new Composite(this.parent, SWT.NULL);
        layout= new GridLayout();
        layout.numColumns = 1;
        composite.setLayout(layout);
        gd = new GridData(GridData.FILL_BOTH);
        composite.setLayoutData(gd);
        setControl(composite);
        initializeDialogUnits(composite);
        
        Composite compositeSetingInf = new Composite(composite, SWT.NONE);
        layout = new GridLayout();
        layout.numColumns = 4;
        compositeSetingInf.setLayout(layout);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 4;
        compositeSetingInf.setLayoutData(gd);
        
		return compositeSetingInf;
	}
	
	/**
	 * 模板的设置信息
	 * @param compositeSetingInf
	 */
	private void itemSettingInf(Composite compositeSetingInf,List<File> templates) {
		for(File template : templates){
			final TemplateItemWizard itemWizard = new TemplateItemWizard();
			String templatePath = template.getPath();
			itemWizard.setTemplatePath(templatePath);
			
			String templateName = FilePathUtils.getTemplateSimpleName(template.getName());
			final boolean javaFileFlag = templateName.endsWith(".java.vm");
			Button btnNeedGenerate = new Button(compositeSetingInf, SWT.CHECK);
			btnNeedGenerate.setToolTipText("Generate "+ templateName+" > "+templateName.replace(".vm",""));
			btnNeedGenerate.setText("Generate "+ templateName+" > "+templateName.replace(".vm",""));
			btnNeedGenerate.setLayoutData(span4());
			btnNeedGenerate.setSelection(true);
			btnNeedGenerate.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					resetControlStatus(itemWizard);
				}
			});
			itemWizard.setBtnNeedGenerate(btnNeedGenerate);
			// 文件夹选项
			Label txtLabel = new Label(compositeSetingInf, SWT.NONE);
			txtLabel.setText("*");
			txtLabel.setForeground(nominateColor);
			txtLabel = new Label(compositeSetingInf, SWT.NONE);
			txtLabel.setText(CodeGenerationSubclipseMessages.getString("SAVE_FOLDER"));
			itemWizard.setTxtLabel(txtLabel);
			
			final Text txtPackage = new Text(compositeSetingInf, SWT.BORDER);
			txtPackage.setLayoutData(CodeGenerationUIUtils.createHFillGridData());
			txtPackage.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					if(javaFileFlag){
						String errorMessage = CodeGenerationUIUtils.packageChanged(txtPackage.getText());
						setErrorMessage(errorMessage);
					}
				}
			});
			itemWizard.setTxtPackage(txtPackage);
			
			Button btnBrowsePackage = new Button(compositeSetingInf, SWT.NONE);
			btnBrowsePackage.setText(CodeGenerationSubclipseMessages.getString("PAGE_BTN_BROWSE"));
			btnBrowsePackage.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(javaFileFlag){
						IJavaElement javaElement = choosePackage();
						if( null != javaElement ){
							txtPackage.setText(javaElement.getElementName());
						}
					}else{
						IPath path = CodeGenerationUIUtils.chooseIPathInProject( getShell(), getSelectedProject() );
		        		if( null != path ){
		        			txtPackage.setText(path.toString());
		        		}
					}
				}
			});
			itemWizard.setBtnBrowsePackage(btnBrowsePackage);
			
			items.add(itemWizard);
		}
	}
	
	/**/
	protected void resetControlStatus(TemplateItemWizard itemWizard){
		Button btnNeedGenerate = itemWizard.getBtnNeedGenerate();
		if(btnNeedGenerate.getSelection()){
			itemWizard.getBtnBrowsePackage() .setEnabled(true);
		}else{
			itemWizard.getBtnBrowsePackage() .setEnabled(false);
		}
	}

	private GridData span4() {
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 4;
		return gd;
	}
	
	public String getProjectName() {
		String projectName = txtProject.getText();
		return projectName;
	}
	
	/**
	 * 通用设置信息
	 * @param compositeSetingInf
	 */
	private void commonSettingInf(Composite compositeSetingInf) {
		tagNeed(compositeSetingInf,"Java Project:");
       
        txtProject = new Text(compositeSetingInf, SWT.BORDER);
        txtProject.setLayoutData(CodeGenerationUIUtils.createHFillGridData());
        txtProject.setEnabled(false);

        Button btnBrowseProject = new Button(compositeSetingInf, SWT.NONE);
        btnBrowseProject.setText(CodeGenerationSubclipseMessages.getString("PAGE_BTN_BROWSE"));
        btnBrowseProject.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            	IJavaProject javaProject = handleProjectButtonSelected();
        		if( null != javaProject ){
        			String projectName = javaProject.getElementName();
            		txtProject.setText(projectName);
            		intelligentSetValueByJavaProjectNameForWizardPage();
        		}
            }
        });
        tagNeed(compositeSetingInf,"Java src folder:");
        txtJavaSrcFolder = new Text(compositeSetingInf, SWT.BORDER);
        txtJavaSrcFolder.setLayoutData(CodeGenerationUIUtils.createHFillGridData());
        txtJavaSrcFolder.setEnabled(false);

        Button btnBrowseSrcFolder = new Button(compositeSetingInf, SWT.NONE);
        btnBrowseSrcFolder.setText(CodeGenerationSubclipseMessages.getString("PAGE_BTN_BROWSE"));
        btnBrowseSrcFolder.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            	IPackageFragmentRoot packageFragmentRootTmp = CodeGenerationUIUtils.chooseContainer(getShell());
            	if( null != packageFragmentRootTmp ){
            		packageFragmentRoot = packageFragmentRootTmp;
            		String path = packageFragmentRoot.getResource().getProjectRelativePath().toString();
            		txtJavaSrcFolder.setText(path);
            	}
            }
        });
	}

	private void tagNeed(Composite compositeSetingInf,String text) {
		Label label = new Label(compositeSetingInf, SWT.NONE);
        label.setText("*");
        label.setForeground(nominateColor);
        label = new Label(compositeSetingInf, SWT.NONE);
        label.setText(text);
	}
	
	private void showHorizontalLine(Composite compositeSetingInf) {
		GridData gd;
		Label label;
		label = new Label(compositeSetingInf, SWT.SEPARATOR | SWT.HORIZONTAL );
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 4;
        label.setLayoutData(gd);
	}

	
	/**
	 * 获取已选定的Project
	 * @return
	 */
	public IProject getSelectedProject(){
		if( txtProject.getText().trim().equals(EMPTY_STRING) ){
			return null;
		}
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(txtProject.getText().trim());
		return project;
	}
	
	/**
	 * 获取已选定的JavaProject
	 * @return
	 */
	public IJavaProject getSelectedJavaProject(){
		IProject project = getSelectedProject();
		if( null == project ){
			return null;
		}
		IJavaProject javaProject = JavaCore.create(project);
		return javaProject;
	}

	public ArrayList<TemplateItemWizard> getItems() {
		return items;
	}

	/**
	 * 获取Src Folder
	 * @return
	 */
	public String getSelectedPackageFragmentRoot() {
		return txtJavaSrcFolder.getText();
	}
	

	/**
	 * 处理 Project块中的Browser按钮<br>
	 * 弹出一个选择项目的"搜索选择"窗口,用户选择之后将返回一个IJavaProject对象
	 * @return
	 */
	private IJavaProject handleProjectButtonSelected() {
		return CodeGenerationUIUtils.chooseJavaProject(getShell(), txtProject.getText());
	}

	/**
	 * 选取一个package
	 */
	private IJavaElement choosePackage() {
		IJavaProject javaProject = getSelectedJavaProject();
		if( null == javaProject ){
			MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_PROJECT_2")); //$NON-NLS-1$
			return null;
		}
		return CodeGenerationUIUtils.choosePackage(getShell(), javaProject);
	}

	/**
	 * set JavaProjectName
	 * @return
	 */
	public void setJavaProjectName(String javaProjectName){
		if( null != javaProjectName && !javaProjectName.trim().isEmpty() ){
			txtProject.setText(javaProjectName.trim());
		}
	}
	
	/**
	 * set JavaSrcFolder
	 * @return
	 */
	public void setJavaSrcFolder(String srcFolder){
		if( null != srcFolder && !srcFolder.trim().isEmpty() ){
			this.txtJavaSrcFolder.setText(srcFolder.trim());
		}
	}

	public String getNameHandlerJava() {
		return nameHandlerJava;
	}

	
}
