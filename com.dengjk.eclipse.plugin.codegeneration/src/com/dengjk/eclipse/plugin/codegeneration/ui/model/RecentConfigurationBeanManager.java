package com.dengjk.eclipse.plugin.codegeneration.ui.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;

/**
 * 类说明:最近的设值
 * 
 */
public class RecentConfigurationBeanManager{
    
    private static Object objectSyn = new Object();
   
    private File cacheFile;
    
    public static final String CACHE_FILE_RBL = "codeGenerationRecentConfigV20190929.txt";
    
    private RecentConfigurationBean recentConfigurationBean = null;

    private static RecentConfigurationBeanManager recentConfigurationBeanManagerSinglton = null;
    
    /**
     * private。防止在外部被实例化
     */
    private RecentConfigurationBeanManager() {
        try {
        	IPath path = Platform.getStateLocation(CodeGenerationUIPlugin.getDefault().getBundle());
            IPath cacheFile = path.append(CACHE_FILE_RBL);
            this.cacheFile = cacheFile.toFile();
            readCache();
        } catch (Exception e) {
        }
    }

    public static synchronized RecentConfigurationBeanManager getSingletonInstance(){
        //double check
        if( null == recentConfigurationBeanManagerSinglton ){
            synchronized(objectSyn) {  
              if( null == recentConfigurationBeanManagerSinglton ){
            	  recentConfigurationBeanManagerSinglton = new RecentConfigurationBeanManager();
              }
            }
        }
        return recentConfigurationBeanManagerSinglton;
    }
    
    /**
     * 设置RecentConfigurationBean
     * @param recentDescriptionBean
     * @param MAX_SIZE 最多保存多少个。运行传入null，传入null时，使用默认的MAX_SIZE
     */
    public void setRecentConfigurationBean( RecentConfigurationBean recentConfigurationBean ){
    	 this.recentConfigurationBean = recentConfigurationBean;
    }

    public RecentConfigurationBean getRecentConfigurationBean(){
    	 return this.recentConfigurationBean;
    }
    
    public void readCache() {
        if (cacheFile == null || !cacheFile.exists()) {
            return;
        }

        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new FileInputStream(cacheFile));
            recentConfigurationBean = (RecentConfigurationBean)in.readObject();
        } catch (Exception e) {
            // ignore
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e1) {
                    // ignore
                }
            }
        }
    }

    public void writeCache() {
    	if( recentConfigurationBean == null ){
    		return ;
    	}
        if (cacheFile == null) {
            return;
        }
        if( cacheFile.exists() ){
            //避免发布新版本时，由于版本冲突而导致Recent Description永远都无法写入到cache
            try {
                cacheFile.delete();
            } catch (Exception e) {
                // ignore
            }
        }
        try {
            cacheFile.createNewFile();
        } catch (Exception e) {
            // ignore
        }
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(cacheFile));
            out.writeObject(recentConfigurationBean);
            out.flush();
        } catch (Exception e) {
            // ignore
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e1) {
                    // ignore
                }
            }
        }
    } 
}
