package com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.sourceforge.sqlexplorer.dbproduct.ManagedDriver;
import net.sourceforge.sqlexplorer.dbproduct.MetaDataSession;
import net.sourceforge.sqlexplorer.dbstructure.nodes.AbstractNode;

/**
 * 类说明:DB Browse视图中Driver展示节点
 * 
 */
public class ManageDriverNode extends AbstractNode {
	
	private ManagedDriver managerDriver;

    private List<String> _childNames = new ArrayList<String>();

    public ManageDriverNode(String name, MetaDataSession session) throws SQLException {
    	super(name, session);
    	setImageKey("Images.ConnectionInfoNode");
    }

    public String[] getChildNames() {

        if (_childNames.size() == 0) {
            getChildNodes();
        }
        return (String[]) _childNames.toArray(new String[] {});
    }
    
    @Override
    public String getLabelText() {
        return _name;
    }

    @Override
    public String getType() {
        return "ConnectionInfo";
    }


    @Override
    public String getUniqueIdentifier() {
        return getQualifiedName();
    }

    public void loadChildren() {
    	synchronized(this)
    	{
    		syncLoadChildren();
    	}
    }
    
    private void syncLoadChildren() {
    	;
    }

	public ManagedDriver getManagerDriver() {
		return managerDriver;
	}

	public void setManagerDriver(ManagedDriver managerDriver) {
		this.managerDriver = managerDriver;
	}
}
