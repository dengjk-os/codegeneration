package com.dengjk.eclipse.plugin.codegeneration.ui.views;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.ui.IPackagesViewPart;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.ViewPart;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.CodeGenerationUIUtils;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.actions.GenerateeFileToggleLinkingAction;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.GennerateListContentProvider;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.GennerateListLabelProvider;

/**
 * 类说明:最近生成的文件列表
 * 
 */
public class ViewGennerateList extends ViewPart {
	public ViewGennerateList() {
	}

    private Composite _composite;

    private IFile ifileSelected;
    
    private TableViewer tableViewer = null;
    
	@Override
	public void createPartControl(Composite parent) {
		// list = new List( parent, SWT.NONE );
		// fillViewToolBarAction();
		fillViewToolBarContextMenu();
		//fillListContextMenu();

        // initialize default message
		IFile[] ifiles = null;
		
        _composite = new Composite(parent, SWT.NULL);
        _composite.setLayout(new FillLayout());
        final TableViewer tableViewer = new TableViewer( _composite, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
        this.tableViewer = tableViewer;
    	final Table table = tableViewer.getTable();
    	table.setHeaderVisible(true);
    	table.setLinesVisible(true);
    	table.setLayout(new TableLayout());

    	tableViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent e) {
				IStructuredSelection selection=(IStructuredSelection)e.getSelection();
				IFile ifile = (IFile)selection.getFirstElement();
				CodeGenerationUIUtils.openFileWithEditor(ifile, null);
			}
		});
    	
    	TableColumn tableColumn = new TableColumn(table, SWT.NONE);
    	tableColumn.setWidth(200);
    	tableColumn.setText("文件名");
    	
    	tableColumn = new TableColumn(table, SWT.NONE);
    	tableColumn.setWidth(750);
    	tableColumn.setText("文件路径");
    	
        tableViewer.setContentProvider( new GennerateListContentProvider() );
        tableViewer.setLabelProvider( new GennerateListLabelProvider() );
        tableViewer.setInput( ifiles );
		table.redraw();//重画界面
        tableViewer.refresh();
        
        resetGenerateList(ifiles);
        
	}

	@Override
	public void setFocus() {
		
	}
	
	public void resetGenerateList( java.util.Collection<IFile> ifilesList ){
		IFile[] ifiles = null;
		if( null == ifilesList || ifilesList.isEmpty() ){
			resetGenerateList(ifiles);
			return ;
		}
		ifiles = ifilesList.toArray( new IFile[ifilesList.size()]);
		resetGenerateList(ifiles);
	}

    /**
     * @param ifiles IFile[].
     */
    public void resetGenerateList(IFile[] ifiles) {
    	tableViewer.setInput( ifiles );
    	
        tableViewer.refresh();
        
    }


	/**
	 * 添加工具栏按钮
	 */
	private void fillViewToolBarAction(){
		IActionBars bars = getViewSite().getActionBars();
		IToolBarManager toolBar = bars.getToolBarManager();
		IPackagesViewPart fPackageExplorerPart = CodeGenerationUIPlugin.getDefault().getPackagesView();
		if( null != fPackageExplorerPart ){
			toolBar.add( new GenerateeFileToggleLinkingAction(fPackageExplorerPart) );
		}
	}
	
	/**
	 * 添加工具栏下拉菜单
	 */
	private void fillViewToolBarContextMenu(){
		//IActionBars bars = getViewSite().getActionBars();
		//IMenuManager menuManager = bars.getMenuManager();
		//menuManager.add( new TBToggleLinkingAction(fPackageExplorerPart) );
	}

	public IFile getIfileSelected() {
		return ifileSelected;
	}

	public void setIfileSelected(IFile ifileSelected) {
		this.ifileSelected = ifileSelected;
	}

	public TableViewer getTableViewer() {
		return tableViewer;
	}

	public void setTableViewer(TableViewer tableViewer) {
		this.tableViewer = tableViewer;
	}

}
