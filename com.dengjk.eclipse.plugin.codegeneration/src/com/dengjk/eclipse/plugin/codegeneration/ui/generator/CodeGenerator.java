package com.dengjk.eclipse.plugin.codegeneration.ui.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.resource.loader.FileResourceLoader;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IJavaProject;

import com.dengjk.eclipse.plugin.codegeneration.dbinfo.ColumnInfo;
import com.dengjk.eclipse.plugin.codegeneration.dbinfo.TableInfo;
import com.dengjk.eclipse.plugin.codegeneration.ui.preferences.PropBean;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.CodeGenerationUIUtils;
import com.dengjk.eclipse.plugin.codegeneration.ui.wizards.generate.TemplateItemWizard;
import com.dengjk.eclipse.plugin.codegeneration.utils.FilePathUtils;

/**
 * 类说明:生成所有文件的入口
 * 
 */
public class CodeGenerator {
	
	private IJavaProject project;
	
	private ArrayList<TemplateItemWizard> items;
	
	private String srcFolder;
	
	private PropBean propBean;

	private CodeGenerator(PropBean propBean,IJavaProject project, String srcFolder, ArrayList<TemplateItemWizard> items) {
		super();
		this.propBean = propBean;
		this.project = project;
		this.srcFolder = srcFolder;
		this.items = items;
	}
	

	private  VelocityContext initVelocityContext(ArrayList<TemplateItemWizard> items,TableInfo tableInfo,String author){
		// 将类名首字母小写
		String classname = Character.toLowerCase(tableInfo.getClassName().charAt(0)) + tableInfo.getClassName().substring(1);
		// 主键列
		ColumnInfo primaryKey = null;
		for(ColumnInfo columnInfo : tableInfo.getColumns()){
			if(columnInfo.isPrimaryKey()){
				primaryKey = columnInfo;
				break;
			}
		}
		
		// java对象数据传递到模板文件vm
		VelocityContext velocityContext = new VelocityContext();
		
		// 所有文件的保存路径或package
		for(TemplateItemWizard template : items){
			String templatePath = template.getTemplatePath();
			String packName = FilePathUtils.getTemplatePackName(templatePath);
			velocityContext.put(packName, template.getTxtPackage().getText());
        }
		
		velocityContext.put("tableInfo", tableInfo);
		velocityContext.put("tableComment", tableInfo.getTableComment());
		velocityContext.put("tableName", tableInfo.getTableName());
		velocityContext.put("className", tableInfo.getClassName());
		velocityContext.put("classname", classname);
		velocityContext.put("columns", tableInfo.getColumns());
		velocityContext.put("primaryKey", primaryKey);
		velocityContext.put("author", author);
		velocityContext.put("datetime", new Date());
		
		return velocityContext;
	}

	public static CodeGenerator getCodeGenerator(PropBean propBean,IJavaProject project, String srcFolder, ArrayList<TemplateItemWizard> items)throws Exception {
		// 解决打包成插件后提示： The specified class for ResourceManager (org.apache.velocity.runtime.resource.ResourceManagerImpl)
		// does not implement org.apache.velocity.runtime.resource.ResourceManager; 
		// Velocity is not initialized correctly.
		
		final ClassLoader oldContextClassLoader = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(CodeGenerator.class.getClassLoader());
		
		Properties p = new Properties();
		String velocityPath = propBean.getVelocityPath();
		if(velocityPath != null && velocityPath.trim().length() > 0){
			InputStream in = null;
			try{
				in = new FileInputStream(new File(velocityPath));
				// 使用properties对象加载输入流
				p.load(in);
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				if(in != null){
					in.close();
				}
			}
		}
		if(!p.containsKey(Velocity.RUNTIME_LOG)){
			// 日志路径
			p.setProperty(Velocity.RUNTIME_LOG,Platform.getLocation().toString()+"/codegeneration-velocity.log");
		}
		// 资源加载类
		p.setProperty("file.resource.loader.class",FileResourceLoader.class.getName());
		// 加载目录下的vm文件
		String filePath = propBean.getTemplatePath();
		filePath = (filePath == null)? "D:/DeveloverTools/eclipse4/code-template" : filePath;
		p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH,filePath);
		try {
			// 初始化Velocity引擎，指定配置Properties
			Velocity.init(p);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		// set back default class loader
	    Thread.currentThread().setContextClassLoader(oldContextClassLoader);
		
		return new CodeGenerator(propBean,project,srcFolder ,items);
	}
	
	
	
	public ArrayList<IFile> generate(TableInfo tableInfo) throws Exception{
		ArrayList<IFile> result = new ArrayList<IFile>();
		if(items == null){
			return result;
		}
		// 模板上下文
		VelocityContext context = initVelocityContext(items,tableInfo,propBean.getUserName());
		
		for(TemplateItemWizard template : items){
			
			// 是否勾选了模板
			if(!template.getBtnNeedGenerate().getSelection()){
				continue;
			}
			
			String templatePath = template.getTemplatePath();
			String templateName = FilePathUtils.getFileName(templatePath);
			String filePath = buildFilePath( template,context);
			File file = new File(filePath);
			if(file.exists() && propBean.isIsneedBackUpFile()){
				backUpFile(filePath);
			}
			File folder = file.getParentFile();
			if(!folder.exists()){
				folder.mkdirs();
			}
			Writer writer = null;
			try{
				String encoding = (propBean.getFileCharset() == null)? "UTF-8" : propBean.getFileCharset();
				writer = new OutputStreamWriter(new FileOutputStream(file),encoding);
				Template tpl = Velocity.getTemplate(templateName,encoding);
				tpl.merge(context, writer);
				writer.flush();
			}finally {
				if(writer != null){
					writer.close();
				}
			}
			String projectPath = project.getProject().getLocation().toString();
			int start = projectPath.length()+1;
            final IFile ifile = project.getProject().getFile(filePath.substring(start));
            if( propBean.isNeedOpenFile() ){
            	CodeGenerationUIUtils.openFileWithEditor(ifile, null);
    		}
            CodeGenerationUIUtils.refreshFolder(ifile, project.getProject());
            result.add(ifile );
        }
		return result;
		
	}
	
	private void backUpFile(String filePath){
		File file = new File(filePath);
		filePath = filePath + ".bak";
		file.renameTo(new File(filePath));  
	}
	
	private String buildFilePath(TemplateItemWizard template,VelocityContext context){
		String filePath = null;
		String templatePath = template.getTemplatePath();
		String fileName = FilePathUtils.getTemplateSimpleName(templatePath).replaceAll(".vm","");
		// 文件名支持Velocity表达式
		StringWriter writer = new StringWriter();
		Velocity.evaluate(context,writer,"logTag",fileName);
		fileName = writer.toString(); 

		String projectPath = project.getProject().getLocation().toString();
		String txtPackage = template.getTxtPackage().getText();
		// 包路径
		if(txtPackage.indexOf(".") != -1){
			txtPackage = txtPackage.replaceAll("\\.",Matcher.quoteReplacement(File.separator));
			filePath = projectPath + File.separator + srcFolder + File.separator + txtPackage + File.separator + fileName;
		}else{
			filePath = projectPath+ File.separator + txtPackage + File.separator + fileName;
		}
		
		return filePath;
		
	}
	
	
}
