
package com.dengjk.eclipse.plugin.codegeneration.ui.wizards.generate;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;

import com.dengjk.eclipse.plugin.codegeneration.dbinfo.TableInfo;
import com.dengjk.eclipse.plugin.codegeneration.dbinfo.handler.NameHandlerHelper;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.dbadapterinitial.TableInfoInitial;
import com.dengjk.eclipse.plugin.codegeneration.ui.exception.CodeGenerationException;
import com.dengjk.eclipse.plugin.codegeneration.ui.generator.CodeGenerator;
import com.dengjk.eclipse.plugin.codegeneration.ui.preferences.PreferenceInfoReader;
import com.dengjk.eclipse.plugin.codegeneration.ui.preferences.PropBean;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.CodeGenerationUIUtils;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewGennerateList;
import com.dengjk.eclipse.plugin.codegeneration.utils.PluginLogUtils;

import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;
import net.sourceforge.sqlexplorer.dbstructure.nodes.TableNode;

/**
 * 类说明:GennerateCode向导
 * 
 */
public class CodeGenerateWizard extends Wizard {
	
	
	private CodeGeneratePage codeGeneratePage;

	private List<TableInfo> tableInfoList;
	
	public CodeGenerateWizard(INode[] selectedNodes ){
		
		setWindowTitle("Code Generation");
		
		tableInfoList = new ArrayList<TableInfo>();
		
		if( null != selectedNodes && selectedNodes.length > 0 ){
			for(int i=0; i<selectedNodes.length; i++){
				TableInfo tableInfo = TableInfoInitial.buildTableInfo((TableNode) selectedNodes[i]);
				tableInfoList.add(tableInfo);
			}
		}
	}
	
	@Override
	public void addPages() {
		codeGeneratePage = new CodeGeneratePage(CodeGenerationSubclipseMessages.getString("PAGE_SIMPLE_GENNERATE_TITLE"));
		addPage(codeGeneratePage);
    }
	
	@Override
	public boolean performFinish() {
		if(!checkInputData()) return false;

		PropBean propBean = PreferenceInfoReader.getPropBeanFromPreference();
		
		IJavaProject javaProject = codeGeneratePage.getSelectedJavaProject();
		
		if( null == javaProject || !javaProject.exists() ){
			MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_PROJECT_3"));
			return false;
		}
		String srcFolder = codeGeneratePage.getSelectedPackageFragmentRoot();
		
		// 记录设置过的值
		intelligentSaveDefaultValueByMemory();
		
		ArrayList<TemplateItemWizard> items = codeGeneratePage.getItems();
		String handlerJavaFile = codeGeneratePage.getNameHandlerJava();
		java.util.LinkedHashSet<IFile> generateIFileSet = new java.util.LinkedHashSet<IFile>();
		for(TableInfo tableInfo : tableInfoList){
			try {
				// 名称转换
				NameHandlerHelper.handlerName(handlerJavaFile,tableInfo);
				// 文件生成
				ArrayList<IFile> files = CodeGenerator.getCodeGenerator(propBean,javaProject,srcFolder,items).generate(tableInfo);
				//展示最近生成的文件
				generateIFileSet.addAll(files);
			} catch (CodeGenerationException e) {
				PluginLogUtils.logErr(e);
				MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),e.getMessage()); 
				return false;
			}catch (Exception e) {
				PluginLogUtils.logErr(e);
	            MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_CONTACTUS") + e.getMessage()); 
				return false;
			}
		}
		
		
		ViewGennerateList viewGennerateList = CodeGenerationUIPlugin.getDefault().getViewGennerateList();
		viewGennerateList.resetGenerateList(generateIFileSet);
		viewGennerateList.getSite().getPage().bringToTop(viewGennerateList);
		
		return true;
	}
	
	private void intelligentSaveDefaultValueByMemory(){
		String projectName = codeGeneratePage.getProjectName();
		String srcFolder = codeGeneratePage.getSelectedPackageFragmentRoot();
		StringBuilder templateSavePaths = new StringBuilder("");
		
		ArrayList<TemplateItemWizard> items = codeGeneratePage.getItems();
		for(TemplateItemWizard item : items){
			String savePath = item.getTemplatePath()+"&"+item.getTxtPackage().getText();
			templateSavePaths.append(savePath).append(";");		
		}
		// 记录设置过的值
		CodeGenerationUIUtils.intelligentSaveDefaultValueByMemory(projectName,srcFolder,templateSavePaths.toString());
	}

	private boolean checkInputData() {
		IJavaProject javaProject = codeGeneratePage.getSelectedJavaProject();
		String srcFolder = codeGeneratePage.getSelectedPackageFragmentRoot();
		if( null == javaProject ){
			MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),
					 CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_PROJECT_3"));
			return false;
		}
		if( null == srcFolder || srcFolder.isEmpty() ){
			MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_SRC_FOLDER"));
			return false;
		}
		
		ArrayList<TemplateItemWizard> generatorItems = codeGeneratePage.getItems();
		if( null == generatorItems || generatorItems.isEmpty() ){
			MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_TEMPLATE_FOLDER"));
			return false;
		}
		// 校验每个模板
		boolean noItems = true;
		for(TemplateItemWizard itemWizard : generatorItems){
			boolean selected = itemWizard.getBtnNeedGenerate().getSelection();
			String savePath = itemWizard.getTxtPackage().getText();
			if(selected){
				noItems = false;
				if(savePath == null || savePath.trim().equals("")){
					String errorTip = itemWizard.getTxtLabel().getText()+CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_NO_SAVE_FOLDER");
					MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),errorTip);
					return false;
				}
			}
			
		}
		
		if(noItems){
			MessageDialog.openError(getShell(), CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_TEMPLATE_FOLDER"));
			return false;
		}
		
		return true;
	}
	
	
	public boolean canFinish() {
		return super.canFinish();
	}

}
