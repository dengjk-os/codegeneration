package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import net.sourceforge.sqlexplorer.dbproduct.Alias;
import net.sourceforge.sqlexplorer.dbproduct.AliasManager;
import net.sourceforge.sqlexplorer.dbproduct.ManagedDriver;
import net.sourceforge.sqlexplorer.dbproduct.MetaDataSession;
import net.sourceforge.sqlexplorer.dbproduct.User;
import net.sourceforge.sqlexplorer.dbstructure.nodes.DatabaseNode;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes.ManageDriverNode;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.ImageUtil;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewDBBrowse;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewTableInfo;

/**
 * 类说明:DB Browse 视图 右键后打开数据库连接的菜单
 * 
 */
public class OpenConnectionAction extends AbstractDBTreeContextAction {

    private static final ImageDescriptor _image = ImageUtil.getDescriptor("Images.OpenDBIcon");
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
    	if (_selectedNodes.length == 0) {
    		return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.OpenConnection");
        }
        if (! (_selectedNodes[0] instanceof ManageDriverNode) ) {
        	return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.OpenConnection");
        }
        ManageDriverNode connectionInfoNode = (ManageDriverNode)_selectedNodes[0];
        if( null == connectionInfoNode.getChildNodes() || connectionInfoNode.getChildNodes().length == 0 ){
        	return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.OpenConnection");
        }
        return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.OpenAnotherConnection");
    }

    public void run() {
    	
    	PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				BusyIndicator.showWhile(shell.getDisplay(), new Runnable() {
						public void run() {
					        // refresh structure view
					        ViewDBBrowse structureView = CodeGenerationUIPlugin.getDefault().getDatabaseStructureView();    
					    	
					        AliasManager aliasManager = CodeGenerationUIPlugin.getDefault().getAliasManager();
					        // refresh nodes
					        for (int i = 0; i < _selectedNodes.length; i++) {
					        	ManageDriverNode connectionInfoNode = (ManageDriverNode)_selectedNodes[i];
					        	ManagedDriver managedDriver = connectionInfoNode.getManagerDriver();
					
					        	Alias alias = aliasManager.getAlias(managedDriver.getId());
					        	if( null == alias ){
					        		//# FIXME 请在此处添加代码，弹出输入用户名或密码的窗口，如果用户名或密码无误，则保存alias对象
					        		//MessageDialog.openInformation(null, "提示", "请在此处添加代码，弹出输入用户名或密码的窗口，如果用户名或密码无误，则保存alias对象");
					        		MessageDialog.openError(null, "提示", "数据库连接URI，或用户名、密码不正确，请修改数据库连接配置后重试");
					        		return ;
					        	}
					        	
					            User user = alias.getDefaultUser();
					            
					            if (user != null) {
					            	user.setAutoCommit(false);
					            	user.setCommitOnClose(false);
					            }
					            
					            MetaDataSession metaDataSession = user.getMetaDataSession();
					            DatabaseNode databaseNode = metaDataSession.getRoot();
					            if( null == databaseNode ){
					            	//# FIXME 请在此处添加代码，弹出输入用户名或密码的窗口，如果用户名或密码无误，则保存alias对象
					        		//MessageDialog.openInformation(null, "提示", "请在此处添加代码，弹出输入用户名或密码的窗口，如果用户名或密码无误，则保存alias对象");
					            	MessageDialog.openError(null, "提示", "数据库连接URI，或用户名、密码不正确，请修改数据库连接配置后重试");
					            	return ;
					            }
					            databaseNode.setParent(connectionInfoNode);
					            databaseNode.setName(alias.getName());
					            
					            try {
									aliasManager.addAlias(alias);
								} catch (Exception e) {
								}
					            
					            connectionInfoNode.addChildNode(databaseNode);
					        }
					        
					        _treeViewer.refresh();
					
					        // refresh detail view
					        ViewTableInfo detailView = (ViewTableInfo) CodeGenerationUIPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(ViewTableInfo.class.getName());
					        structureView.getTreeViewer().synchronizeDetailView(detailView);
						}
					});
				}
			});
    	
    }

    public boolean isAvailable() {

        if (null == _selectedNodes || _selectedNodes.length == 0) {
            return false;
        }
        if (_selectedNodes[0] instanceof ManageDriverNode) {
            return true;
        }
        return false;
    }
}
