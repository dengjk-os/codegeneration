package com.dengjk.eclipse.plugin.codegeneration.ui.views;

import java.sql.SQLException;
import java.util.List;

import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.TableColumnInfoSorter;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.TableInfoContentProvider;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.TableInfoLabelProvider;

import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;
import net.sourceforge.sqlexplorer.dbstructure.nodes.TableNode;
import net.sourceforge.squirrel_sql.fw.sql.IndexInfo;
import net.sourceforge.squirrel_sql.fw.sql.TableColumnInfo;

/**
 * 类说明:Table Info 视图
 * 
 */
public class ViewTableInfo extends ViewPart {

    private Composite _composite;
    
	@Override
	public void createPartControl(Composite parent) {

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, CodeGenerationUIPlugin.PLUGIN_ID + ".DatabaseDetailView");
        
        // create new composite to display information
        _composite = new Composite(parent, SWT.NULL);
        _composite.setLayout(new FillLayout());

        // initialize default message
        setSelectedNode(null);
        
        // synchronize with structure view that may already exist
        ViewDBBrowse structureView = CodeGenerationUIPlugin.getDefault().getDatabaseStructureView();

        if (structureView != null) {
            structureView.getTreeViewer().synchronizeDetailView(this);
        }

	}

	@Override
	public void setFocus() {

	}
	
    private static final String COLUMN_LABELS[] = CodeGenerationSubclipseMessages.getString("TABLE_COLUMNS").split(",");
    
    /**
     * Show the details for the given node. If node is null, a default message
     * will be displayed.
     * 
     * @param node INode.
     */
    public void setSelectedNode(INode node) {

        // clean first..
        Composite parent = _composite.getParent();
        _composite.dispose();
        _composite = new Composite(parent, SWT.NULL);
        _composite.setLayout(new FillLayout());

        if ( null != node && node instanceof TableNode) {

        	final TableViewer tableViewer = new TableViewer( _composite, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
        	Table table = tableViewer.getTable();
        	table.setHeaderVisible(true);
        	table.setLinesVisible(true);
        	table.setLayout(new TableLayout());
        	
        	int columnIndex = 0;
        	for( String columnLabel : COLUMN_LABELS ){
        		columnIndex ++;
        		TableColumn tableColumn = new TableColumn(table, SWT.NONE);
            	tableColumn.setWidth(100);
            	tableColumn.setText(columnLabel);
            	final int currentColumnIndex = columnIndex;
            	tableColumn.addSelectionListener(new SelectionAdapter() {
        			boolean sortType = true;//记录上一次的排序方式,默认是升序
        			public void widgetSelected(final SelectionEvent e) {
        				sortType = !sortType;
        				tableViewer.setSorter(TableColumnInfoSorter.generateSorter(currentColumnIndex,sortType));
        			}
        		});
        	}
        	
            TableNode tableNode = (TableNode) node;
            
            TableColumnInfo[] cols = null;
            List<IndexInfo> indexList = null;
			try {
				cols = node.getSession().getMetaData().getColumnInfo(tableNode.getTableInfo());
				indexList = tableNode.getSession().getMetaData().getIndexInfo(tableNode.getTableInfo());
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if( null != cols && cols.length > 0 ){
				//最后一位记录该column是否是主键，此段代码待优化
                Comparable<?>[][] dataRows = new Comparable[ cols.length ][];
                int index = 0;
                for (TableColumnInfo col : cols) {
                	Comparable<?>[] row = new Comparable[ 7 + 2 ];
                	dataRows[index++] = row;

                	int i = 0;
                	row[i++] = col.getColumnName();
                	//row[i++] = col.getDataType();
                	row[i++] = col.getTypeName();
                	row[i++] = col.getColumnSize();
                	row[i++] = col.getDecimalDigits();
                	//row[i++] = col.getRadix();
                	//row[i++] = col.isNullAllowed();
                	row[i++] = col.getDefaultValue();
                	//row[i++] = col.getOctetLength();
                	//row[i++] = col.getOrdinalPosition();
                	row[i++] = col.isNullable();
                	row[i++] = col.getRemarks();
                	boolean isPrimaryKey = false;
                	if( null != tableNode.getPrimaryKeyNames() ){
                		for( String pkColumnName : tableNode.getPrimaryKeyNames() ){
                			if( null != col.getColumnName() 
    								&& col.getColumnName().toLowerCase().equals(pkColumnName.toLowerCase())){
                				isPrimaryKey = true;
                				break;
                			}
                		}
                	}
                	row[i++] = isPrimaryKey;
                	boolean isIndexKey = false;
                	if( null != indexList && !indexList.isEmpty() ){
    					for( IndexInfo indexInfo: indexList ){
    						if( null != indexInfo.getColumnName() 
    								&& null != col.getColumnName() 
    								&& indexInfo.getColumnName().equals(col.getColumnName())){
    							isIndexKey = true;
    							break;
    						}
    					}
        			}
                	row[i++] = isIndexKey;
                	if (i != 7 + 2)
                		throw new RuntimeException("Internal error: ColumnInfoTab: wrong number of columns");
                }
                
                tableViewer.setContentProvider( new TableInfoContentProvider() );
                tableViewer.setLabelProvider( new TableInfoLabelProvider() );
                tableViewer.setInput( dataRows );
        		table.redraw();//重画界面
                tableViewer.refresh();
			}
        }else{
            // add default message
            String message = "尚未选定Table";

            Label label = new Label(_composite, SWT.FILL);
            label.setText(message);
            label.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

        }
        	

        _composite.layout();
        _composite.getParent().layout();
        _composite.redraw();
    }

}
