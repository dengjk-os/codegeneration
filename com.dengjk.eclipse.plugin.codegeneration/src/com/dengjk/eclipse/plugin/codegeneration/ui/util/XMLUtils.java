package com.dengjk.eclipse.plugin.codegeneration.ui.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;

/**
 * helper class for reading and writing XML files
 * 
 * @author Heiko
 *
 */
public class XMLUtils {

	public static void save(Element pRoot, File pFile) {
        try {
        	XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(pFile), OutputFormat.createPrettyPrint());
        	xmlWriter.startDocument();
        	xmlWriter.write(pRoot);
        	xmlWriter.endDocument();
        	xmlWriter.flush();
        	xmlWriter.close();
        } catch (Exception e) {
        	CodeGenerationUIPlugin.error("Couldn't save: " + pFile.getAbsolutePath(), e);
        }
		
	}
	
	public static Element readRoot(File pFile)	{
		if(!pFile.exists())
		{
			return null;
		}
    	try {
    		return readRoot(new FileInputStream(pFile));
    	}
    	catch(DocumentException e) {
    		CodeGenerationUIPlugin.error("Cannot load: " + pFile.getAbsolutePath(), e);
    	} catch (FileNotFoundException ignored) {
			// impossible :-)
			
		}
		return null;
	}
	
	public static Element readRoot(InputStream pFile) throws DocumentException	{
        SAXReader reader = new SAXReader();
        return reader.read(pFile).getRootElement();
	}
}
