package com.dengjk.eclipse.plugin.codegeneration.ui.util;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;

public class ImageUtil {

    private static Map<String,Integer> _imageCount = new HashMap<String, Integer>();

    private static Map<String,Image> _images = new HashMap<String, Image>();


    /**
     * Dispose of an image in cache. Once there are no more open handles to the
     * image it will be disposed of.
     * 
     */
    public static void disposeImage(String propertyName) {

        try {
            Image image = (Image) _images.get(propertyName);

            if (image == null) {
                return;
            }

            Integer handleCount = (Integer) _imageCount.get(propertyName);

            if (handleCount == null || handleCount == 0) {
                image.dispose();
                _images.remove(propertyName);
            	_imageCount.remove(propertyName);
            } else {
                handleCount = new Integer(handleCount.intValue() - 1);
            	_imageCount.put(propertyName, handleCount);
            }

        } catch (Throwable e) {
            CodeGenerationUIPlugin.error("Error disposing images", e);
        }
    }


    /**
     * Create an image descriptor for the given image property in the
     * text.properties file.
     * 
     * @param propertyName
     * @return
     */
    public static ImageDescriptor getDescriptor(String propertyName) {

        try {

            if (propertyName == null) {
                return null;
            }
            
            // get image path
            String path = CodeGenerationSubclipseMessages.getString(propertyName);

            if (path == null || path.trim().length() == 0) {
                CodeGenerationUIPlugin.error("Missing image path for " + propertyName, null);
                return null;
            }

            // create image
            URL url = URLUtil.getResourceURL(path);
            return ImageDescriptor.createFromURL(url);

        } catch (Exception e) {
            CodeGenerationUIPlugin.error("Couldn't create image for " + propertyName, e);
            return null;
        }

    }

    /**
     * Create an image descriptor for the given image property in the
     * text.properties file.
     * 
     * @param propertyName
     * @return
     */
    public static ImageDescriptor getDescriptorByKey(String pKey) {

        try {

            // get image path
            String path = pKey;

            if (path == null || path.trim().length() == 0) {
                CodeGenerationUIPlugin.error("Missing image path for " + pKey, null);
                return null;
            }

            // create image
            URL url = URLUtil.getResourceURL(path);
            return ImageDescriptor.createFromURL(url);

        } catch (Exception e) {
            CodeGenerationUIPlugin.error("Couldn't create image for " + pKey, e);
            return null;
        }

    }


    public static ImageDescriptor getFragmentDescriptor(String fragmentId, String path) {
        
        try {

            if (path == null || path.trim().length() == 0) {
                return null;
            }
            
            // create image
            URL url = URLUtil.getFragmentResourceURL(fragmentId, path);
            return ImageDescriptor.createFromURL(url);

        } catch (Exception e) {
            CodeGenerationUIPlugin.error("Couldn't create image for " + fragmentId + ": " + path, e);
            return null;
        }
        
    }

    /**
     * Get an image object from cache or create one if it doesn't exist yet.
     * Everytime an object is retrieved, it should be disposed of using the
     * ImageUtil.disposeImage method.
     * 
     * @param propertyName
     */
    public static Image getImage(String propertyName) {

        Image image = (Image) _images.get(propertyName);

        if (image == null) {
            image = getDescriptor(propertyName).createImage();

            if (image == null) {
                return null;
            }

            _images.put(propertyName, image);
        }

        // increase image handle count by one

        Integer handleCount = (Integer) _imageCount.get(propertyName);

        if (handleCount == null) {
            handleCount = new Integer(1);
        } else {
            handleCount = new Integer(handleCount.intValue() + 1);
        }
        _imageCount.put(propertyName, handleCount);

        return image;
    }
    
    public static Image getFragmentImage(String fragmentId, String path) {
        
        try {

            if (path == null || path.trim().length() == 0) {
                return null;
            }
            
            // create image
            URL url = URLUtil.getFragmentResourceURL(fragmentId, path);
            ImageDescriptor descriptor = ImageDescriptor.createFromURL(url);
            if (descriptor == null) {
                return null;
            }
            return descriptor.createImage();

        } catch (Exception e) {
            CodeGenerationUIPlugin.error("Couldn't create image for " + fragmentId + ": " + path, e);
            return null;
        }
    }
}
