package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.actions.ActionGroup;

/**
 * 类说明:DB Browse 视图 右键 ActionGroup
 * 
 */
public class ViewDBBrowseActionGroup extends ActionGroup {
	
	private TreeViewer treeViewer;
	
	private Shell shell;

	public ViewDBBrowseActionGroup(TreeViewer treeViewer, Shell shell){
		this.treeViewer = treeViewer;
    	this.shell = shell;
	}
	
	@Override
	public void fillContextMenu(IMenuManager menu) {

		INode[] nodes = getSelectedNodes();
		List<AbstractDBTreeContextAction> actionList = new ArrayList<AbstractDBTreeContextAction>();
	
		OpenConnectionAction openConnectionAction = new OpenConnectionAction();
		openConnectionAction.setSelectedNodes(nodes);
		openConnectionAction.setTreeViewer(treeViewer);
		if( openConnectionAction.isAvailable() ){
			actionList.add( openConnectionAction );
		}

		NewDriverAction newConnectionAction = new NewDriverAction(shell);
		newConnectionAction.setSelectedNodes(nodes);
		newConnectionAction.setTreeViewer(treeViewer);
		if( newConnectionAction.isAvailable() ){
			actionList.add( newConnectionAction );
		}
		
		EditDriverAction editDriverAction = new EditDriverAction(shell);
		editDriverAction.setSelectedNodes(nodes);
		editDriverAction.setTreeViewer(treeViewer);
		if( editDriverAction.isAvailable() ){
			actionList.add( editDriverAction );
			if(null == nodes || nodes.length == 0){
				editDriverAction.setEnabled(false);
			}
		}

		CopyDriverAction copyDriverAction = new CopyDriverAction(shell);
		copyDriverAction.setSelectedNodes(nodes);
		copyDriverAction.setTreeViewer(treeViewer);
		if( copyDriverAction.isAvailable() ){
			actionList.add( copyDriverAction );
			if(null == nodes || nodes.length == 0){
				copyDriverAction.setEnabled(false);
			}
		}

		DelectManageDriverAction delectManageDriverAction = new DelectManageDriverAction();
		delectManageDriverAction.setSelectedNodes(nodes);
		delectManageDriverAction.setTreeViewer(treeViewer);
		if( delectManageDriverAction.isAvailable() ){
			actionList.add( delectManageDriverAction );
			if(null == nodes || nodes.length == 0){
				delectManageDriverAction.setEnabled(false);
			}
		}

		RefreshNodeAction refreshNodeAction = new RefreshNodeAction();
		refreshNodeAction.setSelectedNodes(nodes);
		refreshNodeAction.setTreeViewer(treeViewer);
		if( refreshNodeAction.isAvailable() ){
			actionList.add( refreshNodeAction );
		}
		
		
		CodeGenerateAction sampleGenerateXmlAndJavaAction = new CodeGenerateAction(shell);
		sampleGenerateXmlAndJavaAction.setSelectedNodes(nodes);
		sampleGenerateXmlAndJavaAction.setTreeViewer(treeViewer);
		if( sampleGenerateXmlAndJavaAction.isAvailable() ){
			actionList.add( sampleGenerateXmlAndJavaAction );
		}
		
		
		CloseConnectionAction closeConnectionAction = new CloseConnectionAction();
		closeConnectionAction.setSelectedNodes(nodes);
		closeConnectionAction.setTreeViewer(treeViewer);
		if( closeConnectionAction.isAvailable() ){
			actionList.add( closeConnectionAction );
		}
		
		AbstractDBTreeContextAction[] actions = actionList.toArray(new AbstractDBTreeContextAction[actionList.size()]);

		for (AbstractDBTreeContextAction current : actions) {
			menu.add(current);
		}

	}

	private INode[] getSelectedNodes() {
		// find our target node..
		IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();

		// check if we have a valid selection
		if (selection == null) {
			return null;
		}

		List<INode> selectedNodes = new ArrayList<INode>();
		Iterator<?> it = selection.iterator();

		while (it.hasNext()) {
			Object object = it.next();
			if (object instanceof INode) {
				selectedNodes.add((INode) object);
			}
		}

		if (selectedNodes.size() == 0) {
			return null;
		}

		INode[] nodes = selectedNodes.toArray(new INode[selectedNodes.size()]);
		return nodes;
	}

}
