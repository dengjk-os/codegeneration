package com.dengjk.eclipse.plugin.codegeneration.ui.views;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import net.sourceforge.sqlexplorer.dbproduct.DriverManager;
import net.sourceforge.sqlexplorer.dbproduct.ManagedDriver;
import net.sourceforge.sqlexplorer.dbstructure.nodes.AbstractNode;
import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.ViewPart;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes.ManageDriverNode;
import com.dengjk.eclipse.plugin.codegeneration.ui.exception.SQLCannotConnectException;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.actions.NewDriverAction;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.actions.ViewDBBrowseActionGroup;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.DBTreeContentProvider;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.DBTreeLabelProvider;

/**
 * 类说明:DB Browse 视图
 * 
 */
public class ViewDBBrowse extends ViewPart {
	public ViewDBBrowse() {
	}
	
	private RootNode rootNodeViewDBBrowse = null;

    private Composite _parent;
    
	//private org.eclipse.swt.widgets.List list;
    
    private DBTreeView treeViewer;

    private java.util.List<ManagedDriver> managedDrivers = new ArrayList<ManagedDriver>();

	public static class RootNode extends AbstractNode
	{
		private java.util.List<ManageDriverNode> nodes = new java.util.ArrayList<ManageDriverNode>();
		
		public RootNode(ManageDriverNode pNode)
		{
			super(pNode.getName());
			nodes.add(pNode);
		}
		
		
		@Override
		public void loadChildren() {
			if( null == _children ){
				_children = new ArrayList<INode>();
			}
			_children.addAll(nodes);
		}
		
	}

	@Override
	public void createPartControl(Composite parent) {
		
		this._parent = parent;
		this.clearParent();
        // create outline
        //treeViewer = new TreeViewer(_parent, SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI );
        treeViewer = new DBTreeView(_parent, SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI, this);
        getSite().setSelectionProvider(treeViewer);
        
        // use hash lookup to improve performance
        treeViewer.setUseHashlookup(true);

        // add content and label provider
        treeViewer.setContentProvider(new DBTreeContentProvider());
        treeViewer.setLabelProvider(new DBTreeLabelProvider());
        treeViewer.setAutoExpandLevel(2);
        
        // 装载所有的已经保存的managedDrivers
        DriverManager driverManager = CodeGenerationUIPlugin.getDefault().getDriverModel();
        if( null != driverManager ){
        	Collection<ManagedDriver> managedDriverCollection = driverManager.getDrivers();
        	if( null != managedDriverCollection && !managedDriverCollection.isEmpty() ){
            	for (ManagedDriver managedDriver : managedDriverCollection) {
            		try {
						addManagedDriver(managedDriver);
					} catch (Exception e) {
					}
            	}
        	}
        }

        _parent.layout();

        // bring this view to top of the view stack, above detail if needed..
        getSite().getPage().bringToTop(this);

        getSite().setSelectionProvider(treeViewer);

        
        // create action bar
        IToolBarManager toolBarMgr = getViewSite().getActionBars().getToolBarManager();
        toolBarMgr.add( new NewDriverAction(getSite().getShell()) );
        
        // add context menu
        final ViewDBBrowseActionGroup actionGroup = new ViewDBBrowseActionGroup(treeViewer, getSite().getShell());
        MenuManager menuManager = new MenuManager("DBTreeContextMenu");
        menuManager.setRemoveAllWhenShown(true);
        Menu contextMenu = menuManager.createContextMenu(treeViewer.getTree());
        treeViewer.getTree().setMenu(contextMenu);

        menuManager.addMenuListener(new IMenuListener() {

            public void menuAboutToShow(IMenuManager manager) {

                actionGroup.fillContextMenu(manager);
            }
        });      
        
        IActionBars bars = getViewSite().getActionBars();
		IMenuManager menuManagerBars = bars.getMenuManager();
		menuManagerBars.add( new NewDriverAction(getSite().getShell()) );
        
        getSite().registerContextMenu(menuManager, treeViewer);
        
        if( null != rootNodeViewDBBrowse 
        		&& null != rootNodeViewDBBrowse.getChildNodes()
        		&& rootNodeViewDBBrowse.getChildNodes().length > 0 ){
        	StructuredSelection selected = new StructuredSelection(rootNodeViewDBBrowse.getChildNodes()[0]);
        	treeViewer.setSelection(selected);
        }
		
        /*树视图监听事件 end*/
	}
	

	/**
     * Adds a new user
     * @param user
     */
    public void addManagedDriver( ManagedDriver managedDriver ) throws SQLCannotConnectException {
    	// Make sure we list each user only once
    	for (ManagedDriver driverTmp : managedDrivers)
    		if (driverTmp.getId().trim().equalsIgnoreCase( managedDriver.getId().trim() ))
    			return;
    	
        managedDrivers.add(managedDriver);
        
        ManageDriverNode connectionInfoNode = null;
		try {
			connectionInfoNode = new ManageDriverNode( managedDriver.getId(), null );
			connectionInfoNode.setManagerDriver(managedDriver);
		} catch (SQLException e1) {
		}
		
		if( null == connectionInfoNode ){
			return ;
		}
        
        if( null == rootNodeViewDBBrowse ){
        	rootNodeViewDBBrowse = new RootNode(connectionInfoNode);
        }else{
        	rootNodeViewDBBrowse.addChildNode(connectionInfoNode);
        }
    	
        // set input session
        treeViewer.setInput(rootNodeViewDBBrowse);

    }
    
	@Override
	public void setFocus() {

	}

    /**
     * Remove all items from parent
     */
    private void clearParent() {
        Control[] children = _parent.getChildren();
        if (children != null) {
            for (int i = 0; i < children.length; i++) {
                children[i].dispose();
            }
        }
    }
    
	public RootNode getRootNodeViewDBBrowse() {
		return rootNodeViewDBBrowse;
	}


	public void setRootNodeViewDBBrowse(RootNode rootNodeViewDBBrowse) {
		this.rootNodeViewDBBrowse = rootNodeViewDBBrowse;
	}

	public java.util.List<ManagedDriver> getAllManagedDrivers() {
		return managedDrivers;
	}


	public DBTreeView getTreeViewer() {
		return treeViewer;
	}

}
