package com.dengjk.eclipse.plugin.codegeneration.ui.dbadapterinitial;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.dengjk.eclipse.plugin.codegeneration.dbinfo.ColumnInfo;
import com.dengjk.eclipse.plugin.codegeneration.utils.DataTypeMapperUtils;

import net.sourceforge.sqlexplorer.dbstructure.nodes.TableNode;
import net.sourceforge.squirrel_sql.fw.sql.IndexInfo;
import net.sourceforge.squirrel_sql.fw.sql.TableColumnInfo;

public class TableAndColumnInfoInitial {

	/**
	 * 从tableNode中获取 TBTableColumnInfo[]
	 * @param tableNode
	 * @return
	 */
	public static List<ColumnInfo> getColumnInfosFromTableNode(final TableNode tableNode){
		TableColumnInfo[] allCols = null;
		List<ColumnInfo> allTBCols = null;
		//获取TableColumnInfo[]
		try {
			allCols = tableNode.getSession().getMetaData().getColumnInfo(tableNode.getTableInfo());
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if( null != allCols ){
			allTBCols = TableColumnInfoAdapter.adpateTableColumnInfos(allCols);
			
			// 填充其他信息
			for(ColumnInfo columnInfo : allTBCols ){
				String attrType = DataTypeMapperUtils.getJavaType(columnInfo.getTypeName());
				columnInfo.setAttrType(attrType);
			}
			
			// 索引标记
			setIndexKey(tableNode, allTBCols);
		}
		return allTBCols;
	}
	
	/**
	 * 设置是否是Index
	 * 
	 * @param tableNode 不允许为null
	 * @param allTBCols 不允许为null
	 */
	private static void setIndexKey(final TableNode tableNode, final List<ColumnInfo> allTBCols){
		try {
			List<IndexInfo> indexList = tableNode.getSession().getMetaData().getIndexInfo(tableNode.getTableInfo());
			if( null != indexList && !indexList.isEmpty() 
					&& null != allTBCols && allTBCols.size() > 0 ){
				for( ColumnInfo columnInfo : allTBCols ){
					for( IndexInfo indexInfo: indexList ){
						if(	null != indexInfo.getColumnName() && null != columnInfo.getColumnName()
								&& indexInfo.getColumnName().equals(columnInfo.getColumnName())){
							columnInfo.setIndexKey(true);
							break;
						}
					}
				}
			}
			// 主键列
			Set<String> primaryKeyColumns = new HashSet<String>();
			if(tableNode.getPrimaryKeyNames() != null && tableNode.getPrimaryKeyNames().size() > 0){
				primaryKeyColumns.addAll(tableNode.getPrimaryKeyNames());
			}
			if(!primaryKeyColumns.isEmpty()){
				for( ColumnInfo columnInfo : allTBCols ){
					if(primaryKeyColumns.contains(columnInfo.getColumnName())){
						columnInfo.setPrimaryKey(true);
					}
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
}
