package com.dengjk.eclipse.plugin.codegeneration.ui.preferences;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants;

/**
 * 类说明:首选项配置
 * 
 */
public class PropBean {

	private boolean isNeedOpenFile = false;
	
	private String userName = CodeGenerationUIConstants.EMPTY_STRING;
	
	private String templatePath = CodeGenerationUIConstants.EMPTY_STRING;
	
	private String velocityPath = CodeGenerationUIConstants.CHARACTER_ENCODING;
	
	private String fileCharset = CodeGenerationUIConstants.CHARACTER_ENCODING;
	
	private boolean isneedBackUpFile = false;
	
	public boolean isNeedOpenFile() {
		return isNeedOpenFile;
	}

	public void setNeedOpenFile(boolean isNeedOpenFile) {
		this.isNeedOpenFile = isNeedOpenFile;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getVelocityPath() {
		return velocityPath;
	}

	public void setVelocityPath(String velocityPath) {
		this.velocityPath = velocityPath;
	}

	public boolean isIsneedBackUpFile() {
		return isneedBackUpFile;
	}

	public void setNeedBackUpFile(boolean isneedBackUpFile) {
		this.isneedBackUpFile = isneedBackUpFile;
	}

	public String getFileCharset() {
		return fileCharset;
	}

	public void setFileCharset(String fileCharset) {
		this.fileCharset = fileCharset;
	}

	
	
}
