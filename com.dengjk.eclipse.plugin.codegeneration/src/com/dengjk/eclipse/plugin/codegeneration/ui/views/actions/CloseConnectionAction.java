package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import net.sourceforge.sqlexplorer.dbstructure.nodes.AbstractNode;
import net.sourceforge.sqlexplorer.dbstructure.nodes.DatabaseNode;
import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;

import org.eclipse.jface.resource.ImageDescriptor;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.ImageUtil;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewDBBrowse;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewTableInfo;

/**
 * 类说明:DB Browse 视图 右键后关闭数据库连接的菜单
 * 
 */
public class CloseConnectionAction extends AbstractDBTreeContextAction {

    private static final ImageDescriptor _image = ImageUtil.getDescriptor("Images.DeleteConnection");
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
        return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.CloseConnection");
    }

    public void run() {
        
        // refresh structure view
        ViewDBBrowse structureView = CodeGenerationUIPlugin.getDefault().getDatabaseStructureView();    
    	  
        // refresh nodes
        for (int i = 0; i < _selectedNodes.length; i++) {
        	DatabaseNode databaseNode = (DatabaseNode)_selectedNodes[i];
    		INode parent = databaseNode.getParent();
        	if( null != parent && parent instanceof AbstractNode ){
        		AbstractNode connectionInfoNode = (AbstractNode)parent;
        		connectionInfoNode.removeChildNode(_selectedNodes[i]);
        	}
        }
        
        _treeViewer.refresh();

        // refresh detail view
        ViewTableInfo detailView = (ViewTableInfo) CodeGenerationUIPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(ViewTableInfo.class.getName());
        structureView.getTreeViewer().synchronizeDetailView(detailView);
    }

    public boolean isAvailable() {

        if (null == _selectedNodes || _selectedNodes.length == 0) {
            return false;
        }
        if (_selectedNodes[0] instanceof DatabaseNode) {
            return true;
        }
        return false;
    }
}
