package com.dengjk.eclipse.plugin.codegeneration.ui.preferences;


import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;

/**
 * 类说明:首选项基本配置
 * 
 */
public class PreferencePageGeneral extends PreferencePage implements IWorkbenchPreferencePage {

    
    public static final String P_NEED_OPEN_FILE = "P_NEED_OPEN_FILE"; //$NON-NLS-1$
    
    public static final String P_NEED_BACKUP_FILE = "P_NEED_BACKUP_FILE"; //$NON-NLS-1$
    
    public static final String P_TEMPLATE_PATH = "P_TEMPLATE_PATH"; //$NON-NLS-1$
    
    public static final String P_USER_NAME = "P_USER_NAME"; //$NON-NLS-1$
    
    public static final String P_VELOCITY_PROP = "P_VELOCITY_PROP"; //$NON-NLS-1$
    
    public static final String P_FILE_CHARSET = "P_FILE_CHARSET";
    
    private IPreferenceStore preferenceStore;
    
    private Composite parent;
	
	private Button btnNeedOpenFileYes = null;
	
	private Button btnNeedOpenFileNo = null;
	
	private Button btnNeedBackupFileYes = null;
	
	private Button btnNeedBackupFileNo = null;
	
	private Text txtTemplatePath = null;
	
	private Text txtUser = null;
	
	private Text txtVelocityPath = null;
	
	private Combo fileCharset = null;
	
    /**
     * Create the preference page.
     */
    public PreferencePageGeneral() {
        setPreferenceStore(CodeGenerationUIPlugin.getDefault().getPreferenceStore());
    }

    /**
     * Create contents of the preference page.
     * @param parent
     */
    public Control createContents(Composite parent) {
        
        this.parent = parent;
        
        preferenceStore = CodeGenerationUIPlugin.getDefault().getPreferenceStore();
        
        Composite composite = new Composite(parent, SWT.NULL);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        composite.setLayout(layout);
        GridData gd = new GridData();
        gd.horizontalSpan = 2;
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        Group group = new Group(composite, SWT.NONE);
        layout = new GridLayout();
        layout.numColumns = 3;
        group.setLayout(layout);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 3;
        group.setLayoutData(gd);
        group.setText(CodeGenerationSubclipseMessages.getString("PREFER_GROUP_BASICINFO"));

        
        Label labelForTemplatePath = new Label(group, SWT.END);
        labelForTemplatePath.setText(CodeGenerationSubclipseMessages.getString("PREFER_TEMPLATE_PATH"));
        
        txtTemplatePath = new Text(group, SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL); 
        gd.horizontalSpan = 2;    
        txtTemplatePath.setLayoutData(gd);
        
        Label label=new Label(group, SWT.END);
        label.setText(CodeGenerationSubclipseMessages.getString("PREFER_LABLE_AUTHOR"));
        
        txtUser = new Text(group, SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL); 
        gd.horizontalSpan = 2;    
        txtUser.setLayoutData(gd);
        
        
        label = new Label(group, SWT.NONE);
        label.setText(CodeGenerationSubclipseMessages.getString("PREFER_LABLE_VELOCITY_FILE"));
        
        txtVelocityPath = new Text(group, SWT.BORDER);
        txtVelocityPath.setToolTipText(CodeGenerationSubclipseMessages.getString("PREFER_LABLE_VELOCITY_FILE"));
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        txtVelocityPath.setLayoutData(gd);
        
        label = new Label(group, SWT.NONE);
        label.setText(CodeGenerationSubclipseMessages.getString("PREFER_LABLE_FILE_ENCODING"));
        
        fileCharset = new Combo(group, SWT.BORDER|SWT.READ_ONLY);
        fileCharset.setToolTipText(CodeGenerationSubclipseMessages.getString("PREFER_LABLE_FILE_ENCODING"));
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        fileCharset.add("UTF-8",0);
        fileCharset.add("GBK", 1);
        fileCharset.add("ISO-8859-1", 2);
        fileCharset.setLayoutData(gd);
        
        label = new Label(group, SWT.NONE);
        label.setText(CodeGenerationSubclipseMessages.getString("PREFER_BACKPU_FILE"));
        
        btnNeedBackupFileYes = new Button(group, SWT.RADIO);
        btnNeedBackupFileYes.setToolTipText(CodeGenerationSubclipseMessages.getString("PREFER_BACKPU_FILE_YES"));
        btnNeedBackupFileYes.setText(CodeGenerationSubclipseMessages.getString("PREFER_BACKPU_FILE_YES"));  
        gd = new GridData(GridData.FILL_VERTICAL); 
        gd.horizontalSpan = 1; 
        btnNeedBackupFileYes.setLayoutData(gd);
        
        btnNeedBackupFileNo = new Button(group, SWT.RADIO);
        btnNeedBackupFileNo.setToolTipText(CodeGenerationSubclipseMessages.getString("PREFER_BACKPU_FILE_NO"));
        btnNeedBackupFileNo.setText(CodeGenerationSubclipseMessages.getString("PREFER_BACKPU_FILE_NO"));  
        gd = new GridData(GridData.FILL_VERTICAL); 
        gd.horizontalSpan = 1; 
        btnNeedBackupFileNo.setLayoutData(gd);
        
       
        
        label = new Label(group, SWT.NONE);
        label.setText(CodeGenerationSubclipseMessages.getString("PREFER_BTN_OPENFILE"));

        btnNeedOpenFileYes = new Button(group, SWT.RADIO);
        btnNeedOpenFileYes.setToolTipText(CodeGenerationSubclipseMessages.getString("PREFER_BTN_OPENFILE_YES"));
        btnNeedOpenFileYes.setText(CodeGenerationSubclipseMessages.getString("PREFER_BTN_OPENFILE_YES"));  
        gd = new GridData(GridData.FILL_VERTICAL); 
        gd.horizontalSpan = 1; 
        btnNeedOpenFileYes.setLayoutData(gd);
        
        btnNeedOpenFileNo = new Button(group, SWT.RADIO);
        btnNeedOpenFileNo.setToolTipText(CodeGenerationSubclipseMessages.getString("PREFER_BTN_OPENFILE_NO"));
        btnNeedOpenFileNo.setText(CodeGenerationSubclipseMessages.getString("PREFER_BTN_OPENFILE_NO"));  
        gd = new GridData(GridData.FILL_VERTICAL); 
        gd.horizontalSpan = 1; 
        btnNeedOpenFileNo.setLayoutData(gd);
        
        
        initialize();
        
        return composite;
    }
    
    
    /**
     * 参数初始化
     */
    private void initialize() {
    	PropBean propBean = PreferenceInfoReader.getPropBeanFromPreference();
    	if(propBean.isNeedOpenFile()){
    		btnNeedOpenFileYes.setSelection(true);
    	}else{
    		btnNeedOpenFileNo.setSelection(true);
    	}
    	if(propBean.isIsneedBackUpFile()){
    		btnNeedBackupFileYes.setSelection(true);
    	}else{
    		btnNeedBackupFileNo.setSelection(true);
    	}
    	txtTemplatePath.setText(propBean.getTemplatePath());
        txtUser.setText(propBean.getUserName());
        int selected = "UTF-8".equalsIgnoreCase(propBean.getFileCharset())? 0: ("GBK".equalsIgnoreCase(propBean.getFileCharset())? 1 : 2);
        fileCharset.select(selected);
        
    }
    
    /**
     * Initialize the preference page.
     */
    public void init(IWorkbench workbench) {
        setPreferenceStore(CodeGenerationUIPlugin.getDefault().getPreferenceStore());
    }

    @Override
    protected void performDefaults(){
        preferenceStore = CodeGenerationUIPlugin.getDefault().getPreferenceStore();
        btnNeedOpenFileNo.setSelection(false);
        btnNeedBackupFileYes.setSelection(true);
    }
    
    @Override
    protected void performApply(){
        performOk();
    }
    

    @Override
    public boolean performOk(){
        preferenceStore = CodeGenerationUIPlugin.getDefault().getPreferenceStore();
        preferenceStore.setValue(P_NEED_OPEN_FILE, Boolean.toString(btnNeedOpenFileYes.getSelection()));
        preferenceStore.setValue(P_NEED_BACKUP_FILE, Boolean.toString(btnNeedBackupFileYes.getSelection()));
        preferenceStore.setValue(P_TEMPLATE_PATH, txtTemplatePath.getText());
        preferenceStore.setValue(P_USER_NAME, txtUser.getText());
       
        int selectIndex = fileCharset.getSelectionIndex();
        String fileCharsetName = fileCharset.getItem(selectIndex);
        preferenceStore.setValue(P_FILE_CHARSET, fileCharsetName);
        
        return true;
        
    }
    
}
