package com.dengjk.eclipse.plugin.codegeneration.ui.wizards.driver;

import static com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants.EMPTY_STRING;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import net.sourceforge.sqlexplorer.dbproduct.Alias;
import net.sourceforge.sqlexplorer.dbproduct.Locations;
import net.sourceforge.sqlexplorer.dbproduct.ManagedDriver;
import net.sourceforge.sqlexplorer.dbproduct.User;
import net.sourceforge.squirrel_sql.fw.sql.SQLDriverClassLoader;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.CodeGenerationUIUtils;

/**
 * 类说明:数据库设置向导页
 * 
 */
public class NewDriverWizardPage extends WizardPage {
	
	//用户设置的控件值
	private String tagName;
	private String connectionURL;
	private String userName;
	private String password;
	private String className;//当前使用的
	
	//页面上的组件
	private  Combo driverTemplate;
	private  Text driverTag;
	private  Text connctionUrl;
	private  Text user_name;
	private  Text pass_word;
	private  List driverJarsList;
	private  Button btnAddJars;
	private  Button removeJars;
	private  Combo driverClassName;
	private  Button testDriver;
	
	private String currentProjectID;
	
	private final Map<Integer,DriverTempleteDO> driverTempletes;
	
	private Map<Integer,String> lastInputUrl = new HashMap<Integer,String>();
	
	protected NewDriverWizardPage(String pageName){
		super(pageName);
		setTitle(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_TITLE"));
		setMessage(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_TITLE_TIP"),INFORMATION);
		driverTempletes = new HashMap<Integer,DriverTempleteDO>();
		driverTempletes.put(0, new DriverTempleteDO("MySQL Connector/J","jdbc:mysql://[host]:[port]/[db]"));
		driverTempletes.put(1, new DriverTempleteDO("Oracle Connector/J","jdbc:oracle:thin:@[host]:[port]:[oracleSID]"));
	}
	
	@Override
	public void createControl(Composite parent) {
		/*显示界面组件 start*/
		Composite topComposite = new Composite(parent,SWT.NONE);
		topComposite.setLayout(new GridLayout(3,false));
		this.setControl(topComposite);
		
		GridData inputGrid=new GridData(GridData.FILL_HORIZONTAL);//第二列的布局
		//inputGrid.verticalIndent=8;
		inputGrid.horizontalSpan=2;

		driverTemplate = showCombo(topComposite, inputGrid,
				CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_COMBO_DRIVERTEMLATE"),true);//驱动模板的下拉选择框
		for(Integer key:driverTempletes.keySet()){
			driverTemplate.add(driverTempletes.get(key).templateName, key);
		}driverTemplate.select(0);
		
		driverTag = showInputText(topComposite, inputGrid, 
				CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_COMBO_DRIVERTAG"),false);//项目名称
		driverTag.setToolTipText(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_COMBO_DRIVERTAG_TIP"));
		if( currentProjectID != null && !currentProjectID.trim().isEmpty()){
			driverTag.setEnabled(false);
		}
		
		setTip(topComposite,CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_INFORMATION"));
		
		connctionUrl = showInputText(topComposite, inputGrid, 
				CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_CONNECTURL"),false);//数据库连接URL
		connctionUrl.setText(driverTempletes.get(0).getTemplate());
		
		user_name = showInputText(topComposite, inputGrid, 
				CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_USER"),false);//用户名
		
		pass_word = showInputText(topComposite, inputGrid, 
				CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_PWD"),true);

        Label label = new Label(topComposite, SWT.SEPARATOR | SWT.HORIZONTAL );
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 3;
        label.setLayoutData(gd);
        
		showDriverJars(topComposite);
		
		driverClassName=showCombo(topComposite, inputGrid,
				CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_DRIVER_CLASSNAME"), false);
		
		showTestButton(topComposite);
		/*显示界面组件 end*/
		/*--------------------------------------------------------------------------------------------------------*/
		/*添加组件事件 start*/
		connctionUrlFocusLost();
		
		templateSelectionEvent();
		
		driverJarsEvent();
		
		testConnectionEvent();
		
		/*添加组件事件 end*/
		/*--------------------------------------------------------------------------------------------------------*/
		/*组件值回显（用户编辑） start*/
		showValue();
		/*组件值回显（用户编辑）  end*/
	}

	private void connctionUrlFocusLost() {
		connctionUrl.addFocusListener(new FocusAdapter(){
			@Override
			public void focusLost(FocusEvent e) {
				lastInputUrl.put(driverTemplate.getSelectionIndex(), connctionUrl.getText().trim());
			}
			
		});
	}

	private void showValue() {
		if(currentProjectID!=null && currentProjectID.trim()!=""){
			Alias  alias = CodeGenerationUIPlugin.getDefault().getAliasManager().getAlias(currentProjectID.trim());
			driverTag.setText(alias.getName());
			connctionUrl.setText(alias.getUrl());
			User user = alias.getDefaultUser();
			if(user!=null){
				user_name.setText(user.getUserName());
				pass_word.setText(user.getPassword());
			}
			ManagedDriver  driver = alias.getDriver();
			if(driver!=null){
				LinkedList<String> jarList = driver.getJars();
				driverJarsList.removeAll();
				for(String jarPath: jarList){
					driverJarsList.add(jarPath);
				}
				recomputeDriverClassCombo();
				if(driver.getDriverClassName()!=null){
					driverClassName.setText(driver.getDriverClassName());
				}
			}

		}
	}
	
	private void testConnectionEvent(){
		testDriver.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean rst = testConnection();
				if( rst ){
					MessageDialog.openInformation(getShell(), 
							CodeGenerationSubclipseMessages.getString("CONFIRM_TIP"),
							CodeGenerationSubclipseMessages.getString("CONFIRM_TIP_DB"));
				}
			}
		});
	}
	
	/**
	 * 新增或修改驱动jar后的效果
	 */
	private void driverJarsEvent() {
		btnAddJars.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				FileDialog fileDialog = new FileDialog( getShell(), SWT.OPEN);
				fileDialog.setFilterExtensions(new String[]{"*.jar","*.JAR","*.Jar"});
				String fileName = fileDialog.open();
				if(fileName!=null && !fileName.isEmpty()){
					boolean isExist = false;
					for( String jarPath : getDriverJarList() ){
						if( jarPath.equals(fileName) ){
							isExist = true;
						}
					}
					if( isExist ){
						return ;
					}
					driverJarsList.add(fileName);
					recomputeDriverClassCombo();
				}
			}
		});
		removeJars.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(driverJarsList.getSelectionIndex( )> -1){
					driverJarsList.remove(driverJarsList.getSelectionIndex());
					recomputeDriverClassCombo();
				}
			}
		});
	}

	/**
	 * 当新增了Jar或修改了Jar之后，重新计算driver
	 */
	private void recomputeDriverClassCombo() {
		URL[] urls = new URL[this.getDriverJarList().size()];
		int index = 0;
		for( String jarPath : this.getDriverJarList() ){
			File file = new File(jarPath);
			URL fileUrl = null;
			try {
				fileUrl = file.toURI().toURL();
			} catch (MalformedURLException e) {
			}
			urls[ index++ ] = fileUrl;
		}
		driverClassName.removeAll();
		if( null == urls || urls.length == 0 ){
			return ;
		}
		Set<String> classSet = new HashSet<String>();//用于排除重复的class名
    	SQLDriverClassLoader cl = new SQLDriverClassLoader(ClassLoader.getSystemClassLoader(), urls );
        try {
            Class<?>[] classes = cl.getAssignableClasses(Driver.class);
            if( null != classes && classes.length > 0 ){
                for (Class<?> classTmp: classes ) {
                	String classTmpName = classTmp.getName();
                	if( !classSet.contains( classTmpName )){
                		classSet.add(classTmpName);
                    	driverClassName.add(classTmpName);
                	}
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (driverClassName.getItemCount() > 0) {
        	driverClassName.setText(driverClassName.getItem(0));
        }
	}
	
	private void templateSelectionEvent() {
		driverTemplate.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(lastInputUrl.get(driverTemplate.getSelectionIndex())!=null 
						&& !lastInputUrl.get(driverTemplate.getSelectionIndex()).isEmpty()){
					connctionUrl.setText(lastInputUrl.get(driverTemplate.getSelectionIndex()));
				}else{
					DriverTempleteDO  dtd = driverTempletes.get(driverTemplate.getSelectionIndex());
					if(dtd.getTemplate()!=null){
						connctionUrl.setText(dtd.getTemplate());
					}
				}
			}
		});
	}
	
	private void showTestButton(Composite topComposite) {
		testDriver = new Button(topComposite,SWT.NULL);
		GridData testGrid=new GridData(SWT.BEGINNING);
		testGrid.verticalIndent = 10;
		testDriver.setText(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_BTN_TESTDRIVER"));
		testDriver.setLayoutData(testGrid);
	}
	
	/*
	 * 显示驱动jar组件
	 */
	private void showDriverJars(Composite topComposite) {
		
		setLable(topComposite,CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_LABEL_JARS"),true);
		driverJarsList = new List(topComposite,SWT.BORDER | SWT.V_SCROLL);
        
		GridData driverJarsListlayoutData = new GridData(GridData.FILL_HORIZONTAL);
		driverJarsListlayoutData.horizontalSpan=2;
		driverJarsListlayoutData.verticalSpan=2;
		driverJarsListlayoutData.heightHint = 75;
		driverJarsList.setLayoutData(driverJarsListlayoutData);        
		
		btnAddJars= new Button(topComposite,SWT.NONE);
		btnAddJars.setText(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_LABEL_ADDJARS"));
		btnAddJars.setToolTipText(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_LABEL_ADDJARS_TIP"));
		
		removeJars = new Button(topComposite,SWT.NONE);
		removeJars.setText(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_LABEL_REMOVEJARS"));
		btnAddJars.setToolTipText(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_LABEL_REMOVEJARS_TIP"));
		
	}
	
	/*
	 * 显示Comb组件
	 */
	private Combo showCombo(Composite topComposite, GridData inputGrid,String name,boolean readOnly) {
		setLable(topComposite,name,false);
		Combo combo = null;
		if(readOnly){
			combo = new Combo(topComposite,SWT.READ_ONLY);
		}else{
			combo = new Combo(topComposite,SWT.NONE);
		}
		
		combo.setLayoutData(inputGrid);
		return combo;
	}
	/*
	 * 显示普通输入框组件
	 */
	private Text showInputText(Composite topComposite, GridData inputGrid,
			String lableName,boolean isPassword) {
		setLable(topComposite,lableName,false);
		Text text = null;
		if(isPassword){
			text = new Text(topComposite,SWT.BORDER | SWT.PASSWORD);
		}else{
			text = new Text(topComposite,SWT.BORDER);
		}
		text.setLayoutData(inputGrid);
		return text;
	}
	
	private void setLable(Composite topComposite,String text,boolean fullRow) {
		Label driverLable = new Label(topComposite,SWT.NONE);
		GridData labelGrid=new GridData(SWT.BEGINNING);
		//labelGrid.widthHint=100;
		if(fullRow){
			labelGrid.horizontalSpan = 3;
		}
		driverLable.setLayoutData(labelGrid);
		driverLable.setText(text);
	}
	
	private void setTip(Composite topComposite,String text) {
		Label lable = new Label(topComposite,SWT.NONE);
		GridData labelGrid=new GridData(SWT.BEGINNING);
		labelGrid.horizontalSpan = 3;
		lable.setLayoutData(labelGrid);
		lable.setText(text);
		lable.setForeground(CodeGenerationUIUtils.getColor(SWT.COLOR_RED));
	}
	
	public String getTagName() {
		if(driverTag!=null){
			tagName = driverTag.getText();
		}
		return tagName;
	}
	
	public String getConnectionURL() {
		if(connctionUrl!=null){
			connectionURL = connctionUrl.getText();
		}
		return connectionURL;
	}
	
	public String getUserName() {
		if(user_name!=null){
			userName = user_name.getText();
		}
		return userName;
	}
	
	public String getPassword() {
		if(this.pass_word!=null){
			password = pass_word.getText();
		}
		return password;
	}
	
	private class DriverTempleteDO{
		
		public DriverTempleteDO(String templateName, String template) {
			this.templateName = templateName;
			this.template = template;
		}
		
		private String templateName;
		private String template;
		
		public String getTemplateName() {
			return templateName;
		}
		public void setTemplateName(String templateName) {
			this.templateName = templateName;
		}
		public String getTemplate() {
			return template;
		}
		public void setTemplate(String template) {
			this.template = template;
		}
	}

	/**
	 * 获取当前设置的jar列表
	 * @return
	 */
	public java.util.List<String> getDriverJarList() {
		java.util.List<String> driverJarList = new java.util.ArrayList<String>();
		if(this.driverJarsList!=null){
			for(String path:driverJarsList.getItems()){
				driverJarList.add(path);
			}
		}
		return driverJarList;
	}
	
	public String getClassName() {
		if(this.driverClassName!=null && driverClassName.getText()!=null){
			className = driverClassName.getText();
		}
		return className;
	}
	
	public boolean testConnection(){
		String driverClassName = getClassName();
		String username = getUserName();
		String password = getPassword();
		String url = getConnectionURL();
		if( null == username || username.trim().equals(EMPTY_STRING)
				|| null == password || password.trim().equals(EMPTY_STRING)
				|| null == url || url.trim().equals(EMPTY_STRING)){
			MessageDialog.openError(getShell(), 
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_DB_3"));
			return false;
		}
		if( null == driverClassName || driverClassName.trim().equals(EMPTY_STRING) ){
			MessageDialog.openError(getShell(), 
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_DB_4"));
			return false;
		}
		try {
			boolean rst = testConnection(username, password, url, driverClassName);
			if(!rst){
				MessageDialog.openError(getShell(), 
						CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),
						CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_DB"));
			}
			return rst;
		} catch (Exception e1) {
			MessageDialog.openError(getShell(), 
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"),
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_DB_2")+e1.getMessage());
		}
		return false;
	}
	
	/*
	 * 用于测试数据库联接
	 */
	public boolean testConnection(String userName, String password, String url,String driverClassName)throws Exception{
		
		URL[] urls = new URL[this.getDriverJarList().size()];
		int index = 0;
		for( String jarPath : this.getDriverJarList() ){
			File file = new File(jarPath);
			URL fileUrl = null;
			try {
				fileUrl = file.toURI().toURL();
			} catch (MalformedURLException e) {
			}
			urls[ index++ ] = fileUrl;
		}
		if( null == urls || urls.length == 0 ){
			new Exception(CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_DB_5"));
		}
		SQLDriverClassLoader loader = null;
        if (driverClassName!=null) {
			loader = new SQLDriverClassLoader(getClass().getClassLoader(),urls);
			Properties props = new Properties();
			if ( userName != null) props.put("user", userName);
			if ( password != null) props.put("password", password);
			
	        Driver jdbcDriver;
			try {
				Class<?> driverCls = loader.loadClass(driverClassName);
				jdbcDriver = (Driver)driverCls.newInstance();
			} catch (Exception e) {
				throw e;
			}
			if( null == jdbcDriver){
				throw new Exception(CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_DB_6"));
			}
			
			Connection jdbcConn = null;
			try {
				jdbcConn = jdbcDriver.connect(Locations.expandWorkspace( url ), props);
			} catch (SQLException e) {
				throw e;
			}finally{
				if (jdbcConn != null) {
					try {
						jdbcConn.close();
					} catch (Exception e) {
					}
					jdbcDriver=null;
					loader = null;
					return true;
				}
			}
        }
		return false;
	}

	public void setCurrentProjectID(String currentProjectID) {
		this.currentProjectID = currentProjectID;
	}
}
