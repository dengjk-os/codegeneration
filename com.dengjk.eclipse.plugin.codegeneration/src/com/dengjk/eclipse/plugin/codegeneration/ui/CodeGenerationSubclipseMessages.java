package com.dengjk.eclipse.plugin.codegeneration.ui;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;


public class CodeGenerationSubclipseMessages {

	private static final String BUNDLE_NAME = CodeGenerationSubclipseMessages.class.getPackage().getName()+".codegenerationMessage";

	private static ResourceBundle[] resources = null;

	private CodeGenerationSubclipseMessages() {
	}

	public static String getString(String key) {
		init();
		for (int i = 0; i < resources.length; i++) {
			try {
				if (resources[i] != null){
					return resources[i].getString(key);
				}
			} catch (MissingResourceException e) {
			}
		}

		return '!' + key + '!';
	}

	private static synchronized void init() {
		if (resources == null) {
			Bundle mainPlugin = CodeGenerationUIPlugin.getPlugin().getBundle();
			Bundle[] fragments = Platform.getFragments(mainPlugin);

			if (fragments == null) {
				fragments = new Bundle[0];
			}

			resources = new ResourceBundle[fragments.length + 1];

			resources[0] = ResourceBundle.getBundle(BUNDLE_NAME);
		}
	}

}
