
package com.dengjk.eclipse.plugin.codegeneration.ui.wizards.driver;

import net.sourceforge.sqlexplorer.dbproduct.Alias;
import net.sourceforge.sqlexplorer.dbproduct.AliasManager;
import net.sourceforge.sqlexplorer.dbproduct.DriverManager;
import net.sourceforge.sqlexplorer.dbproduct.ManagedDriver;
import net.sourceforge.sqlexplorer.dbproduct.User;

import static com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants.EMPTY_STRING;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.exception.SQLCannotConnectException;

/**
 * 类说明:数据库连接以及分库分表设置向导页
 * 
 */
public class NewDriverWizard extends Wizard {
	private boolean edit = false;//true:编辑 ;false:新建
	private NewDriverWizardPage newDriverWizardPage;//数据库连接设置页面
	
	private String currentProjectID;
	public void addPages() {
		newDriverWizardPage = new NewDriverWizardPage(CodeGenerationSubclipseMessages.getString("PAGE_NEWDRIVER_TITLE"));
		newDriverWizardPage.setCurrentProjectID(currentProjectID);
		//tddlSeting = new TddlSetingWizardPage("分库分表设置");
		
		addPage(newDriverWizardPage);
		//addPage(tddlSeting);
    }
	
	@Override
	public boolean performFinish() {
		if( null == newDriverWizardPage.getTagName() 
				|| newDriverWizardPage.getTagName().trim().equals(EMPTY_STRING)){
			MessageDialog.openError(getShell(), 
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR"), 
					CodeGenerationSubclipseMessages.getString("CONFIRM_ERROR_DB_7"));
			return false;
		}
		
    	boolean rst = newDriverWizardPage.testConnection();
		if( !rst ){
			return false;
		}
		
		DriverManager driverManager = CodeGenerationUIPlugin.getDefault().getDriverModel();
		
		//在DB视图中显示Drivers视图
		boolean isTest = false;
		if( isTest ){
			test();
		}
		
		//保存driver、Alias
		ManagedDriver managedDriver = null;
		AliasManager aliasManager = null;
		aliasManager = CodeGenerationUIPlugin.getDefault().getAliasManager();
		Alias alias = null;
		if(edit){
			managedDriver = driverManager.getDriver((newDriverWizardPage.getTagName()));
            alias = aliasManager.getAlias(newDriverWizardPage.getTagName());
            instalvalue(managedDriver, alias);
        }else{
        	managedDriver = new ManagedDriver(newDriverWizardPage.getTagName());
        	alias = new Alias();
           
        	instalvalue(managedDriver, alias);
            
            try {
                driverManager.addDriver(managedDriver);
    		} catch (Exception e) {
    		}
    		
    		try {
    			aliasManager.addAlias(alias);
    		} catch (Exception e) {
    		}
            
            try {
            	CodeGenerationUIPlugin.getDefault().getDatabaseStructureView().addManagedDriver(managedDriver);
    		} catch (SQLCannotConnectException e1) {
    			e1.printStackTrace();
    		}
        }
		
        
		return true;
	}

	private void instalvalue(ManagedDriver managedDriver, Alias alias) {
		if(managedDriver!=null && alias!=null){
			managedDriver.setDriverClassName(newDriverWizardPage.getClassName());
			managedDriver.setName(managedDriver.getId());
			managedDriver.setJars(newDriverWizardPage.getDriverJarList().toArray(
					new String[newDriverWizardPage.getDriverJarList().size()]));

			alias.setName(managedDriver.getId());
			alias.setDriver(managedDriver);
			alias.setUrl(newDriverWizardPage.getConnectionURL());
			User userAlias = new User( newDriverWizardPage.getUserName(), newDriverWizardPage.getPassword() );
			alias.setHasNoUserName(false);
			alias.setDefaultUser(userAlias);
			alias.setConnectAtStartup(false);
			alias.setAutoLogon(true);
		}
	}
	
	/*
	 * 连库的那几项都是必填的，为空的话不能走下一步 
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#canFinish()
	 */
	public boolean canFinish() {
		return super.canFinish();
	}
	
	private void test(){
        ManagedDriver driver = new ManagedDriver("toolStore");
    	DriverManager driverManager = CodeGenerationUIPlugin.getDefault().getDriverModel();
        
        try {
			CodeGenerationUIPlugin.getDefault().getDriverModel().addDriver(driver);
		} catch (Exception e) {
		}
        
        driver.setDriverClassName("com.mysql.jdbc.Driver");
        driver.setName(driver.getId());
        driver.setJars(new String[]{"D:/worksoft/java/mysql/mysql-connector-java-5.1.7/mysql-connector-java-5.1.7-bin.jar"});
        
        driverManager.addDriver(driver);
		
		ManagedDriver driver2 = new ManagedDriver("jiechen");
        
		driver2.setDriverClassName("com.mysql.jdbc.Driver");
		driver2.setName(driver.getId());
		driver2.setJars(new String[]{"D:/worksoft/java/mysql/mysql-connector-java-5.1.7/mysql-connector-java-5.1.7-bin.jar"});
		
        driverManager.addDriver(driver2);
		
        try {
        	//AutoDAOUIPlugin.getDefault().getDatabaseStructureView().addUser(user2);
        	CodeGenerationUIPlugin.getDefault().getDatabaseStructureView().addManagedDriver(driver2);
		} catch (SQLCannotConnectException e1) {
			e1.printStackTrace();
		}
	}
	
	public void initialise(String projectID){
		if(projectID!=null && !projectID.trim().isEmpty()){
			
		}
	}

	public void setCurrentProjectID(String currentProjectID) {
		this.currentProjectID = currentProjectID;
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

}
