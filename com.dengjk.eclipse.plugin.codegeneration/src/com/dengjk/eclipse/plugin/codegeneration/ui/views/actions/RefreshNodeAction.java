package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import org.eclipse.jface.resource.ImageDescriptor;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes.ManageDriverNode;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.ImageUtil;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewDBBrowse;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewTableInfo;


/**
 * 类说明:DB Browse 视图 右键后刷新功能
 * 
 */
public class RefreshNodeAction extends AbstractDBTreeContextAction {

    private static final ImageDescriptor _image = ImageUtil.getDescriptor("Images.RefreshIcon");
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
        return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.Refresh");
    }

    public void run() {
                
        // refresh nodes
        for (int i = 0; i < _selectedNodes.length; i++) {
            _selectedNodes[i].refresh();
        }
        
        // refresh structure view
        ViewDBBrowse structureView = CodeGenerationUIPlugin.getDefault().getDatabaseStructureView();       
        _treeViewer.refresh();

        // refresh detail view
        ViewTableInfo detailView = (ViewTableInfo) CodeGenerationUIPlugin.getDefault().getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(ViewTableInfo.class.getName());
        structureView.getTreeViewer().synchronizeDetailView(detailView);
    }

	

    public boolean isAvailable() {

        if (null == _selectedNodes || _selectedNodes.length == 0) {
            return false;
        }
        if (_selectedNodes[0] instanceof ManageDriverNode) {
            return false;
        }
        return true;
    }
}
