package com.dengjk.eclipse.plugin.codegeneration.ui.views.provider;

import static com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants.EMPTY_STRING;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;

/**
 * 类说明:Table Info
 * 
 */
public class TableInfoLabelProvider implements ITableLabelProvider{

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		if( columnIndex > 0 ){
			return null;
		}
		Object[] dataRows = (Object[])element;
		if( null == dataRows || dataRows.length == 0 || null == dataRows[columnIndex] ){
			return null;
		}
		try {
			boolean isPrimaryKey = (Boolean)dataRows[dataRows.length - 2];
			if( isPrimaryKey ){
				ImageDescriptor _image = CodeGenerationUIPlugin.getImageDescriptor(CodeGenerationSubclipseMessages.getString("Images.PKColumn"));
				return _image.createImage();
			}
			boolean isIndexKey = (Boolean)dataRows[dataRows.length - 1];
			if( isIndexKey ){
				ImageDescriptor _image = CodeGenerationUIPlugin.getImageDescriptor(CodeGenerationSubclipseMessages.getString("Images.IndexIcon"));
				return _image.createImage();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		Object[] dataRows = (Object[])element;
		if( null == dataRows || dataRows.length == 0 || null == dataRows[columnIndex] ){
			return EMPTY_STRING;
		}
		return String.valueOf(dataRows[columnIndex]);
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		
	}

}
