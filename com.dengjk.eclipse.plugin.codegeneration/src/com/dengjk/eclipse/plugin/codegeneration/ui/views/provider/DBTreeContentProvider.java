package com.dengjk.eclipse.plugin.codegeneration.ui.views.provider;

import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * 类说明:Content provider for database structure outline.
 * 
 */
public class DBTreeContentProvider implements ITreeContentProvider {

    public void dispose() {
    }

    public Object[] getChildren(Object parentElement) {

        Object[] children = ((INode) parentElement).getChildNodes();
        return children;
    }

    /**
     * Return all the children of an INode element.
     * 
     * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
     */
    public Object[] getElements(Object inputElement) {

    	Object[] objs = getChildren(inputElement);
        return objs;
        
    }

    /**
     * Return the parent of an INode element.
     * 
     * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
     */
    public Object getParent(Object element) {

        Object parent = ((INode) element).getParent();
        return parent;
    }

    public boolean hasChildren(Object element) {

        return ((INode) element).hasChildNodes();
    }

    public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
    }

}
