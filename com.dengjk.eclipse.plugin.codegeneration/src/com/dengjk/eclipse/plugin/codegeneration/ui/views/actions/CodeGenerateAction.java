package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import net.sourceforge.sqlexplorer.dbstructure.nodes.TableNode;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.util.ImageUtil;
import com.dengjk.eclipse.plugin.codegeneration.ui.wizards.WizardDialogCodeGenerationLocation;
import com.dengjk.eclipse.plugin.codegeneration.ui.wizards.generate.CodeGenerateWizard;

/**
 * 类说明:简单生成
 * 
 */
public class CodeGenerateAction extends AbstractDBTreeContextAction {

    private Shell shell;
    
    private static final ImageDescriptor _image = ImageUtil.getDescriptor("Images.GenerateXmlAndJavaSimple");
    
    public CodeGenerateAction(Shell shell){
    	this.shell = shell;
    }
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
        return CodeGenerationSubclipseMessages.getString("DatabaseStructureView.Actions.SampleGenerate");
    }

    public void run() {
		
    	CodeGenerateWizard generateXmlAndJavaWizard = new CodeGenerateWizard( _selectedNodes );
    	generateXmlAndJavaWizard.setNeedsProgressMonitor(true);
		WizardDialog wizardDialog = new WizardDialogCodeGenerationLocation(shell, generateXmlAndJavaWizard, 650, 850); //$NON-NLS-1$
		wizardDialog.setMinimumPageSize(350, 700);
		wizardDialog.open();
		
    }

    public boolean isAvailable() {

        if (null == _selectedNodes || _selectedNodes.length == 0) {
            return false;
        }
        if (_selectedNodes[0] instanceof TableNode) {
            return ((TableNode)_selectedNodes[0]).isTable();
        }
        return false;
    }
}
