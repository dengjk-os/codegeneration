package com.dengjk.eclipse.plugin.codegeneration.ui.exception;

/**
 * 类说明:插件异常类
 * 
 */
public class CodeGenerationException extends Exception {

	private static final long serialVersionUID = -8177088210623670294L;

	public CodeGenerationException() {
		super();
	}

	public CodeGenerationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CodeGenerationException(String arg0) {
		super(arg0);
	}

	public CodeGenerationException(Throwable arg0) {
		super(arg0);
	}

}
