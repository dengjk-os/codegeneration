package com.dengjk.eclipse.plugin.codegeneration.ui.wizards;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Shell;

public class WizardDialogCodeGenerationLocation  extends WizardDialog{
    
    private Shell shell;
    
    private int width;
    private int height;
    
    public WizardDialogCodeGenerationLocation(Shell parentShell, IWizard newWizard, int width, int height) {
        super(parentShell, newWizard);
        this.shell = parentShell;
        this.width = width;
        this.height = height;
        //如果小于0，随便设置一个值
        if( this.width < 0 ){
            this.width = 400;
        }
        if( this.height < 0 ){
            this.height = 250;
        }
    }

    protected void cancelPressed() {
        super.cancelPressed();
    }

    protected void okPressed() {
        super.okPressed();
    }

    protected Point getInitialLocation(Point initialSize) {
        try {
            int width = shell.getMonitor().getClientArea().width;
            int height = shell.getMonitor().getClientArea().height;
            int xp = (width - this.width) / 2;
            int yp = (height - this.height) / 2;
            if( xp < 0 ) xp = 0;
            if( yp < 0 ) yp = 0;
            return new Point(xp, yp);
        } catch (NumberFormatException e) {}
        return super.getInitialLocation(initialSize);
    }
    
    protected Point getInitialSize() {
        try {
            return new Point(this.width, this.height);
        } catch (NumberFormatException e) {}        
         return super.getInitialSize();
    }
}
