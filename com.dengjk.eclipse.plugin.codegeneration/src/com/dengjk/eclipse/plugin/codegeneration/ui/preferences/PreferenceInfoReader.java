package com.dengjk.eclipse.plugin.codegeneration.ui.preferences;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;

/**
 * 类说明:从首选项中读取配置
 * 
 */
public class PreferenceInfoReader {

	/**
	 * 从首选项中读取配置
	 * @return
	 */
	public static PropBean getPropBeanFromPreference(){
		PropBean propBean = new PropBean();
		boolean isNeedOpenFile = false;
        try {
        	String allowOpenFileStr = CodeGenerationUIPlugin.getDefault().getPreferenceStore().getString(PreferencePageGeneral.P_NEED_OPEN_FILE );
            if ( allowOpenFileStr != null && !allowOpenFileStr.trim().isEmpty()) {
            	isNeedOpenFile = Boolean.valueOf(allowOpenFileStr).booleanValue();
            }
        } catch (Exception e) {
        }
        
        boolean isNeedBackupFile = false;
        try {
        	String backupFileStr = CodeGenerationUIPlugin.getDefault().getPreferenceStore().getString(PreferencePageGeneral.P_NEED_BACKUP_FILE);
        	if ( backupFileStr != null && !backupFileStr.trim().isEmpty()) {
        		isNeedBackupFile = Boolean.valueOf(backupFileStr).booleanValue();
        	}
        } catch (Exception e) {
        }
        
        String templatePath = CodeGenerationUIPlugin.getDefault().getPreferenceStore().getString(PreferencePageGeneral.P_TEMPLATE_PATH );
        if( null == templatePath || templatePath.trim().isEmpty()){
        	templatePath = "";
        }
        
        String userName = CodeGenerationUIPlugin.getDefault().getPreferenceStore().getString(PreferencePageGeneral.P_USER_NAME );
        if( null == userName || userName.trim().isEmpty()){
        	userName = System.getProperty("user.name");
        }
        
        String velocityPath = CodeGenerationUIPlugin.getDefault().getPreferenceStore().getString(PreferencePageGeneral.P_VELOCITY_PROP);
        if (null==velocityPath || velocityPath.trim().isEmpty()){
        	velocityPath = "";
        }
        
        String fileCharset = CodeGenerationUIPlugin.getDefault().getPreferenceStore().getString(PreferencePageGeneral.P_FILE_CHARSET);
        if (null==fileCharset || fileCharset.trim().isEmpty()){
        	fileCharset = "UTF-8";
        }
        
        propBean.setTemplatePath(templatePath);
        propBean.setVelocityPath(velocityPath);
        propBean.setUserName(userName);
        propBean.setNeedOpenFile(isNeedOpenFile);
        propBean.setNeedBackUpFile(isNeedBackupFile);
        propBean.setFileCharset(fileCharset);
        
		return propBean;
	}
	
}
