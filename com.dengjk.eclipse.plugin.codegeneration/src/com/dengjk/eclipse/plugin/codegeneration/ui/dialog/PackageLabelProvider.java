package com.dengjk.eclipse.plugin.codegeneration.ui.dialog;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * 类说明：展示可供选择的Package列表。
 * 
 */
public class PackageLabelProvider extends LabelProvider {
	
	@Override
	public String getText(Object element) {
		//显示指定的标签
		if(element instanceof IJavaElement){
			IJavaElement javaElement = (IJavaElement)element;
			if( null != javaElement ){
				return javaElement.getElementName();
			}
		}
		return super.getText(element);
	}
	
	@Override
	public Image getImage(Object element) {
		return super.getImage(element);
	}

}
