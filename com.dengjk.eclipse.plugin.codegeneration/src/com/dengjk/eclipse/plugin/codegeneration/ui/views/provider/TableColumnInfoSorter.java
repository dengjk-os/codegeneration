package com.dengjk.eclipse.plugin.codegeneration.ui.views.provider;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

/**
 * 类说明:ColumnInfo 列排序器
 * 
 */
@SuppressWarnings("deprecation")
public class TableColumnInfoSorter extends ViewerSorter {
	
	/**
	 * 每列对应一个不同的常量
	 *
	 */
	public static final int SORT_TYPE_COLUMN_NAME = 1;
	public static final int SORT_TYPE_TYPE_NAME = 2;
	public static final int SORT_TYPE_TYPE_LENGTH = 3;
	public static final int SORT_TYPE_NULL_NAME = 4;
	
	private int sortType;
	
	private TableColumnInfoSorter(){
		
	}
	
	public static TableColumnInfoSorter generateSorter(int sortType,boolean ascFlag){
		if(ascFlag){
			return new TableColumnInfoSorter(sortType);
		}
		return new TableColumnInfoSorter(-sortType);
	}

	/**
	 * 根据不同的列进行排序
	 *
	 */
	private TableColumnInfoSorter(int sortType){
		this();
		this.sortType = sortType;
	}
	
	public int compare(Viewer viewer,Object obj1,Object obj2){
		Object[] o1=(Object[])obj1;
		Object[] o2=(Object[])obj2;
		if( sortType > 0 ){
			if( null != o1[sortType - 1] && null != o2[sortType - 1]){
				return String.valueOf(o1[sortType - 1]).compareTo(String.valueOf(o2[sortType - 1]));
			}
		}else if( sortType < 0 ){
			if( null != o1[0 - sortType - 1] && null != o2[0 - sortType - 1]){
				return String.valueOf(o2[0 - sortType - 1]).compareTo(String.valueOf(o1[0 - sortType - 1]));
			}
		}
		return 0;
	}
	
	public int getSortType() {
		return sortType;
	}

}