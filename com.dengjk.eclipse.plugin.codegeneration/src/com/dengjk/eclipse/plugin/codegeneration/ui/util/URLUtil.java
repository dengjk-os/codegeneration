package com.dengjk.eclipse.plugin.codegeneration.ui.util;

import java.net.URL;

import org.eclipse.core.runtime.Platform;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;

public class URLUtil {

    private URLUtil() {
    }


    public static URL getResourceURL(String s) {
        if (!initialized)
            init();
        URL url = null;
        try {
            url = new URL(baseURL, s);
        } catch (Throwable e) {
        }
        return url;
    }

    static private boolean initialized = false;


    static private void init() {
        CodeGenerationUIPlugin defaultPlugin = CodeGenerationUIPlugin.getDefault();

        baseURL = defaultPlugin.getBundle().getEntry("/");
        initialized = true;
    }

    private static URL baseURL;





    
    /**
     * Return a URL to a file located in your plugin fragment
     * @param yourPluginId e.g net.sourceforge.sqlexplorer.oracle
     * @param filePath path to file within your fragment e.g. icons/test.gif
     * @return URL to the file.
     */
    public static URL getFragmentResourceURL(String yourPluginId, String filePath) {
        
        URL url = null;
        
        try {
            URL baseURL = Platform.getBundle(yourPluginId).getEntry("/");
            url = new URL(baseURL, filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
     
        return url;
    }
    
}
