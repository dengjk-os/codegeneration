package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IJarEntryResource;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.IPackagesViewPart;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.TableItem;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewGennerateList;

/**
 * 类说明:自动关联选中的文件与代码
 * 
 */
public class GenerateeFileToggleLinkingAction extends Action {

	private IPackagesViewPart fPackageExplorerPart;
	
    private static final ImageDescriptor _image = CodeGenerationUIPlugin.getImageDescriptor(CodeGenerationSubclipseMessages.getString("Images.TaskMemoryLink"));
    
    public ImageDescriptor getImageDescriptor() {
        return _image;
    }

    public String getText() {
        return CodeGenerationSubclipseMessages.getString("ViewGennerateList.Actions.ToggleLinkingAction");
    }

    public String getToolTipText() {
    	return CodeGenerationSubclipseMessages.getString("ViewGennerateList.Actions.ToggleLinkingAction");
    }

	/**
	 * Constructs a new action.
	 * @param explorer the package explorer
	 */
	public GenerateeFileToggleLinkingAction(IPackagesViewPart explorer) {
		//setChecked(explorer.isLinkingEnabled());
		setChecked(true);
		fPackageExplorerPart= explorer;
	}

	/**
	 * Runs the action.
	 */
	public void run() {
		//setChecked(fPackageExplorerPart.isLinkingEnabled());
		setChecked(true);
		boolean isChecked = isChecked();
		if( isChecked ){
			ViewGennerateList viewGennerateList = CodeGenerationUIPlugin.getDefault().getViewGennerateList();
			TableItem[] tableItems = viewGennerateList.getTableViewer().getTable().getSelection();
			if( null != tableItems && tableItems.length > 0 ){
				try {
					IFile ifile = (IFile)tableItems[0].getData();
					TreeViewer fViewer = fPackageExplorerPart.getTreeViewer();
					showInput( ifile, fViewer);
				} catch (Exception e) {
				}
			}
		}
	}
	

	boolean showInput(Object input, TreeViewer fViewer) {
		Object element= null;

		if (input instanceof IFile && isOnClassPath((IFile)input)) {
			element= JavaCore.create((IFile)input);
		}

		if (element == null) // try a non Java resource
			element= input;

		if (element != null) {
			ISelection newSelection= new StructuredSelection(element);
			if (fViewer.getSelection().equals(newSelection)) {
				fViewer.reveal(element);
			} else {
				fViewer.setSelection(newSelection, true);

				while (element != null && fViewer.getSelection().isEmpty()) {
					// Try to select parent in case element is filtered
					element= getParent(element);
					if (element != null) {
						newSelection= new StructuredSelection(element);
						fViewer.setSelection(newSelection, true);
					}
				}
			}
			return true;
		}
		return false;
	}


	private boolean isOnClassPath(IFile file) {
		IJavaProject jproject= JavaCore.create(file.getProject());
		return jproject.isOnClasspath(file);
	}
	
	/**
	 * Returns the element's parent.
	 * @param element the element
	 *
	 * @return the parent or <code>null</code> if there's no parent
	 */
	private Object getParent(Object element) {
		if (element instanceof IJavaElement)
			return ((IJavaElement)element).getParent();
		else if (element instanceof IResource)
			return ((IResource)element).getParent();
		else if (element instanceof IJarEntryResource) {
			return ((IJarEntryResource)element).getParent();
		}
		return null;
	}


}
