package com.dengjk.eclipse.plugin.codegeneration.ui.views.actions;

import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.TreeViewer;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.ViewDBBrowse;


public abstract class AbstractDBTreeContextAction extends Action {

    protected INode[] _selectedNodes;
    
    protected TreeViewer _treeViewer;
    
    /**
     * Store nodes for use in the actions.
     * @param nodes
     */
    public final void setSelectedNodes(INode[] nodes) {
        _selectedNodes = nodes;        
    }


    /**
     * Store treeViewer for use in the actions
     * @param treeViewer
     */
    public void setTreeViewer(TreeViewer treeViewer) {
       _treeViewer = treeViewer;        
    }


    /**
     * Implement this method to return true when your action is available
     * for the selected node(s).  When true, the action will be included in the
     * context menu, when false it will be ignored.
     * 
     * 
     * @return true if the action should be included in the context menu
     */
    public boolean isAvailable() {
        return true;
    }

    /**
     * Implement this method to return true when your action is the default action
     * for the selected node.  When true, the action will be run when a double click
     * on a node occurs. 
     * 
     * 
     * @return true if the action should be included in the context menu
     */
    public boolean isDefault() {
        return false;
    }
    
	protected ViewDBBrowse getView() {
		return CodeGenerationUIPlugin.getDefault().getDatabaseStructureView();
	}
}
