package com.dengjk.eclipse.plugin.codegeneration.ui.views.provider;

import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.dengjk.eclipse.plugin.codegeneration.ui.util.ImageUtil;

/**
 * 类说明:Label provider for database structure outline.
 * 
 */
public class DBTreeLabelProvider extends LabelProvider {

    private Image _defaultNodeImage = ImageUtil.getImage("Images.DefaultNodeIcon");

    private Image _defaultParentNodeImage = ImageUtil.getImage("Images.DefaultParentNodeIcon");


    public void dispose() {

        super.dispose();
        ImageUtil.disposeImage("Images.DefaultNodeIcon");
        ImageUtil.disposeImage("Images.DefaultParentNodeIcon");

    }


    /**
     * Return the image used for the given INode. If the INode does not have an
     * image, default images are returned.
     * 
     * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
     */
    public Image getImage(Object element) {

        INode node = (INode) element;

        // return expanded image if node is expanded and we have an image
        if (node.isExpanded() && node.getExpandedImage() != null && node.getChildNodes() != null
                && node.getChildNodes().length != 0) {
            return node.getExpandedImage();
        }

        // return custom image
        if (node.getImage() != null) {
            return node.getImage();
        }

        // return one of the default images
        if (node.hasChildNodes()) {
            return _defaultParentNodeImage;
        } else {
            return _defaultNodeImage;
        }

    }


    /**
     * Return the text to display the INode.
     * 
     * @see org.eclipse.jface.viewers.ILabelProvider#getText(java.lang.Object)
     */
    public String getText(Object element) {

        INode node = (INode) element;
        String text = node.getLabelText();

        // return default if no label is provided
        if (text == null) {
            text = node.toString();
        }

        if (node.getLabelDecoration() != null) {
            text = text + " [" + node.getLabelDecoration() + "]";
        }

        return text;
    }

}
