package com.dengjk.eclipse.plugin.codegeneration.ui.util;

import static com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIConstants.EMPTY_STRING;

import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

/**
 * 类说明:智能化地设置值
 * 
 */
public class IntelligentSetValueUtil {
	
	
	public static final String RECOMMENDVALUE_SRC_FOLDER1 = "src/main/java";
	
	public static final String RECOMMENDVALUE_SRC_FOLDER2 = "src";
	
	
	/**
	 * 用户指定的packageRoot是否合法
	 * 
	 * @param javaProject
	 * @param formerValue
	 * @return
	 */
	public static boolean isValidPackageRoot(IJavaProject javaProject, String packageRoot){
		String txtSrcFolderValue = EMPTY_STRING;
		if( null != packageRoot ){
			txtSrcFolderValue = packageRoot.trim();
		}
		if( txtSrcFolderValue.isEmpty() ){
			return false;
		}
		
		try {
			IPackageFragmentRoot[] packageFragmentRoots = javaProject.getPackageFragmentRoots();
			if( null != packageFragmentRoots ){
				for( IPackageFragmentRoot packageFragmentRoot : packageFragmentRoots ){
					if (packageFragmentRoot.getKind() == IPackageFragmentRoot.K_SOURCE) {
						IResource resourcePackageRoot = packageFragmentRoot.getResource();
						if( null == resourcePackageRoot ){
							continue;
						}
						if(resourcePackageRoot.getProjectRelativePath().toString().equals(txtSrcFolderValue)){
							return true;
						}
					}
				}
			}
		} catch (Exception e2) {
		}
		return false;
	}
	
	/**
	 * 用户指定的JavaType是否合法
	 * 
	 * @param javaProject
	 * @param javaTypeName
	 * @return
	 */
	public static boolean isValidJavaType(IJavaProject javaProject, String javaTypeName){
		if( null == javaTypeName || javaTypeName.trim().isEmpty() ){
			return false;
		}
		IType type = null;
		try {
			type = javaProject.findType(javaTypeName);
		} catch (JavaModelException e) {
		}
		if( null != type ){
			return true;
		}
		return false;
	}
	
	/**
	 * 智能化地返回src folder的值。<BR/>
	 * 安装用户的习惯，一般都是src或者是src/main/java
	 * 
	 * @param javaProject
	 * @param formerValue 原始值
	 * @return
	 */
	public static String getSrcFolderValue(IJavaProject javaProject){
		if( null == javaProject ){
			return null;
		}
		
		boolean isSrcFolderExist = false;
		boolean isSrcMainJavaFolderExist = false;
		try {
			IPackageFragmentRoot[] packageFragmentRoots = javaProject.getPackageFragmentRoots();
			if( null != packageFragmentRoots ){
				for( IPackageFragmentRoot packageFragmentRoot : packageFragmentRoots ){
					if (packageFragmentRoot.getKind() == IPackageFragmentRoot.K_SOURCE) {
						IResource resourcePackageRoot = packageFragmentRoot.getResource();
						if( null == resourcePackageRoot ){
							continue;
						}
						if(resourcePackageRoot.getProjectRelativePath().toString().equals(RECOMMENDVALUE_SRC_FOLDER2)){
							isSrcFolderExist = true;
						}
						if(resourcePackageRoot.getProjectRelativePath().toString().equals(RECOMMENDVALUE_SRC_FOLDER1)){
							isSrcMainJavaFolderExist = true;
						}
					}
				}
			}
		} catch (Exception e2) {
		}
		if( isSrcFolderExist ){
			return RECOMMENDVALUE_SRC_FOLDER2;
		}
		if( isSrcMainJavaFolderExist ){
			return RECOMMENDVALUE_SRC_FOLDER1;
		}
		return null;
	}	

	/**
	 * 智能化地返回src resources的值。<BR/>
	 * 安装用户的习惯，一般都是resources或者是src/main/resources
	 * 
	 * @param javaProject
	 * @param formerValue 原始值
	 * @return
	 */
	public static String getSrcResourcesValue(IJavaProject javaProject){
		if( null == javaProject ){
			return null;
		}
		
		try {
			IPackageFragmentRoot[] packageFragmentRoots = javaProject.getPackageFragmentRoots();
			if( null != packageFragmentRoots ){
				for( IPackageFragmentRoot packageFragmentRoot : packageFragmentRoots ){
					if (packageFragmentRoot.getKind() == IPackageFragmentRoot.K_SOURCE) {
						IResource resourcePackageRoot = packageFragmentRoot.getResource();
						if( null == resourcePackageRoot ){
							continue;
						}
						
					}
				}
			}
		} catch (Exception e2) {
		}
		
		return null;
	}
	
	/**
	 * 智能化地返回src test case folder的值。<BR/>
	 * 安装用户的习惯，一般都是src或者是src/test/java
	 * 
	 * @param javaProject
	 * @param formerValue 原始值
	 * @return
	 */
	public static String getSrcTestFolderValue(IJavaProject javaProject){
		if( null == javaProject ){
			return null;
		}
		
		try {
			IPackageFragmentRoot[] packageFragmentRoots = javaProject.getPackageFragmentRoots();
			if( null != packageFragmentRoots ){
				for( IPackageFragmentRoot packageFragmentRoot : packageFragmentRoots ){
					if (packageFragmentRoot.getKind() == IPackageFragmentRoot.K_SOURCE) {
						IResource resourcePackageRoot = packageFragmentRoot.getResource();
						if( null == resourcePackageRoot ){
							continue;
						}
					}
				}
			}
		} catch (Exception e2) {
		}
		
		return null;
	}
}
