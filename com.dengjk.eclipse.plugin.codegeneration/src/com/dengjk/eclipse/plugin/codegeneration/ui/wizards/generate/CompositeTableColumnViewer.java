package com.dengjk.eclipse.plugin.codegeneration.ui.wizards.generate;

import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.dengjk.eclipse.plugin.codegeneration.dbinfo.ColumnInfo;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.TableColumnInfoSorter;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.TableInfoContentProvider;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.provider.TableInfoLabelProvider;

import net.sourceforge.sqlexplorer.dbstructure.nodes.TableNode;

/**
 * 类说明:ColumnInfo Table视图
 * 
 */
public class CompositeTableColumnViewer extends Composite {
	
	private CheckboxTableViewer checkboxTableViewer;
	
	public CompositeTableColumnViewer(Composite parent, int style, TableNode tableNode, ColumnInfo[] cols ) {
		super(parent, style);
		GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        this.setLayout(layout);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 1;
        this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        
        checkboxTableViewer = CheckboxTableViewer.newCheckList( this, SWT.BORDER | SWT.FULL_SELECTION);
    	Table table = checkboxTableViewer.getTable();
    	gd = new GridData(SWT.FILL, SWT.FILL, true, true);
    	table.setLayoutData(gd);
    	table.setHeaderVisible(true);
    	table.setLinesVisible(true);
    	table.setLayout(new TableLayout());
    	
    	TableColumn tableColumn = new TableColumn(table, SWT.NONE);
    	tableColumn.setWidth(120);
    	tableColumn.setText("Column Name");
    	tableColumn.addSelectionListener(new SelectionAdapter() {
			boolean sortType = true;//记录上一次的排序方式,默认是升序
			public void widgetSelected(final SelectionEvent e) {
				sortType = !sortType;
				checkboxTableViewer.setSorter(TableColumnInfoSorter.generateSorter(TableColumnInfoSorter.SORT_TYPE_COLUMN_NAME,sortType));
			}
		});
    	
    	tableColumn = new TableColumn(table, SWT.NONE);
    	tableColumn.setWidth(85);
    	tableColumn.setText("Column Type");
    	tableColumn.addSelectionListener(new SelectionAdapter() {
			boolean sortType = true;//记录上一次的排序方式,默认是升序
			public void widgetSelected(final SelectionEvent e) {
				sortType=!sortType;
				checkboxTableViewer.setSorter(TableColumnInfoSorter.generateSorter(TableColumnInfoSorter.SORT_TYPE_TYPE_NAME,sortType));
			}
		});
    	
    	tableColumn = new TableColumn(table, SWT.NONE);
    	tableColumn.setWidth(95);
    	tableColumn.setText("Type Length");
    	tableColumn.addSelectionListener(new SelectionAdapter() {
			boolean sortType = true;//记录上一次的排序方式,默认是升序
			public void widgetSelected(final SelectionEvent e) {
				sortType=!sortType;
				checkboxTableViewer.setSorter(TableColumnInfoSorter.generateSorter(TableColumnInfoSorter.SORT_TYPE_TYPE_LENGTH,sortType));
			}
		});
    	
    	tableColumn = new TableColumn(table, SWT.NONE);
    	tableColumn.setWidth(95);
    	tableColumn.setText("Null Value");
    	tableColumn.addSelectionListener(new SelectionAdapter() {
			boolean sortType = true;//记录上一次的排序方式,默认是升序
			public void widgetSelected(final SelectionEvent e) {
				sortType=!sortType;
				checkboxTableViewer.setSorter(TableColumnInfoSorter.generateSorter(TableColumnInfoSorter.SORT_TYPE_NULL_NAME,sortType));
			}
		});
    	
    	tableColumn = new TableColumn(table, SWT.NONE);
    	tableColumn.setWidth(95);
    	tableColumn.setText("Default Value");
    	
    	tableColumn = new TableColumn(table, SWT.NONE);
    	tableColumn.setWidth(95);
    	tableColumn.setText("Comment");
        
		if( null != cols && cols.length > 0 ){
            
			checkboxTableViewer.setContentProvider( new TableInfoContentProvider() );
            checkboxTableViewer.setLabelProvider( new TableInfoLabelProvider() );
            checkboxTableViewer.setInput( cols );
            
            checkboxTableViewer.setAllChecked(true);
           
		}
	}

	public CheckboxTableViewer getCheckboxTableViewer() {
		return checkboxTableViewer;
	}

	public void setCheckboxTableViewer(CheckboxTableViewer checkboxTableViewer) {
		this.checkboxTableViewer = checkboxTableViewer;
	}	

}
