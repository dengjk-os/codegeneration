package com.dengjk.eclipse.plugin.codegeneration.ui.wizards.generate;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * 一个模板对应的按钮
 *
 */
public class TemplateItemWizard {
	
	/**
	 * 模板路径
	 */
	private String templatePath;

	/**
	 * 是否需要选择
	 */
	private Button btnNeedGenerate;
	
	/**
	 * 
	 */
	private Label txtLabel;
	
	/**
	 * 路径
	 */
	private Text txtPackage;
	
	/**
	 * 查看按钮
	 */
	private Button btnBrowsePackage;
	

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public Button getBtnNeedGenerate() {
		return btnNeedGenerate;
	}

	public void setBtnNeedGenerate(Button btnNeedGenerate) {
		this.btnNeedGenerate = btnNeedGenerate;
	}
	
	public Label getTxtLabel() {
		return txtLabel;
	}

	public void setTxtLabel(Label txtLabel) {
		this.txtLabel = txtLabel;
	}

	public Text getTxtPackage() {
		return txtPackage;
	}

	public void setTxtPackage(Text txtPackage) {
		this.txtPackage = txtPackage;
	}

	public Button getBtnBrowsePackage() {
		return btnBrowsePackage;
	}

	public void setBtnBrowsePackage(Button btnBrowsePackage) {
		this.btnBrowsePackage = btnBrowsePackage;
	}
	
}
