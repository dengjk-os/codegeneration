
package com.dengjk.eclipse.plugin.codegeneration.ui.views;

import net.sourceforge.sqlexplorer.dbstructure.nodes.INode;

import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.dengjk.eclipse.plugin.codegeneration.ui.dbstructure.nodes.ManageDriverNode;
import com.dengjk.eclipse.plugin.codegeneration.ui.views.actions.OpenConnectionAction;

/**
 * 类说明:DBBrowse中的TreeViewer
 * 
 */
public class DBTreeView extends TreeViewer {

	private ViewDBBrowse viewDBBrowse;
	
	public DBTreeView(Composite parent, int style, ViewDBBrowse viewDBBrowse) {
		super(parent, style);
		this.viewDBBrowse = viewDBBrowse;
		initialTreeViewer();
	}
	
	private void initialTreeViewer(){
		addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				Object element = ((IStructuredSelection) event.getSelection()).getFirstElement();
				try {
                    // find view
                	ViewTableInfo detailView = (ViewTableInfo) viewDBBrowse.getSite().getPage().findView(
                			ViewTableInfo.class.getName());
                    if (detailView == null) {
                    	viewDBBrowse.getSite().getPage().showView(ViewTableInfo.class.getName());
                    }
                    viewDBBrowse.getSite().getPage().bringToTop(detailView);
                    synchronizeDetailView(detailView);
                } catch (Exception e) {
                    // fail silent
                }
                
				if (DBTreeView.this.isExpandable(element)) {
					DBTreeView.this.setExpandedState(element,
							!DBTreeView.this.getExpandedState(element));
					
				} else if ((element instanceof ManageDriverNode)) {
					ManageDriverNode manageDriverNode = (ManageDriverNode)element;
					OpenConnectionAction openConnectionAction = new OpenConnectionAction();
					openConnectionAction.setSelectedNodes(new INode[]{ manageDriverNode });
					openConnectionAction.setTreeViewer(DBTreeView.this);
					openConnectionAction.run();
				}
			}
		});
        
        /*树视图监听事件 start*/
        // add selection change listener, so we can update detail view as
        // required.
        addSelectionChangedListener(new ISelectionChangedListener() {

            public void selectionChanged(SelectionChangedEvent ev) {

                // set the selected node in the detail view.
            	ViewTableInfo detailView = (ViewTableInfo) viewDBBrowse.getSite().getPage().findView(
            			ViewTableInfo.class.getName());
            	synchronizeDetailView(detailView);
            }
        });

        // add expand/collapse listener
        addTreeListener(new ITreeViewerListener() {

            public void treeCollapsed(TreeExpansionEvent event) {

                // refresh the node to change image
                INode node = (INode) event.getElement();
                node.setExpanded(false);
                TreeViewer viewer = (TreeViewer) event.getSource();
                viewer.update(node, null);
            }


            public void treeExpanded(TreeExpansionEvent event) {

                // refresh the node to change image
                INode node = (INode) event.getElement();
                node.setExpanded(true);
                TreeViewer viewer = (TreeViewer) event.getSource();
                viewer.update(node, null);
            }

        });
	}

	public void synchronizeDetailView( final ViewTableInfo detailView){
        BusyIndicator.showWhile(Display.getCurrent(), new Runnable() {

            public void run() {

                if (detailView == null) {
                    return;
                }

                if (this == null) {
                    return;
                }

                INode selectedNode = null;

                if (this != null) {

                    // find our target node..
                    IStructuredSelection selection = (IStructuredSelection) DBTreeView.this.getSelection();

                    // check if we have a valid selection
                    if (selection != null && (selection.getFirstElement() instanceof INode)) {

                        selectedNode = (INode) selection.getFirstElement();

                    }

                }

                detailView.setSelectedNode(selectedNode);
            }
        });
	}

}
