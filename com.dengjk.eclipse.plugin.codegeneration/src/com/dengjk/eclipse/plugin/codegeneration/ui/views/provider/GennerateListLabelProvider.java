package com.dengjk.eclipse.plugin.codegeneration.ui.views.provider;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationUIPlugin;
import com.dengjk.eclipse.plugin.codegeneration.ui.CodeGenerationSubclipseMessages;

/**
 * 类说明:最近生成的文件
 * 
 */
public class GennerateListLabelProvider implements ITableLabelProvider{

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		if( columnIndex > 0 ){
			return null;
		}
		IFile ifile = (IFile)element;
		String fileName = ifile.getName();
		if( fileName.trim().toLowerCase().endsWith(".java")){
			ImageDescriptor _image = CodeGenerationUIPlugin.getImageDescriptor(CodeGenerationSubclipseMessages.getString("Images.JavaObject"));
			return _image.createImage();
		}else if( fileName.trim().toLowerCase().endsWith(".xml")){
			ImageDescriptor _image = CodeGenerationUIPlugin.getImageDescriptor(CodeGenerationSubclipseMessages.getString("Images.XMLObject"));
			return _image.createImage();
		}
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		IFile ifile = (IFile)element;
		if( columnIndex == 0 ){
			return ifile.getName();
		}
		return "(" + ifile.getProject().getName() + ")" + ifile.getProjectRelativePath().toString();
		/*Object[] dataRows = (Object[])element;
		if( null == dataRows || dataRows.length == 0 || null == dataRows[columnIndex] ){
			return EMPTY_STRING;
		}
		return String.valueOf(dataRows[columnIndex]);*/
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		
	}

}
