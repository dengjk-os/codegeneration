package com.dengjk.eclipse.plugin.codegeneration.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用Map数据
 */
public class DataTypeMapperUtils {
    /**
     * 状态编码转换
     */
    private final static Map<String, String> DATA_TYPE_MAP = new HashMap<String, String>();

    static {
    	DATA_TYPE_MAP.put("tinyint", "Integer");
    	DATA_TYPE_MAP.put("smallint", "Integer");
    	DATA_TYPE_MAP.put("mediumint", "Integer");
    	DATA_TYPE_MAP.put("int", "Integer");
    	DATA_TYPE_MAP.put("integer", "integer");
    	DATA_TYPE_MAP.put("bigint", "Long");
    	DATA_TYPE_MAP.put("float", "Float");
    	DATA_TYPE_MAP.put("double", "Double");
    	DATA_TYPE_MAP.put("decimal", "BigDecimal");
    	DATA_TYPE_MAP.put("bit", "Boolean");
    	DATA_TYPE_MAP.put("char", "String");
    	DATA_TYPE_MAP.put("varchar", "String");
    	DATA_TYPE_MAP.put("tinytext", "String");
    	DATA_TYPE_MAP.put("text", "String");
    	DATA_TYPE_MAP.put("mediumtext", "String");
    	DATA_TYPE_MAP.put("longtext", "String");
    	DATA_TYPE_MAP.put("date", "Date");
    	DATA_TYPE_MAP.put("datetime", "Date");
    	DATA_TYPE_MAP.put("timestamp", "Date");
    }
    
    public static final String getJavaType(String columnDataType){
    	if(columnDataType != null){
    		columnDataType = columnDataType.toLowerCase();
    	}
    	// 是否有空格隔开，存在BIGINT UNSIGNED类型
    	int separatorBlank =  columnDataType.indexOf(" ");
    	if(separatorBlank != -1){
    		columnDataType = columnDataType.substring(0,separatorBlank);
    	}
    	return DATA_TYPE_MAP.get(columnDataType);
    }
   
}
