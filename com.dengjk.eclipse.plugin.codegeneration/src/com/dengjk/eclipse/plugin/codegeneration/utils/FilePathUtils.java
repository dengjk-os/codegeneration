package com.dengjk.eclipse.plugin.codegeneration.utils;

import java.io.File;

public class FilePathUtils {

	
	public static String getFileName(String filePath){
		if(filePath == null){
			return null;
		}
		int last = filePath.lastIndexOf(File.separator);
		if(last == -1){
			return filePath;
		}
		return filePath.substring(last+1);
	}
	
	public static String getFolderPath(String filePath){
		if(filePath == null){
			return null;
		}
		int last = filePath.lastIndexOf(File.separator);
		if(last == -1){
			return filePath;
		}
		return filePath.substring(0,last);
	}
	
	/**
	 * 获取截取前缀后的模板名称
	 * @param filePath
	 * @return
	 */
	public static String getTemplateSimpleName(String filePath){
		 String fileName = getFileName(filePath);
		 int packageIndx = fileName.indexOf("-");
		 if(packageIndx == -1){
			 return fileName;
		 }
		 return fileName.substring(packageIndx+1);
	}
	
	/**
	 * 获取模板前缀名称
	 * @param filePath
	 * @return
	 */
	public static String getTemplatePackName(String filePath){
		String fileName = getFileName(filePath);
		int packageIndx = fileName.indexOf("-");
		if(packageIndx == -1){
			return null;
		}
		return fileName.substring(0,packageIndx);
	}
	
}
