package com.dengjk.eclipse.plugin.codegeneration.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.eclipse.core.runtime.Platform;

public class PluginLogUtils {
	
	static {
		String logFile = Platform.getLocation().toString()+"/codegeneration.log";
		File file = new File(logFile);
		try {
			if(!file.exists()){
				file.createNewFile();
			}
			PrintStream printer = new PrintStream(new FileOutputStream(file),true,"UTF-8");
			System.setErr(printer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static  String getTrace(Throwable t) {
	    StringWriter stringWriter= new StringWriter(); 
	    
	    PrintWriter writer= new PrintWriter(stringWriter); 
	    
	    t.printStackTrace(writer);
	    
	    StringBuffer buffer= stringWriter.getBuffer();  
	    
	    return buffer.toString();  
	} 
	
	/**
	 * 借助System输出实现，避免需引入日志框架
	 * @param e
	 */
	public static void logErr(Throwable e){
		String eInfo = getTrace(e);
        System.err.print(eInfo);
	}
	
	
	
	public static void logErr(String message){
		System.err.print(message);
	}
	
	
	
}
